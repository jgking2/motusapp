import React, {Component} from 'react';
import {
    View,     
    ScrollView,
    TouchableOpacity,
    ActivityIndicator,
    Image,
    Text,
    StyleSheet
} from 'react-native';
import autobind from 'autobind-decorator';

import { connect } from 'react-redux';
import { 
    DevicesLoading, 
    ProviderButton
} from './components';

import Swiper from 'react-native-swiper';
import { ButtonGroup } from 'react-native-elements';

import { 
    MotusMenuToggle, 
    colors,
    MotusTabs
} from '../../components';

import { ConnectDevice } from './ConnectDevice';
import { ImportDeviceActivity } from './ImportDeviceActivity';
import { assets } from '../../assets';

const mapStateToProps = ({ 
    profileState : { 
        profile : { devices }
    }
    }) => ({ 
        devices,
    });

// const mapDispatchToProps = dispatch => ({
//     requestOAuth : (provider) => dispatch(requestOauthDialog(provider))
// });

@connect( mapStateToProps )
export class ActivitiesScreen extends Component {
    swiper;
    state = {
        selectedIndex : 0
    };

    static navigationOptions = ({ navigation : { navigate } }) => ({
        title: 'Devices',
        headerLeft: (
            <MotusMenuToggle  
                color={"white"}               
                onPress={() => navigate('DrawerOpen')} />
        )
    });

    render() {
        const {
            devices = {},
            navigation : { 
                navigate
            }
        } = this.props;
        const { selectedIndex } = this.state;
        
        return (
            <View style={{ flex: 1 }}>
                <MotusTabs 
                    selectedIndex={selectedIndex}
                    onPress={this.changePage.bind(this)}
                    buttons={['Manage', 'Add Device']} />
                
                <Swiper
                    loadMinimal={true}                    
                    onIndexChanged={this.onSwipe.bind(this)}
                    loop={false}
                    ref={(ref) => this.swiper = ref}>

                    <ImportDeviceActivity 
                        onSelected={(provider) => navigate(provider.key)}
                        providers={devices}  />

                    <ConnectDevice  />
                </Swiper>
            </View>
        )
        // new Swiper().
    }

    changePage(targetIndex) {
        this.setState({
            selectedIndex : targetIndex
        });
        const scrollBy = (targetIndex + 1) % 2? -1 : 1;
        this.swiper.scrollBy(scrollBy, true);
    }

    onSwipe(selectedIndex) {
        this.setState({
            selectedIndex
        });
    }
}

const styles = StyleSheet.create({
    logoContainer : {
        // flexDirection : "row"
    },
    logo : {
        height: 10,
        width: 40
    }
});