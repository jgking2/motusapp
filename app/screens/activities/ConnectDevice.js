import React, { Component } from 'react';
import { connect } from 'react-redux';
import { 
    ScrollView,
    Button,
    Text
 } from 'react-native';

import { requestOauthDialog } from '../../services/oauth';
import { authorizeProvider } from '../../services/providers';
import { ProviderButton } from './components';

import { motusStyles } from '../../components';

const mapStateToProps = ({ 
    providerState : {
        linked,
        oauth,
        local
    }
}) => ({ 
    linked,
    oauth,
    local
});

const mapDispatchToProps = dispatch => ({
    requestOAuth : (provider) => dispatch(requestOauthDialog(provider)),
    linkLocal : (provider) => dispatch(authorizeProvider(provider))
});

@connect(
    mapStateToProps,
    mapDispatchToProps
)
export class ConnectDevice extends Component {

    render() {
        const { 
            linked = {},
            oauth,
            local
        } = this.props;

        const availableOauth = Object.keys(oauth)
            .filter(key => !linked[key])
            .map(key => ({ 
                config : oauth[key],
                key
            }))
            .map(({ config, key }, index) => {
                return <ProviderButton
                    containerStyle={{marginBottom:10}}
                    onPress={this.connectOauth.bind(this, config.oauth)}
                    key={key}
                    source={config.logo}
                    text={config.connectText} />
            });

        const availableLocal = Object.keys(local)
            .filter(key => !linked[key])
            .map(key => ({ config: local[key], key}))
            .map(({ config, key }, index) => {
                return <ProviderButton 
                    containerStyle={{marginBottom:10}}
                    onPress={this.connectLocal.bind(this, key)}
                    key={key}
                    source={config.logo}
                    text={config.connectText} />
            });;
        const noDevicesAvailable = !(availableLocal.length + availableOauth.length);
        const connectedDevices = Object.keys(linked);
        if(noDevicesAvailable) {
            return (
                <Text style={[
                    motusStyles.missingText, 
                    motusStyles.padded]}>
                    No more devices available, we will add more over  time!
                </Text>
            )
        }
        
        return (
                <ScrollView 
                    contentContainerStyle={{
                        padding:10
                    }} >
                    { availableOauth }
                    { availableLocal }
                </ScrollView>
            )
        }

        connectOauth(provider) {
            console.log(provider);
            const { requestOAuth } = this.props;
            requestOAuth(provider);
        }

        connectLocal(providerKey) {
            console.log(providerKey);
            const { linkLocal } = this.props;
            linkLocal(providerKey);
        }
    }