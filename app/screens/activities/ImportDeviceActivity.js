import React, { Component } from 'react';

import { connect } from 'react-redux';

import { 
    ScrollView ,
    Text,
    Button,
    View,
    StyleSheet
} from 'react-native';
import {
    List,
    ListItem
} from 'react-native-elements';
import { motusStyles } from '../../components';

const mapStateToProps = ({ 
    providerState : { 
        linked,
        oauth,
        local
    } }) => ({ 
        linked,
        oauth,
        local
    });

@connect(mapStateToProps)
export class ImportDeviceActivity extends Component {
    state = {
        providers : { }
    };

    componentWillMount() {
        
        const { 
            oauth, 
            local 
        } = this.props;

        const providers = Object.assign({}, oauth, local);
        this.setState({
            providers
        });
    }

    render() {
        const { 
            linked = {},
            onSelected
        } = this.props;

        const { providers } = this.state;

        const linkedProviders = Object.keys(linked)
            .map(key => providers[key]);
        
        if(!linkedProviders.length) {
            return <NoDevices />
        }

        return (
            <View style={{ margin:10 }}>
                <ScrollView 
                    contentContainerStyle={{
                        borderRadius: 5,
                        borderWidth: StyleSheet.hairlineWidth,
                        borderColor: '#ddd',
                        backgroundColor: 'rgba(256,256,256,1)',
                        shadowColor: 'rgba(0,0,0, .4)',
                        shadowOffset: { height: 1, width: 1 },
                        shadowOpacity: 1,
                        shadowRadius: 1
                    }} >                    
                    {   linkedProviders
                            .map((provider, index) => {
                                return <ConnectedProvider 
                                    onPress={this.selectProvider.bind(this, provider)}
                                    provider={provider} key={index} />
                    })  }
                    
                </ScrollView>
            </View>
        )
    }

    selectProvider(provider) {
        const { onSelected } = this.props;
        onSelected && onSelected(provider);
    }
};

const ConnectedProvider = ({ provider, onPress }) => {
    return <ListItem
        containerStyle={{ 
            borderBottomWidth: .1
        }}
        onPress={onPress}
        avatar={provider.logo}
        roundAvatar
        title={provider.name} />
};


const NoDevices = () => {
    return (
        <Text style={[
            motusStyles.missingText, 
            motusStyles.padded
        ]}>
            Add a device to bring in the activity!
        </Text>
    );
};