import React from 'react';
import { 
    TouchableOpacity, 
    StyleSheet,
    Image,
    Text
} from 'react-native';

export const ProviderButton = ({ 
    source, 
    text,
    onPress,
    containerStyle = {}
}) => {
    return (
        <TouchableOpacity 
            onPress={onPress}
            style={[styles.container, containerStyle]}>
            <Image
                style={styles.image}
                source={source} />
            <Text style={styles.text}>
                {text}
            </Text>
        </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    container: {
        // alignSelf:"stretch", 
        backgroundColor:"white",
        flexDirection: "row",
        padding: 10,
        alignItems:"center",
        borderRadius: 40,
        shadowColor: 'rgba(0,0,0, .4)',
        shadowOffset: { height: 1, width: 1 },
        shadowOpacity: 1,
        shadowRadius: 1
    },
    image: {
        width:45, 
        height:45
    },
    text: {
        flex:.75, 
        textAlign:"center",
        color: "#888",
        fontWeight:"600",
        fontSize:16
    }
})