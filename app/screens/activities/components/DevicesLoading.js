import React from 'react';
import {ActivityIndicator} from 'react-native';

export const DevicesLoading = (props) => {
    return <ActivityIndicator {...props}></ActivityIndicator>
}