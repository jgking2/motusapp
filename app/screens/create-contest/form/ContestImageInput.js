import React, { Component } from 'react';
import { 
    View, 
    Image, 
    Text, 
    StyleSheet,
    Dimensions,
    ImageBackground,
    Alert
} from 'react-native';

import { FetchImage, motusStyles, colors } from '../../../components';

const { width } = Dimensions.get("window");
const imageHeight = width;

const UploadImageContainer = ({ 
    style, children 
}) => {
    return (
        <View style={[styles.imageContainer, style]}>
            { children }
        </View>
    )
};

export class ContestImageInput extends Component {
    componentWillMount() {
        const { url } = this.props;
        this.setState({
            url : url
        });
    }

    state = {
        url : '' //Set to default image
    };

    render() {
        const {
            containerStyle, 
            interiorStyle,
            missingImageText,
            children
        } = this.props;

        const { url } = this.state;
        const uri = { uri : url };

        const MissingPhotoText = (
            <Text style={[
                styles.placeholderText]} >
                Add Image
            </Text>
        );

        return (
            <FetchImage 
                onSelect={this.stageImage.bind(this)}>
                <UploadImageContainer 
                    style={containerStyle}>
                    <ImageBackground 
                        resizeMode={"contain"}
                        style={styles.imageStyle}
                        source={uri}>
                        { 
                            !uri.uri && MissingPhotoText || null 
                        }
                    </ImageBackground>
                </UploadImageContainer>
                
            </FetchImage>
        )
    }

    /**
     * 
     * @param {{ path, sourceURL, width, size, height, data, creationDate }} img 
     */
    stageImage(img) {
        const { path, creationDate } = img;
        const { onSelection } = this.props;
        this.setState({
            url: path
        });
        onSelection && onSelection(path);
    }
}

const styles = StyleSheet.create({
    container : { 
        justifyContent: "center"
    },
    placeholderText : {
        textAlign: "center",
        textAlignVertical:"center",
        color: colors(1).primary
    },
    interiorStyle : { 
        alignItems : "center", 
        justifyContent: "center",
        backgroundColor: colors(.5).secondary,
        height: 75,
        width: 75
    },
    imageStyle : {
        justifyContent: 'center',
        alignItems: 'center',
        height : undefined,
        width : undefined,
        alignSelf: 'stretch',
        flex: 1
    },
    imageContainer : {
        justifyContent: "center",
        alignItems: "center",
        height : imageHeight,
        width: imageHeight,
        overflow: 'hidden'
    }
});