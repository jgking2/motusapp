import React, { Component } from 'react';
import { 
    FlatList, 
    Text, 
    View,
    TouchableOpacity,
    StyleSheet
} from 'react-native';

import { colors } from '../../../components';

export const DurationInput = ({ 
    durations, 
    onChange,
    value,
    ...addtl
}) => {
    return (
        <View style={{ 
            paddingTop: 10, 
            paddingBottom: 10, 
            flexDirection:"row", justifyContent:"space-between"}}>
            {
                durations.map(duration => {
                    return (
                        <DurationSelection
                            key={duration} 
                            duration={duration} 
                            selected={value} 
                            onSelected={onChange}
                            />
                    )
                })
            }
        </View>
    )
};

const DurationSelection = (props) => {
    const { 
        duration, 
        selected, 
        onSelected 
    } = props;

    const containerStyles = [styles.durationContainer];
    const textStyles = [styles.centerText];

    const durationSelected = selected === duration;

    if(durationSelected) {
        containerStyles.push(styles.selectedDuration);
        textStyles.push(styles.selectedText);
    }
    return (
        <TouchableOpacity style={containerStyles} onPress={() => onSelected(duration)} >
            <Text style={textStyles}>{duration} { !durationSelected && 'Days'}</Text>
        </TouchableOpacity>
    )
};

const styles = StyleSheet.create({
    durationContainer : {
        width : 50,
        height: 50,
        borderRadius: 25,
        justifyContent: 'center',
        alignItems : "center"
    },
    selectedDuration : {
        backgroundColor: colors(.6).primary,
        shadowColor: 'rgba(0,0,0,.5)',
        shadowOpacity: .5,
        shadowOffset: {
            height: 1,
            width: 1
        },
        shadowRadius: .3
    },
    centerText: {
        textAlign: "center"
    },
    selectedText: {
        color: 'white',
        fontSize: 20
    }
})