const requiredFields = ['name', 'description'];


export const validate = values => {
    const errors = {};
    requiredFields.forEach(field => {
        if(!values[field]) {
            errors[field] = `${field} required`;
        }
    });
    return errors;
};

//Add async username validation.