import React, { Component } from 'react';
import { FlatList, View, StyleSheet } from 'react-native';

import { UserAvatar, SearchMotusUsersModal } from '../../user-search';

export class ContestMembersInput extends Component {
    state = {
        selectedMembers : []
    }

    render() {
        const { selectedMembers } = this.state;
        return (
            <View style={{ flexDirection : "row" }}>
                <SearchMotusUsersModal onUsersSelected={this.stageUsers.bind(this)} multiSelect={true}>
                        <Icon style={{padding: 5}} name="person-add" />                   
                </SearchMotusUsersModal>
                <FlatList 
                    horizontal={true}
                    data={selectedMembers}
                    keyExtractor={({uid}) => uid}
                    renderItem={({item})=> <UserAvatar user={item} /> } />
                
            </View>
        )
    }

    /**
     * 
     * @param {[]} users 
     */
    stageUsers(users) {
        const { input } = this.props;
        //Broadcast to form.
        input.onChange(users);

        //Updates state to show the users selected.
        this.setState({
            selectedMembers : users
        });
    }
}

const styles = StyleSheet.create({
    
});