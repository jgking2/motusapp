import React, { Component } from 'react';
import { 
    View, 
    TextInput, 
    ScrollView,
    StyleSheet
} from 'react-native';

const maxPrice = 100;
const minPrice = 1;

const maxDescriptionLength = 50;

const fields = {
    name : 'name',
    price : 'price',
    description : 'description',
    startingDate : 'startingDate'
};

const form = {
    [fields.name] : {
        required: true,
        validation: (text) => text.length > 3
    },
    [fields.price] : {
        required: false,
        validation : (text) => {
            let price = +text;
            if(!price) return false;
            return price > minPrice && price < maxPrice;
            // price.toLocaleString(undefined, { 
            //     minimumFractionDigits: 2, maximumFractionDigits: 2
            // });
        }
    },
    [fields.description] : {
        required : false,
        validation : text => text && text.length < maxDescriptionLength
    },
    [fields.startingDate] : {
        required : true,
        validation : date 
    }
}

export class NewContestForm extends Component {
    state = {
        focused : '',
        errors : { },
        dirty :  
    };

    render() {
        return (
            <View>
                <TextInput
                    onFocus={}
                    onChangeText={text => {
                        console.log(text);
                    }} />
                <TextInput
                    multiline={true}
                    onChangeText={text => {
                        console.log(text);
                    }} />
                <TextInput
                    keyboardType={"numeric"}
                    onChangeText={this.onFieldChange.bind(this, fields.price)} />
                
            </View>
        )
    }

    onFieldChange(value, name) {
        console.log(form[name]);
    }

    onFieldFocus(name) {

    }
}

