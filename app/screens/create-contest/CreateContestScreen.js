import React, {Component} from 'react';
import {
    View,
    Text,
    StyleSheet,
    ScrollView
} from 'react-native';
import { connect } from 'react-redux';
import { duration } from 'moment';

import { NewContestForm } from './CreateContestForm';
import {
    createContest,
    generateInvitationToken,
    contestFeeTypeMap
} from '../../services/contests';

import {
    MotusButton,
    motusStyles,
    MotusPayment
} from '../../components';
import { ContestImageInput } from './form';
import { MotusBottomButton } from '../../components';
import { contestFeedSaga } from '../../services/feed/index';

const statuses = {
    creating : 'CREATING',
    saving : 'SAVING',
    complete : 'COMPLETE',
    payment : 'PAYMENT'
};


const mapStateToProps = ({ 
    contestState : { creating, contests }
}) => ({
    creating,
    contests
});

const mapDispatchToProps = (dispatch) => ({
    saveContest : (contest, imageUri) => dispatch(createContest(contest, imageUri)),
    getInviteToken: (contest) => dispatch(generateInvitationToken('yeeeaahh'))
});

@connect(
    mapStateToProps, 
    mapDispatchToProps
)
export class CreateContestScreen extends Component {
    get createContestState() {
        const { saving, tempId } = this.state;
        const { creating, contests } = this.props;

        const newContest = creating[tempId];
        let createdId = (newContest || {}).key;
        const created = contests[createdId];

        if(!newContest) {
            return statuses.creating;
        }
        if(!newContest.key) {
            return statuses.saving;
        }
        if(!created.payments && 
            newContest.feeType.type !== contestFeeTypeMap.Free) {
            return statuses.payment;
        }
        return statuses.complete;
    }

    state = {
        photoURL : '',
        tempId : undefined
    };

    renderPaymentScreen() {
        const { creating } = this.props;
        const { tempId } = this.state; 

        const newContest = creating[tempId];
        return (
            <View>
                <MotusPayment 
                    amount={newContest.feeType.amount}
                    identifier={newContest.key} />
            </View>
        )
    }
    
    render() {
        this.valid;
        const { tempId, photoURL } = this.state;  
        const { creating } = this.props;
        const newContest = creating[tempId];
        //No contest yet.
        switch (this.createContestState) {
            case statuses.creating:
                return (
                    <NewContestForm 
                        onSubmit={this.saveContest.bind(this)}
                    />
                )
            case statuses.saving:
                return (
                    <View>
                        <Text>
                            Saving....
                        </Text>
                    </View>
                )
            case statuses.payment:
                return this.renderPaymentScreen();
            case statuses.complete:
                return (
                    <ContestInvitationDetails 
                        contest={newContest} />
                )
        }
    }

    onFormChange(value) {
        this.valid;
    }

    saveContest(contest) {
        const { saveContest } = this.props;
        const { photoURL } = contest;
        const { tempId } = saveContest(
            contest,
            photoURL
        );

        this.setState({
            tempId
        });
    }

    stageImage(sourceURL) {
        this.setState({
            photoURL : sourceURL
        });
    }
}

CreateContestScreen.navigationOptions = {
    title: 'Start a contest'
};

const ContestInvitationDetails = ({ contest }) => {
    const url = `motus://join-contest/${contest.key}`;
    return (
        <View style={motusStyles.padded}>
            <Text>
                Success!  Use this token to invite people to the motus contest.
                {contest.key}
            </Text>
        </View>
    )
};

const styles = StyleSheet.create({
    container : {
        flex: 1,
    },
    formContainer : {
        backgroundColor: 'rgba(256,256,256,.9)',
        marginTop: 10
    },
    screenBody : {
        paddingBottom: 20
    }
});