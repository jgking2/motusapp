import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
    StyleSheet,
    View,
    Image,
    FlatList,
    Button,
    ScrollView,
    Text,
    KeyboardAvoidingView,
    Dimensions
} from 'react-native';
import DatePicker from 'react-native-datepicker';
import {
    FetchImage,
    MotusTextInput,
    MotusBottomButton,
    colors
} from '../../components';
import { contestFeeTypeMap } from '../../services/contests';
import {
    ButtonGroup
} from 'react-native-elements';

import {
    Control,
    Form,
    actions,
    Errors
} from 'react-redux-form/native';
import {
    ContestImageInput,
    // ContestMembersInput,
    DurationInput
} from './form';
import { styles } from '../../components/parallax-header/styles';
import propTypes from 'redux-form/lib/propTypes';

const { width } = Dimensions.get('window');

const validations = {
    required: (val) => {
        return val && val.length
    },
    contestName: (value) => {
        return (
            value &&
            value.length > 2 &&
            value.length < 12
        );
    },
    feeType: (type) => {

    }
}

@connect()
export class NewContestForm extends Component {

    render() {
        const { onSubmit } = this.props;
        const options = [
            15,
            30,
            45,
            60
        ]; //fugly
        return (
            <Form
                style={fieldStyles.container}
                model="contest"
                onSubmit={onSubmit}>
                <KeyboardAvoidingView
                
                    behavior={"position"}
                    keyboardVerticalOffset={54}
                    contentContainerStyle={{
                        backgroundColor: "white",
                        padding: 10,
                        flex: 1
                    }}
                    style={
                        {
                            flex: 1, backgroundColor: 'white'
                        }}>
                    <ScrollView
                        contentContainerStyle={{ flex: 1 }}>
                        <View style={{
                            marginVertical: 20,
                            alignItems:"center"
                        }}>
                        <Control
                            containerStyle={{
                                borderRadius:100,
                                width: 100,
                                height: 100
                            }}
                            model=".photoURL"
                            component={ContestImageInput}
                            mapProps={{
                                onSelection: ({ onChange }) => onChange
                            }} />
                        </View>
                        <Control
                            withFieldValue={true}
                            model=".feeType"
                            component={FeeGroup}
                        />
                        <Control.TextInput
                            validators={{
                                required: validations.required
                            }}
                            label={"Contest Name"}
                            placeholder={"Contest Name"}
                            model=".name"
                            component={MotusTextInput}
                        />

                        <Errors model=".name" />
                        <Control.TextInput
                            component={MotusTextInput}
                            label={"Description"}
                            placeholder={"Description"}
                            model=".description"
                        />
                        <Text
                            style={fieldStyles.label}>
                            Start Date
                        </Text>
                        <View style={[{
                            flexDirection: "row"
                        }, fieldStyles.inputContainer]}>
                            <Control
                                style={fieldStyles.dateStyle}
                                customStyles={{
                                    dateTouchBody: fieldStyles.dateTouchBody,
                                    dateInput: fieldStyles.dateContainer
                                }}
                                confirmBtnText={"Done"}
                                cancelBtnText={"Cancel"}
                                showIcon={false}
                                model=".startingDate"
                                component={DatePicker}
                                placeholder={"Select Start Date"}
                                mapProps={{
                                    date: ({ modelValue }) => modelValue,
                                    onDateChange: ({ onChange }) => onChange
                                }} />
                        </View>
                        <View>
                            <Control
                                model=".duration"
                                durations={options}
                                component={DurationInput}
                            />
                        </View>
                    </ScrollView>
                    <MotusBottomButton
                        onPress={
                            this.triggerSubmit.bind(this)
                        }
                        text={"Save"} />
                </KeyboardAvoidingView>


            </Form>
        )
    }

    triggerSubmit() {
        const { dispatch } = this.props;
        dispatch(actions.submit('contest'))
    }
}

const fieldStyles = StyleSheet.create({
    container: {
        flex: 1
    },
    textContainer: {
        marginTop: (width * .75),
        backgroundColor: 'rgba(256, 256, 256, 1)'
    },
    label: {
        fontSize: 16,
        color: colors(.9).primary
    },
    inputContainer: {
        paddingVertical: 8,
        borderBottomColor: colors(.9).primary,
        borderBottomWidth: StyleSheet.hairlineWidth
    },
    padded: {
        padding: 5
    },
    dateStyle: {
        flex: 1
    },
    dateContainer: {
        borderWidth: 0,
        alignItems: 'flex-start'
    },
    dateTouchBody: {
        alignItems: 'flex-start'
    }
});

// const typeMap = {
//     Sponsored : 1,
//     Paid: 2,
//     Free : 3
// };

const feeTypes = Object.keys(contestFeeTypeMap);

const FeeGroup = (props) => {
    const [ sponsored, paid, free ] = feeTypes;
    const { value = { }, onChange } = props;

    var label = '';
    let description = '';
    let feeType = undefined;
    let amountMap = type => amount => onChange({ amount, type });

    if(value.type) {
        feeType = value.type;
        switch(value.type) {
            case contestFeeTypeMap.Sponsored:
                label="The pot amount, paid up front";
                description="Amount"; 
                break;
            case contestFeeTypeMap.Paid:
                label="The entry fee, paid individually";
                description="Amount";
                break;
        }
    }
    
    return (
        <View style={{flexDirection : 'column'}}>
            <ButtonGroup                
                containerStyle={{ marginLeft: 0, marginRight: 0 }}
                selectedIndex={((value.type || 0) -1 )}
                onPress={(index) => {
                    let key = feeTypes[index];
                    onChange(key && { type : contestFeeTypeMap[key] })
                }}
                buttons={feeTypes}
            />
            <PriceEntry 
                label={label}
                description={description} 
                onChange={amountMap(feeType)}
                />
        </View>
    )
};

const PriceEntry = ({label, description, onChange }) => {
    return (
        label && 
        <MotusTextInput 
            label={label} 
            placeholder={description}
            onChangeText={onChange} />
        || <View />
    )
};
