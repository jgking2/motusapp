import React, { Component } from 'react';
import { DrawerItems, SafeAreaView } from 'react-navigation';
import { 
    StyleSheet,
    ScrollView,
    Text,
    Image,
    TouchableOpacity,
    View
} from 'react-native';
import { connect } from 'react-redux';

import {
    AvatarOrInitials,
    MotusGradient
} from '../components';
import { userLogout } from '../services/authentication';
import { text } from '../core';

export const AppDrawer = ({
    user,
    logout,
    ...props
}) => {
    const avatarProps = {
        photoURL : user.photoURL,
        name : user.displayName,
        rounded : true,
        large : true
    };
    
    return (        
        <ScrollView>
            <MotusGradient style={{ 
                    alignItems:"center", 
                    padding: 25,
                    paddingTop: 50,
                    flex: 1 
                    }}>
                    
                    <AvatarOrInitials {...avatarProps} />
                    <Text style={{
                        textAlign:"center",
                        paddingTop: 15,
                        backgroundColor:"transparent",
                        color:'white'
                        }}>     
                        {user.displayName}
                    </Text>
                </MotusGradient>
                                
                <DrawerItems {...props} />
                <TouchableOpacity onPress={logout} style={styles.buttonStyle}>
                    <Text style={styles.buttonText}>    
                        {text.actionLogout}
                    </Text>
                </TouchableOpacity>
        </ScrollView>
    )
};

const mapStateToProps = ({ authState : { user } }) => ({
    user
});

const mapDispatchToProp = dispatch => ({
    logout : () => dispatch(userLogout())
});

export const MotusDrawer = connect(
    mapStateToProps, mapDispatchToProp
)(AppDrawer);

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    buttonStyle: {
        margin: 16,        
    },
    buttonText: {
        fontWeight: 'bold',
        color: 'rgba(0,0,0,.96)'
    }
});

