import React, { Component } from 'react';
import { View } from 'react-native';
import { 
    Container,
    Content,
    Header,
    H1,
    Text
} from 'native-base';
import { connect } from 'react-redux';
import { NewUserForm } from './NewUserForm';
import {createPublicProfile} from '../../services/users';
import { saveUserProfile } from '../../services/user-profile';

const mapStateToProps = ({ authState : { user }}) => ( { user } );
const mapDispatchToProps = (dispatch) => {
    return {
        createProfile : (user, profile) => dispatch(saveUserProfile(user, profile))
    }
};

@connect(mapStateToProps, mapDispatchToProps)
export class RegistrationScreen extends Component {
    render() {
        let welcomeMessage = 'Welcome to motus!';
        const { 
            user
        } = this.props;

        if(user) {
            welcomeMessage = `Welcome ${user.displayName}`;
        }

        return (
            <Container style={{paddingTop: 35}}>
                <Content padder>
                    <H1>{welcomeMessage}</H1>
                    <Text>Tell us a little more about yourself, and let's begin.</Text>
                    <NewUserForm onSubmit={this.onSubmission.bind(this)}></NewUserForm>
                </Content>
            </Container>
        )
    }

    onSubmission(profile, ...args) {
        const { 
            user,
            createProfile
        } = this.props;
        
        createProfile(user, profile);
    }
}
