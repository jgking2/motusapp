import React, {Component} from 'react';
import { TextInput, View, StyleSheet, Picker } from 'react-native';
import {
    Item,
    Input,
    Label,
    Button,
    Text,
    ListItem,
    Right,
    Radio
} from 'native-base';
import {
    TextFormInput,
    RadioFormInput
} from '../../components';
import {
    Form, 
    reduxForm,
    Field
} from 'redux-form';
import { validate } from './index';
 
//TODO Add better phone number validation.
//TODO Add username validation, add async one to check username.

const phoneFormatter = (number) => {
  if (!number) return '';
  // NNN-NNN-NNNN
  const splitter = /.{1,3}/g;
  number = number.substring(0, 10);
  return number.substring(0, 7).match(splitter).join('-') + number.substring(7);
};
const phoneParser = (number) => number ? number.replace(/-/g, '') : '';

const UserForm = (props) => {
    const { handleSubmit, submit, invalid } = props;
    let options = [
        'Weight loss',
        'Weight gain',
        'Sculpt'
    ];
    return (
        <View style={{paddingTop: 25}}>
            <View style={fieldStyles}>
                <Field 
                    label="Username" 
                    name="username" 
                    component={TextFormInput} />
            </View>
            <View style={fieldStyles}>
                <Field
                    keyboardType="phone-pad"
                    label="Phone Number"
                    name="phone"                     
                    component={TextFormInput} />
            </View>
            <Field
                    name="goal"
                    options={options}
                    component={RadioFormInput} />

            <Button block bordered disabled={invalid} onPress={handleSubmit}>
                <Text>Submit</Text>
            </Button>
        </View>
    )
}

const fieldStyles = StyleSheet.create({
    paddingBottom : {
        paddingBottom: 2
    }
});

export const NewUserForm = reduxForm({ form: 'newUser' , validate})(UserForm);