
export const validate = values => {
    const errors = {};
    const required = [
        'username',
        'phone'
    ];
    required.forEach(field => {
        if(!values[field]) {
            errors[field] = `${field} required`;
        }
    });
    return errors;
};

//Add async username validation.