import React, { Component } from 'react';

import { 
    View, 
    Text,
    TouchableOpacity,
    Share
} from 'react-native';
import { ContestImageInput } from '../../create-contest/form/ContestImageInput';
import { connect } from 'react-redux';
import Icon from 'react-native-vector-icons/Ionicons';
import { 
    sendContestInvitation 
} from '../../../services/invitations';
import { SearchMotusUsersModal } from '../../user-search';
import { 
    MotusBottomButton,
    motusStyles 
} from '../../../components';
import {
    updateContest
} from '../../../services/contests';
import {
    randomInt
} from '../../../core';

const mapStateToProps = () => ({});

const messages = [
    {
        title: "Let's achieve some fitness and activity goals together!", 
        message: "Come join My Motus Contest:"
    }, {
        title: "You've been invited to a Motus Contest.",
        message: "Do you have what it takes to compete?"
    },
    {
        title: "A fun fitness and activity adventure awaits!",
        message : "You've been invited to My Motus Contest"
    }
];

const mapDispatchToProps = (dispatch) => ({
    inviteUsersToContest : (members, contestId, message) => dispatch(sendContestInvitation(members, contestId, message)),
    submitContestUpdate : (contest, update, imageUrl) => dispatch(updateContest(contest, update, imageUrl))
});

@connect(mapStateToProps, mapDispatchToProps)
export class ContestSettingsScreen extends Component {
    componentWillMount() {
        const {
            navigation : {
                state : {
                    params = {}
                }
            }
        } = this.props;
        const { contest } = params
        this.setState({
            contest
        });
    }

    render() {
        const { 
            contest 
        } = this.state;

        return (
            <View style={[{flex:1},motusStyles.padded]}>
                <Text>Once photo is selected, it's updated for all.</Text>
                <ContestImageInput
                    onSelection={this.stageContestPhoto.bind(this)} 
                    url={contest.photoURL} />
                
                <MotusBottomButton
                    onPress={this.share.bind(this, contest.key)}
                    text={"Share Invitation Link"}
                    />
            </View>
        )
    }

    stageContestPhoto(photoURL) {
        const { submitContestUpdate } = this.props;
        const { contest } = this.state;

        submitContestUpdate(contest, {}, photoURL);
    }

    async share(contestId) {
        const index = randomInt(messages.length - 1);
        const { title, message } = messages[index];
        const content = {
            title,
            message,
            url: `motus://contests/join/${contestId}`
        };
        const response = await Share
            .share(content);
    }
    // _onUserSelection(users) {
    //     const { 
    //         inviteUsersToContest
    //     } = this.props;
    //     const { contest } = this.state;
    //     inviteUsersToContest(users, contest.key);
    // }
}