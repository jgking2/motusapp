import React, {Component} from 'react';
import {
    View,
    Text,
    StyleSheet,
    TouchableOpacity
} from 'react-native';
// import {
//     Icon,
//     Card
// } from 'react-native-elements';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

import { feedTypes } from '../../../services/feed';
import {
    ActivityMessage,
    PictureMessage,
    StandardMessage
} from './index';
import { 
    AvatarOrInitials, 
    Timestamp 
} from '../../../components';

import { colors, motusStyles } from '../../../components';
//TODO - wrap the interior with a standard template.
export const FeedItem = ({ 
    message : feedItem, 
    currentUser, 
    contest,
    onLikePress,
    onCommentPress
}) => {
    const { uid } = feedItem;
    const { members } = contest;
    const footerProps = {
        onLikePress,
        onCommentPress,
        feedItem,
        currentUser
    };
    let FeedBody;
    switch(feedItem.type) {
        case feedTypes.ACTIVITY:
            FeedBody = ActivityMessage;
            break;
        case feedTypes.IMAGE:
            FeedBody = PictureMessage;
            break;
        default:
            FeedBody = StandardMessage
            break;
    }
    return (
        <View style={styles.container}>
            <FeedHeader member={members[uid]} feedItem={feedItem} />
            <FeedBody
                    message={feedItem} 
                    profile={currentUser} />
            <FeedFooter {...footerProps} />
        </View>
    )
}

const FeedHeader = ({ member: { photoURL, displayName }, feedItem : { timestamp } }) => {
    return (
        <View style={[styles.horizontalRow, motusStyles.padded]}>
            <AvatarOrInitials rounded photoURL={photoURL} name={displayName} />
            <Text style={styles.padLeft}>{displayName }</Text>
            <View style={{flex:1}}>
                <Timestamp style={styles.messageTime} date={timestamp} />
            </View>
        </View>
    )
};

const FeedFooter = ({ 
    feedItem, 
    onCommentPress, 
    onLikePress, 
    currentUser : { uid }
}) => {
    const {
        likes = {},
        comment_count = 0
    } = feedItem;

    const alreadyLiked = likes[uid];

    const commentText = `View ${comment_count} comments`;
    const cheerColor = alreadyLiked? colors(1).secondary : 'rgba(0,0,0,.3)';

    return (
        <View style={[styles.horizontalRow, motusStyles.padded]}>            
            { !!onCommentPress &&
                <TouchableOpacity style={styles} onPress={() => onCommentPress(feedItem)} >
                    <Text style={[styles.commentText]}>
                        {commentText}
                    </Text>
                </TouchableOpacity>
            }
            <TouchableOpacity style={[styles.horizontalRow, styles.cheerContainer]}>
                <Text style={{ color : cheerColor, fontSize: 16 }}>
                    { Object.keys(likes).length }
                </Text>
                <Icon
                    size={22}
                    color={cheerColor}
                    onPress={() => onLikePress && onLikePress(feedItem)}
                    name="human-handsup" />
            </TouchableOpacity>
        </View>
    )
};

const styles = StyleSheet.create({
    container : {
        backgroundColor: '#fff',
        margin: 5,
        marginBottom: 0,
        borderWidth: 0,
        borderColor: '#ddd',
        borderWidth: 1,
        borderRadius: 5
    },
    spaceApart: {
        justifyContent: 'space-between'
    },
    cheerContainer : {
        flex: 1,
        justifyContent:'flex-end'
    },
    horizontalRow: {
        flexDirection: "row",
        alignItems: "center"
    },
    pullRight: {
        alignSelf: 'flex-end'
    },
    footerAction : {
        paddingRight: 10
    },
    wrapper : {
        
    },
    commentText: {
        color: '#8c8c8c',
    },
    messageTime: {
        color: '#8c8c8c',
        fontSize: 11,
        alignSelf: "flex-end",
        textAlign: 'right',
    },
    padLeft: {
        paddingLeft: 10
    }
});