import React, {Component} from 'react';
import {
    Image, 
    Text,
    View,
    StyleSheet,
    Dimensions
} from 'react-native';

import { assets } from '../../../assets';

export const PictureMessage  = ({message}) => {
    const url = { uri: message.pictureUrl, cache : 'force-cache' };
    if(!message.pictureUrl) {
        return null;
    }
    return (
        <View style={styles.container}>
            <Image
                progressiveRenderingEnabled={true}
                defaultSource={assets.logos.motus.blue.text}
                resizeMode="cover" 
                style={styles.image} 
                source={url} />
        </View>
    )
}

const styles = StyleSheet.create({
    container : { 
        backgroundColor: 'white',
        flexDirection: "row",
        minHeight: (Dimensions.get('window').width - 12),
        minWidth : (Dimensions.get('window').width - 12)
    },
    image : {
        flex: 1,
        alignSelf: 'stretch',
        width: undefined,
        height: undefined
    }
});