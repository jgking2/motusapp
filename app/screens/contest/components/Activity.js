import React, {Component} from 'react';
import {
    Text,
    View,
    StyleSheet
} from 'react-native';
import { BlurView } from 'react-native-blur';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { config } from '../../../services/metrics';
import { colors } from '../../../components';

const iconMap = {
    run : 'run-fast',
    biking : 'bike',
    steps : 'walk'
};

export const ActivityMessage = ({ message }) => {

    const { metric } = message;

    let bothValues = (!!metric.value && !!metric.duration);
    return (
        <View style={styles.container}>
            <Icon
                size={24}
                style={styles.icon}
                name={iconMap[metric.key] || iconMap.steps} />
            
            <View style={{flexDirection:"row"}}>
                <MeasureValue 
                    metric={metric}
                    />
                { 
                    bothValues 
                    && <Text style={[
                        styles.primary, 
                        styles.divider]}>/</Text> 
                    || null 
                }
                <Duration
                    duration={metric.duration} />
            </View>            
        </View>
    )
};

const MeasureValue = ({ metric, unit }) => {
    if(!metric || !metric.value) return null;
    const { value, type, key } = metric;
    console.log(metric, unit);
    const digits = type === config.keys.count? 0 : 1;
    const suffix = type === config.keys.distance? 
        'miles' : (type === config.keys.count)? 
        key : 'lbs';

    return (
        <View style={styles.row}>
            <Text style={styles.primary}>
                { (+value).toFixed(digits) }
            </Text>
            <Text style={styles.unit}> 
                {suffix}
            </Text>
        </View>
    )
};

const Duration = ({ duration }) => {
    if(!duration) return null;
    return (
        <View style={styles.row}>
            <Text style={styles.primary}>{(+duration).toFixed(0)}</Text>
            <Text style={styles.unit}>minutes</Text>
        </View>
    )
};

const styles = StyleSheet.create({
    blurStyle : {
        position: 'absolute',
        top: 0,
        right: 0,
        left: 0,
        bottom: 0,
        borderRadius: 5
    },
    container: {
        paddingRight: 10,
        paddingLeft: 10,
        borderRadius: 3,
        flexDirection: 'row',
        alignItems: 'center'
    },
    icon : {        
        color : colors(.8).primary,
        borderRadius: 23,
        paddingRight: 9,
        marginRight: 5
    },    
    divider: {
        paddingLeft: 5,
        paddingRight: 5,
        paddingBottom: 2.5,
        color: 'rgba(0,0,0,.2)'
    },
    primary : {
        // color: 'white',
        fontSize : 28,
        fontWeight : '400'
    },
    unit: {
        // color: 'white',
        fontSize: 14,
        padding: 5,
        paddingRight: 0,
        fontWeight: '300'
    },
    row : { 
        flexDirection : 'row',
        alignItems: 'flex-end'
    }
});