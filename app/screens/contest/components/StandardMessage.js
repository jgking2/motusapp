import React from 'react';
import { 
    View, 
    Text, 
    StyleSheet,
    Dimensions
} from 'react-native';
// import Icon from 'react-native-vector-icons/Ionicons';
// <Icon 
// style={styles.alignRight} 
// color={"rgba(0,0,0,.3)"}
// size={30}
// name={"ios-quote"} />
// import moment from 'moment';

export const StandardMessage = ({ message }) => {
    return (
        <View style={styles.container}>

            <Text selectable style={styles.message}>
                {message.content}
            </Text>
        </View>
    )
};

const styles = StyleSheet.create({
    container: {
      flex: 1,
      flexDirection: 'row',
      alignItems: "center",
      padding: 10
    },
    message : {
        backgroundColor: "transparent",
        flex: 1,
        fontSize: 16,
        fontWeight: '100'
    },
    alignRight: {
        flexShrink: 10
    }
  });

