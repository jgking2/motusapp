import React, {PureComponent, Component} from 'react';
import {
    KeyboardAvoidingView, 
    Text, 
    FlatList, 
    View,
    StyleSheet,
    TouchableWithoutFeedback,
    ActivityIndicator
} from 'react-native';

import { Icon } from 'react-native-elements';

import dismissKeyboard from 'dismissKeyboard';
import {connect} from 'react-redux';

import autobind from 'autobind-decorator';

import {
    FeedItem
} from './components';
import {
    ContestInput
} from './inputs';
import {
    feedTypes,
    loadFeed,
    removeFeedSubscription,
    submitPicture,    
    submitMessage,
    toggleFeedLike
} from '../../services/feed';
import { text } from '../../core';

const mapStateToProps = ({ 
    feedState, 
    contestState : { contests, loading },
    authState : {
        user
    },
    profileState : { 
        profile
    }
}) => ({ feedState, contests, loading, profile, user });

const mapDispatchToProps = dispatch => {
    return {
        getContestFeed : (contestId) => dispatch(loadFeed(contestId)),
        sendMessage : (contest, message) => dispatch(submitMessage(contest, message)),
        sendPicture : (contest, message) => dispatch(submitPicture(contest, message)),
        toggleLike : (feedItem, contestId) => dispatch(toggleFeedLike(feedItem, contestId))
    }
};

const defaultFeed = [];

@connect(
    mapStateToProps, 
    mapDispatchToProps
)
export class ContestScreen extends Component {
    componentWillReceiveProps(props) {
        const { 
            navigation : {
                setParams,
                state: {
                    params : {
                        contestId,
                        contest
                    }
                }
            },
            contests
        } = props;

        if(!contest && contests[contestId]) {
            setParams({
                contest : contests[contestId] 
            });
        }
    }

    componentWillMount() {
        const { 
            getContestFeed, 
            navigation
        } = this.props;        
        const contestId = navigation.state.params.contestId;
        this.setState({
            contestId
        });
        getContestFeed(contestId);
    }

    render() {
        const { contestId } = this.state;
        const { feedState, loading } = this.props;

        if(loading) {
            return <Text>Loading...</Text>
        }
        
        const feedItems = feedState.feeds[contestId] || defaultFeed;
        return (
            <KeyboardAvoidingView 
                behavior="padding" 
                style={{
                    flex:1,                
                    flexDirection:'column'
                }}
                contentContainerStyle={{
                    flex:1,
                    flexDirection: "column",
                    justifyContent: "center",
                    alignItems: "stretch",                
                    backgroundColor: 'blue'
                }}
                keyboardVerticalOffset={64}>
                <FlatList
                    ListEmptyComponent={NoContestFeed}
                    keyExtractor={this._extractKey}
                    data={feedItems}
                    initialNumToRender={5}                    
                    ListFooterComponent={<View style={{height: 50}}></View>}
                    renderItem={this._renderMessage}/>
                <ContestInput onSubmit={this._onMessageSubmission} />
            </KeyboardAvoidingView>
        )
    }

    @autobind
    _extractKey({key}) {
        return key;
    }

    @autobind
    _renderMessage({item}) {
        const { contestId } = this.state;
        const { user, contests } = this.props;
        return (
            <FeedItem 
                onCommentPress={this._onCommentPress}
                onLikePress={this._onLikePress}
                contest={contests[contestId]}
                message={item} 
                currentUser={user} />
        )
    }

    @autobind
    _onCommentPress(feedItem) {
        const { contestId } = this.state;
        const { 
            navigation: { navigate }, 
        } = this.props;

        navigate('feedDetail', {
            contestId,
            feedId : feedItem.key
        });
    }

    @autobind
    _onLikePress(feedItem) {
        
        const { 
            navigation : { 
                state : { 
                    params : {
                        contest,
                        contestId
                    }
                }
            },
            toggleLike 
        } = this.props;
        toggleLike(feedItem, contest);
    }

    @autobind
    _onMessageSubmission(message) {
        const {
            sendMessage,
            sendPicture,
            navigation : {
                state : { 
                    params : { 
                        contest 
                    }
                }
            }
        } = this.props;

        let dispatcher = sendMessage;
        switch(message.type) {
            case feedTypes.IMAGE:
                dispatcher = sendPicture;
                break;
            default:
                break;
        }
        dispatcher(contest, message);
    }
}

ContestScreen.navigationOptions = ({ navigation }) => {
    let { 
        navigate,
        state : {
            params : {
                contest 
            },
        } 
    } = navigation;

    return {
        title: `${contest && contest.name || ''}`,
        headerRight: <Icon 
            color={"white"}
            name={"settings"} 
            onPress={() => navigate('contestSettngs', { contest })}/>
    }
};

const NoContestFeed = (props) => {
    return (
        <View>
            <Text>{text.missingContestFeed}</Text>
        </View>
    )
};