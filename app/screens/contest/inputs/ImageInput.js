import React, { PureComponent } from 'react';
import { 
    Text,
    View,
    TextInput,
    TouchableOpacity,
    Image,
    StyleSheet,
    Dimensions,
    Button
} from 'react-native';
import SafeAreaView from 'react-native-safe-area-view';
import { connect } from 'react-redux';

import { withNavigation } from 'react-navigation';

import { withModal } from '../../../hoc';
import { 
    FetchImage, 
    MotusButton,
    colors,
    motusStyles
} from '../../../components';
import { 
    submitPicture, 
    feedTypes 
} from '../../../services/feed';

const mapDispatchToProps = dispatch => ({
    submitMessage : (contest, message) => dispatch(
        submitPicture(contest, message)
    )
});



@connect(undefined, mapDispatchToProps)
@withModal()
@withNavigation //To get the contest to save.
export class ImageInput extends PureComponent {
    state = {};

    render() {
        const { close } = this.props;
        const { path, data, text } = this.state;
        const source = {
            uri : path
        };
        return (
            <SafeAreaView style={[styles.container, motusStyles.padded]}>
                <Text style={motusStyles.missingText}>
                    Share something inspiring!
                </Text>
                <FetchImage onSelect={this._onImageSelection.bind(this)}>
                    {  
                        path &&
                        <Image 
                        source={source} 
                        style={styles.stagedImage} /> ||
                        <View style={[
                            styles.centerContent,
                            motusStyles.roundedEdge,
                            styles.stagedImage, 
                            styles.placeholder
                        ]}>
                            <Text style={{color:"white"}}>Select an image to upload</Text>
                        </View>
                    }
                    
                </FetchImage>
                {/* <TextInput 
                    onChangeText={this._onTextChange.bind(this)} placeholder={"Caption image"} /> */}
                <View style={{flex:1, justifyContent:"flex-end"}}>
                    {   !!path && 
                        <MotusButton 
                            onPress={this.saveImage.bind(this)}
                            text={"Post Photo"} /> || 
                        null 
                    }
                    <Button 
                        color={colors(.8).primary} 
                        onPress={close} 
                        title={"Close"} />
                </View>
            </SafeAreaView>
        )
    }

    saveImage() {
        const { data, path } = this.state;
        const { 
            close,
            submitMessage,
            navigation : {
                state : {
                    params : {
                        contest
                    }
                }
            }
        } = this.props;
        submitMessage(contest, { uri : path  });
        close();
    }

    _onTextChange(text) {
        this.setState({
            text
        });
    }

    _onImageSelection({ data, path }) {
        this.setState({
            data,
            path
        });
    }
}

let { width } = Dimensions.get("window");
//Account for padding
width = width - 20;

const styles = StyleSheet.create({ 
    container : {
        flex: 1,
        paddingTop: 25
    },
    stagedImage : {
        width,
        height: width,
        backgroundColor: 'rgba(0,0,0,.3)'
    },
    centerContent : {
        justifyContent : "center",
        alignItems: "center"
    }
});
