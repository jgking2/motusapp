import React, { Component } from 'react';
import Icon from 'react-native-vector-icons/Ionicons';
import {
    View,
    KeyboardAvoidingView,
    TextInput,
    StyleSheet,
    LayoutAnimation,
    TouchableOpacity,
    Text,
    Animated,
    Keyboard,
    Easing
} from 'react-native';
import { Avatar } from 'react-native-elements';

import autobind from 'autobind-decorator';
import { BlurView } from 'react-native-blur';
import Swiper from 'react-native-swiper'

import { ImageInput } from './ImageInput';
import { ActivityInput } from './activity/ActivityInput';
import { paddingBottom } from '../../../core';

import { 
    MotusButton,
    colors 
} from '../../../components';
import { feedTypes } from '../../../services/feed';

{/* <Swiper
loop={false}
ref={ref => this.swiper = ref} >
</Swiper> */}

const additionalKeyboardSpace = 75;

const textBoxPlaceholder = 'Share something motivating!';

@autobind
export class ContestInput extends Component {
    input: AutoGrowingTextInput;
    state = {
        translateY : new Animated.Value(0),
        showActivity: false,
        message: undefined
    };

    componentWillMount() {
        this.keyboardWillShowSub = Keyboard
            .addListener("keyboardWillShow", this.keyboardWillChange.bind(this));

        this.keyboardWillHideSub = Keyboard
            .addListener("keyboardWillHide", this.keyboardWillChange.bind(this));
    }

    componentWillUnmount() {
        this.keyboardWillHideSub.remove();
        this.keyboardWillShowSub.remove();
    }

    keyboardWillChange = ({ duration, endCoordinates, startCoordinates }) => {
        const { screenY : firstHeight } = startCoordinates;
        const { screenY } = endCoordinates;
        const { translateY } = this.state;
        //Floor it at 0.
        let toValue = Math.min(screenY - firstHeight, 0);
        if(toValue < 0) {
            toValue -= additionalKeyboardSpace;
        }

        Animated.timing(translateY, {
            toValue,
            duration,
            // easing : Easing.inOut(val => 10),
            useNativeDriver : true
        }).start();
    };
        
    shouldComponentUpdate(nextProps, nextState) {
        const { message, showActivity } = nextState;
        const {
            message: currMessage,
            showActivity: currActivity
        } = this.state;

        const activityToggled = showActivity != currActivity;
        let messageExistanceChange = !!message != !!currMessage;
        return messageExistanceChange || activityToggled;
    }
    
    render() {
        const { message, showActivity, translateY } = this.state;
        const { contest } = this.props;

        return (
            <Animated.View
                behavior={"padding"}
                keyboardVerticalOffset={67}
                style={[
                    styles.container,
                    { transform : [
                        { translateY : translateY }
                    ] }
                ]}>
                <KeyboardAvoidingView 
                    keyboardVerticalOffset={64} behavior={"padding"}>
                <BlurView
                    blurType="light"
                    style={styles.blurStyle} />
                
                    { this.renderControls() }
                    { this.renderTextInput() }
                </KeyboardAvoidingView>
            </Animated.View>
        )
    }

    renderTextInput() {
        return (
            <View style={styles.inputContainer}>
                <View style={{ flex: 1, flexDirection:"row" }}>
                <TextInput
                    ref={(ref) => this.input = ref}
                    style={{flex: 1}}
                    multiline={true}
                    // onSi
                    onChangeText={this._onChangeText}
                    placeholderTextColor={"rgba(0,0,0,.5)"}
                    placeholder={textBoxPlaceholder} />
                    
                </View>
                {
                    <TouchableOpacity 
                        style={{
                            alignSelf:"center"
                        }} 
                        onPress={this._handleSubmit.bind(this)}>
                        <Icon
                            color={colors(1).primary}
                            size={30}
                            name={"ios-send"} />
                            
                    </TouchableOpacity>
                }
            </View>
        )
    }

    renderControls() {
        return (
            <View style={styles.controlsContainer}>
                <View style={styles.section}>
                    <ImageInput>
                        <Icon
                            size={30}
                            name={"ios-camera"}
                            style={styles.iconStyle} />
                    </ImageInput>
                </View>
                <View style={styles.section}>
                    <ActivityInput>
                        <Icon
                            size={30}
                            name={"ios-pulse"}
                            style={styles.iconStyle}
                        />
                    </ActivityInput>
                </View>
                <View style={styles.section}>
                    <Icon
                        size={30}
                        name={"ios-text"}
                        onPress={
                            this._onTextIconPress.bind(this)
                        }
                        style={styles.iconStyle}
                    />
                </View>
            </View>
        )
    }

    _onTextIconPress() {
        this.input.focus();
    };

    @autobind
    _onChangeText(text) {
        this.setState({
            message: text
        });
    }

    _onPictureSelection({ data, path: uri }) {
        const type = feedTypes.IMAGE;
    }

    _handleSubmit() {
        const { onSubmit } = this.props;
        const { message } = this.state;
        onSubmit && onSubmit(message);
        this.input.setNativeProps({ text: '' });
        this.setState({
            message: undefined
        });
    }
}

const bottomOffset = paddingBottom || 15;

const styles = StyleSheet.create({
    container: {
        maxHeight: 40,
        backgroundColor: colors(.7).primary,
        position: 'absolute',
        bottom: 0,
        left: 0,
        right: 0
    },
    controlsContainer: {
        flex: 1,
        alignItems: "center",
        flexDirection: 'row',
        // paddingBottom : bottomOffset
    },
    blurStyle: {
        borderRadius: 5,
        position: "absolute",
        top: 0,
        bottom: 0,
        left: 0,
        right: 0
    },
    iconStyle: {
        flex: 1,
        alignSelf: "center",
        textAlignVertical:"center",
        backgroundColor: "transparent",
        color: 'rgba(256,256,256,.8)'//colors(1).primary
    },
    section: {        
        flex: 1,
        // justifyContent: "flex-end",
        alignItems: "center"
    },
    inputContainer: {
        flex: 1,
        flexDirection: 'row',
        padding: 10,
        paddingBottom: 50,
        borderRadius: 5,
        backgroundColor: 'rgba(256,256,256,.5)'
    },
    submitButton: {
        flexShrink: 10
    }
});
