import React, { PureComponent } from 'react';
import { 
    Text,
    View,
    TextInput,
    TouchableOpacity,
    StyleSheet,
    Button,
    ScrollView,
    ActivityIndicator,
    KeyboardAvoidingView
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import SafeAreaView from 'react-native-safe-area-view';
import Swiper from 'react-native-swiper';
import { ButtonGroup } from 'react-native-elements';
import { connect } from 'react-redux';

import { withModal } from '../../../../hoc';
import {
    MotusButton, 
    MotusBottomButton,
    colors,
    motusStyles,
    MotusTabs
} from '../../../../components';
import { 
    saveActivityMetric,
    saveBioMetric 
} from '../../../../services/metrics';

import { ActivityMetric } from './ActivityMetric';

const config = {
    biometrics : [
        'weight',
        'bodyfat'
    ]
};

const mapStateToProps = ({ 
    metricsState,
    contestState : {
        contests
    }
}) => ({ metricsState, contests });
const mapDispatchToProps = dispatch => ({
    saveActivity : (metric, contests) => dispatch(saveActivityMetric(metric, contests)),
    saveBiometric : (metric, contests) => dispatch(saveBioMetric(metric, contests))
});

@withModal()
@connect(mapStateToProps, mapDispatchToProps)
export class ActivityInput extends PureComponent {
    state = {
        selectedIndex: 0,
        tempId : 0,
        metric : undefined
    };

    swiper : Swiper;

    render() {

        const { close, metricsState } = this.props;
        const { selectedIndex, metric, tempId } = this.state;
        const {
            status, 
            error 
        } = (metricsState[tempId] || {});
        //Save has been submitted, let's let the user know how it's going.
        if(status === 'SUCCESS') {
            close();
            return null;
        }

        return (
            <View style={{flex:1}}>
                <SafeAreaView 
                    style={{
                        justifyContent:"flex-end",
                        flexDirection: "row"
                    }}>
                    <Icon 
                        size={34}
                        color={colors(1).primary}
                        style={styles.closeIcon}
                        name="ios-close"
                        onPress={close}
                    />
                    {/* <Button 
                        color={colors(.8).primary} 
                        onPress={close} 
                        title={"Close"} /> */}
                </SafeAreaView>
                <MotusTabs
                    selectedIndex={selectedIndex}
                    onPress={this.changePage.bind(this)}
                    buttons={['Activity', 'Composition']} />
                <Swiper
                    showsPagination={false}
                    loadMinimal={true}
                    onIndexChanged={this.onSwipe.bind(this)}
                    loop={false}
                    ref={(ref) => this.swiper = ref}>
                    <ActivityMetric 
                        onActivityReady={this.activityReady.bind(this)} /> 
                    <BioMetricInputSection />
                    
                </Swiper>
                { 
                    error && <Text>Unable to save, we are looking into this.</Text>
                }
                <KeyboardAvoidingView 
                    behavior={"position"}
                    // keyboardVerticalOffset={54}
                    >
                    <MotusBottomButton 
                        disabled={(status === 'SAVING' || !metric)} 
                        onPress={this.save.bind(this)} text="Save" />                                     
                </KeyboardAvoidingView>                
            </View>
        )
    }

    activityReady(metric) {
        this.setState({ 
            metric
        });

        // console.log(metric);
    }

    changePage(selectedIndex) {
        this.setState({
            selectedIndex
        });
        const scrollBy = (selectedIndex + 1) % 2? -1 : 1;
        this.swiper.scrollBy(scrollBy, true);
    }

    onSwipe(selectedIndex) {
        this.setState({
            selectedIndex
        });
    }

    save() {
        const { metric } = this.state;
        const { saveActivity, contests } = this.props;
        const { tempId } = saveActivity(metric, Object.keys(contests).map(key => contests[key]));

        this.setState({
            tempId
        });
    }
}


const BioMetricInputSection = () => {
    return (
        <View>
            <Text style={[motusStyles.missingText, motusStyles.padded]}>
                Coming soon...
            </Text>
        </View>
    )
};

const styles = StyleSheet.create({
    container : {
        flex: 1,
        paddingTop: 25
    },
    closeIcon : {
        marginRight: 15,
    },
    categorySelected : {
        backgroundColor: 'blue'
    }
});
