import React, { PureComponent } from 'react';
import { 
    TouchableOpacity,
    Text,
    TextInput, 
    View,
    StyleSheet
} from 'react-native';

import { 
    colors,
    MotusInput
 } from '../../../../components';

const activities = {
    'biking' : {
        name: 'Biking',
        type : 'distance',
        placeholder: 'Distance',
        keyboardType : 'decimal-pad',
        duration: true,
        either: true
    },
    'run' : {
        name : 'Run',
        type : 'distance',
        placeholder: 'Distance',
        keyboardType : 'decimal-pad',
        duration : true,
        either : true
    },
    'steps' : {
        name : 'Steps',
        keyboardType : 'number-pad',
        placeholder : 'Number of steps',
        type : 'count'
    }
};

export class ActivityMetric extends PureComponent {
    input : TextInput;

    componentWillMount() {
        const categories = Object
            .keys(activities)
            .map(key => {
                return {
                    key,
                    ...activities[key]
                }                
            });

        this.setState({
            categories
        });
    }

    state = {
        value : undefined,
        categories : [],
        selectedIndex : 0
    };

    render() {
        const { 
            selectedIndex, 
            categories 
        } = this.state;

        const selectedCategory = categories[selectedIndex];        
        return <View style={{padding: 10}}>
            <View style={{flexDirection:"row"}}>
                {
                    categories.map((category, index) => {
                        const selected = (index === selectedIndex);
                        return (
                            <TouchableOpacity 
                                style={{
                                    flex: 1, alignItems:"center"
                                }}
                                onPress={this.selectCategory.bind(this, index)}>
                                <Text style={[styles.subTab,
                                    selected && styles.subTabSelected]}>      {category.name}
                                </Text>
                            </TouchableOpacity>
                        )
                    })
                }
            </View>
            <View style={styles.bottomSection}>
                <MotusInput
                    style={styles.input}
                    inputRef={ref => this.input = ref}
                    onChangeText={this.stageValue.bind(this, selectedCategory.type)}
                    keyboardType={selectedCategory.keyboardType}
                    onBlur={this.setValue.bind(this)} 
                    placeholder={selectedCategory.placeholder} />
                { 
                    selectedCategory.duration && 
                    <MotusInput 
                        style={styles.input}
                        onChangeText={this.stageDuration.bind(this)}
                        onBlur={this.setValue.bind(this)} 
                        placeholder="Total time"
                        keyboardType={"decimal-pad"} />
                }
            </View>
        </View>
    }

    stageValue(type, value) {
        this.setState({
            value,
            type
        });
        this.setValue();
    }

    stageDuration(duration) {
        this.setState({
            duration
        });
        this.setValue();
    }

    setValue() {
        const { 
            value, 
            type, 
            duration,
            categories,
            selectedIndex
        } = this.state;
        const { onActivityReady } = this.props;
        if(value || type || duration) {
            onActivityReady && onActivityReady({  
                value,
                type,
                duration,
                key : categories[selectedIndex].key
            });
        }
    }

    selectCategory(selectedIndex) {
        this.input.clear();
        this.setState({
            selectedIndex
        });
    }
};

const styles = StyleSheet.create({
    subTab : {
        fontSize: 16,
        fontWeight: '100',
        color: 'rgba(0,0,0,.4)'
    },
    subTabSelected : {
        fontSize: 16,
        fontWeight: '400',
        color : colors(1).primary 
    },
    input : {
        textAlign: "center",
        padding: 10,
        fontSize: 24
    },
    bottomSection: {
        marginTop: 25
    }
});