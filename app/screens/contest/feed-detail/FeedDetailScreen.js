import React, { Component } from 'react';

import { connect } from 'react-redux';
import { 
    View,
    Text,
    ScrollView,
    Button,
    FlatList,
    ActivityIndicator,
    TextInput,
    KeyboardAvoidingView,
    StyleSheet
} from 'react-native';

import { FeedItem, MessageInput } from '../components';
import { 
    Timestamp, 
    motusStyles 
} from '../../../components';
import { paddingBottom } from '../../../core';
import {  toggleFeedLike } from '../../../services/feed';
import {
    submitFeedComment,
    subscribeToFeedComments,
    unsubscribeToFeedComments
} from '../../../services/feed-comments';



const defaultComments = [];

const mapStateToProps = ({
    feedState : { feeds },
    feedCommentsState,
    contestState : { contests, loading },
    authState : {
        user
    }
 }) => ({ feeds, feedCommentsState, contests, user });

const mapDispatchToProps = dispatch => ({
    toggleLike : (feedItem, contest) => dispatch(toggleFeedLike(feedItem, contest)),
    submitComment : (comment, feedItem, contest) => dispatch(submitFeedComment(comment, feedItem, contest)),
    subscribeToComments : (feedId, contestId) => dispatch(subscribeToFeedComments(feedId, contestId)),
    unsubscribeToComments : () => dispatch(unsubscribeToFeedComments())
});

@connect(mapStateToProps, mapDispatchToProps)
export class FeedDetailScreen extends Component {

    componentWillUnmount() {
        const { unsubscribeToComments } = this.props;
        unsubscribeToComments();
    }

    componentWillMount() {
        const {
            subscribeToComments,
            feeds,
            contests,
            navigation : {
                state: {
                    params : {
                        contestId,
                        feedId
                    }
                }
            }
        } = this.props;

        const feed = feeds[contestId]
            .filter(feed => feed.key === feedId)[0];
        const contest = contests[contestId];

        subscribeToComments(feed.key, contest.key);
        this.setState({
            feedId,
            feed,
            contest
        });
    }

    render() {
        const { 
            feedId,
            contest 
        } = this.state;

        const {
            user,
            toggleLike,
            feeds,
            feedCommentsState
        } = this.props;

        const feed = feeds[contest.key]
            .filter(feed => feed.key === feedId)[0];

        const comments = feedCommentsState[feed.key] || defaultComments;
        return (
            <View style={{flex:1}}>
                <ScrollView>
                    <FeedItem 
                        onLikePress={this._submitLike.bind(this)}
                        currentUser={user}
                        message={feed}
                        contest={contest} />
                    <Comments 
                        comments={comments} 
                        contest={contest} />
                </ScrollView>
                <KeyboardAvoidingView
                    style={styles.commentContainer}
                    behavior={"padding"}
                    keyboardVerticalOffset={64}>
                    <CommentInput 
                        onSubmit={this._submitComment.bind(this)} />
                </KeyboardAvoidingView>
            </View>
        )
    }

    _submitComment(message) {
        const { feed, contest } = this.state;
        const { submitComment } = this.props;

        submitComment(message, feed, contest);
    }

    _submitLike(feedItem) {
        const { toggleLike } = this.props;
        const { contest } = this.state;
        toggleLike(feedItem, contest);
    }
}

//Move these two components

const Comments = ({ comments, contest }) => {
    if(comments === undefined) {
        return <ActivityIndicator />
    }
    const listData = Object.keys(comments).map(key => comments[key]);
    return <FlatList
                contentContainerStyle={motusStyles.padded}
                data={listData}
                keyExtractor={(item, index) => index}
                renderItem={({item}) => <Comment comment={item} contest={contest} />} />
};

const Comment = ({ contest, comment }) => {
    const { members } = contest;
    const user = members[comment.uid];

    return (
        <View style={[
            motusStyles.roundedEdge, 
            motusStyles.padded,
            styles.comment,
            styles.whiteBg]}>
            <View style={styles.commentTop}>
                <Text 
                    style={styles.commentUsername}>{user.displayName}
                </Text>
                <Timestamp 
                    style={styles.commentTimestamp}
                    date={comment.timestamp} />    
            </View>
            <Text>{ comment.comment }</Text>
        </View>
    )
};

const CommentInput = ({ onSubmit }) => {
    let message = '';
    let input = undefined;
    return (
        <View style={styles.commentBox}>
            <TextInput
                ref={ref => input = ref}
                onChangeText={(msg) => message = msg}
                style={styles.inputBox}
                placeholder={"Got a comment?"}
                multiline={true} />
            <Button
                onPress={() => { 
                    message && onSubmit && onSubmit(message);
                    input.clear();
                }}
                title={"Send"} />
        </View>
    )
};

const styles = StyleSheet.create({
    whiteBg : {
        backgroundColor: 'white'
    },
    commentContainer : {
        backgroundColor: 'white'        
    },
    comment : {
        marginTop: 5
    },
    commentTop : {
        justifyContent: 'space-between',
        alignItems: 'center',
        flexDirection: 'row'
    },
    commentUsername : {
        fontSize: 12,
        color : 'rgba(0,0,0,.8)',
        fontWeight : '300'
    },
    commentTimestamp : {
        fontSize: 10,
        color : 'rgba(0,0,0,.5)',
        alignSelf : 'flex-end'
    },
    commentBox: {
        padding: 10,
        alignSelf: 'stretch',
        flexDirection: 'row',
        paddingBottom
    },
    inputBox : {
        flex: 1
    }
});