import React, {Component} from 'react';
import { PublicStack } from './public';
import { MotusStack } from './AppNavigation';
import {withUserReady} from '../hoc';
import {connect} from 'react-redux';
import {View, Text} from 'react-native';
import { WelcomeScreen } from './welcome';

import { 
    notificationsInitialized 
} from '../services/notifications';
import OneSignal from 'react-native-onesignal';

const uriPrefix = 'motus://';

const mapStateToProps = _ => ({  });
const mapDispatchToProps = (dispatch) => ({
    setNotificationId : (playerId) => dispatch(notificationsInitialized(playerId))
});

@withUserReady({
    unauthorizedScreen: PublicStack
})
@connect(mapStateToProps, mapDispatchToProps)
export class RootScreen extends Component {
    componentWillMount() {
        const { setNotificationId } = this.props;
        OneSignal
            .addEventListener('ids', ({ userId }) => { 
                setNotificationId(userId);
            }); 
    }

    render() {
        // const { userState } = this.props;
        return (
            <View style={{flex:1}}>
                <MotusStack uriPrefix={uriPrefix} />
                <WelcomeScreen />
            </View>
        )
    }
}