import React, {Component} from 'react';
import { 
    StackNavigator, 
    TabNavigator,
    DrawerNavigator
} from 'react-navigation';
import { MotusDrawer } from './MotusDrawer';

import {
    ContestListScreen,
    MessagesScreen,
    PreferencesScreen,
    ActivitiesScreen
} from './index';

import { JoinContestScreen } from './join-contest/JoinContestScreen';
import { 
    ContestScreen, 
    FeedDetailScreen, 
    ContestSettingsScreen 
} from './contest';
import { CreateContestScreen } from './create-contest';
import { HomeScreen } from './home';
import { 
    FitbitScreen, 
    AppleHealthScreen,
    ActivityImportScreen
} from './providers';
import { 
    colors, 
    MotusHeader, 
    headerOptions 
} from '../components';

const sharedNavigationOptions = {
    navigationOptions: {
        header: (props) => <MotusHeader {...props} />,
        ...headerOptions
    }
};

const ContestsStack = StackNavigator({
    contests: {
        screen: ContestListScreen,
        path: 'contests/'
    },
    messages : {
        screen : ContestScreen,
        path: 'contests/:contestId'
    },
    feedDetail : {
        screen: FeedDetailScreen
    },
    contestSettngs: {
        screen : ContestSettingsScreen
    },
    createContest : {
        screen : CreateContestScreen,
        path : 'create-contest/',
        mode: 'modal'
    },
    joinContest: {
        screen: JoinContestScreen,
        path: 'contests/join/:token',
        mode: 'modal'
    }
}, sharedNavigationOptions);

const DashboardStack = StackNavigator({
    home: { 
        screen : HomeScreen 
    }
}, sharedNavigationOptions);

const DeviceLinkStack = StackNavigator({
    activities: {
        screen: ActivitiesScreen
    },
    fitbit : { 
        screen : FitbitScreen ,
        path: 'fitbit'
    },
    applehealth : {
        screen : AppleHealthScreen,
        path: 'apple'
    },
    import : {
        screen : ActivityImportScreen
    }
}, sharedNavigationOptions)

const UserSettingsStack = StackNavigator({
    preferences : { 
        screen : PreferencesScreen
    }
}, sharedNavigationOptions);


export const MotusStack = DrawerNavigator({
    // home: { 
    //     screen : DashboardStack
    // },
    contests: { 
        screen: ContestsStack,
        path: 'main'
    },
    activities: {
        screen : DeviceLinkStack,
        path : 'device'
    },
    preferences: {
        screen: UserSettingsStack,
        path: 'settings'
    }
}, {
    initialRouteName: 'contests',
    drawerOpenRoute: 'DrawerOpen',
    drawerCloseRoute: 'DrawerClose',
    contentComponent: props => <MotusDrawer {...props} />,
    contentOptions: {
        activeTintColor: colors(1).primary
    },
    header: null
});