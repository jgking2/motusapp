import React, { Component } from 'react';
import { View, Text } from 'react-native';

import { connect } from 'react-redux';
import {
    TextFormInput,
    PasswordConfirmFormInput,
    MotusButton
} from '../../../components';

import {
    Form,
    reduxForm,
    Field,
    formValueSelector
} from 'redux-form';

const formName = 'register';

// 
const CredentialRegistration = ({ handleSubmit, submit, invalid, disabled, password }) => {
    
    return (
        <View style={{ 
                padding: 10
            }}>
            <View>
                <Field 
                    autoCapitalize={"words"}
                    label="Full Name"
                    name="displayName"
                    component={TextFormInput} />
            </View>
            {/* <View>
                <Field 
                    autoCapitalize={"words"}
                    label="Last Name"
                    name="lastName"
                    component={TextFormInput} />
            </View> */}
            <View>
                <Field 
                    label="Email" 
                    name="email" 
                    keyboardType="email-address"
                    component={TextFormInput} />
            </View>
            <View>
                <Field
                    secureTextEntry={true}
                    label="Password"
                    name="password"
                    component={TextFormInput} />
            </View>

            {/* <View>
                <Field
                    label="Confirm Password"
                    name="ignore"
                    matchValue={password}
                    component={PasswordConfirmFormInput} />
            </View> */}
            <MotusButton 
                disabled={disabled}
                text={"Submit"}
                onPress={handleSubmit} />
            {/* <Button block bordered disabled={invalid || disabled} onPress={handleSubmit}>
                <Text>Submit</Text>
            </Button> */}
        </View>
    )
};

const selector = formValueSelector(formName);

const mapStateToProps = (state) => ({
    password : selector(state, 'password')
});

// @connect()
export const InitialRegistration = connect(mapStateToProps) (
    reduxForm({
        form: formName,
        validate : () => ({ })}
    )(CredentialRegistration)
);
