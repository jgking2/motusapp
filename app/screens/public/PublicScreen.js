import React, {Component} from 'react';
import {
    View, 
    Text, 
    Button, 
    Alert, 
    TouchableOpacity,
    StyleSheet,
    Platform
} from 'react-native';
import {connect} from 'react-redux';
import { 
    MotusLogo, 
    MotusGradient, 
    MotusButton,
    colors 
} from '../../components';

import autobind from 'autobind-decorator';

const registrationButtonText = {
    en : "Get Started"
};

const loginButtonText = { 
   en : "Already a user?"
};

export class PublicScreen extends Component {
    static navigationOptions = {};

    componentWillMount() { }

    render() {
        return (
            <View style={styles.container}>
                <View style={{height: 320, width: 380}}>
                    <MotusLogo 
                        includeText={true} 
                        textLogoOrientation={"portrait"} 
                        primary={true} />
                </View>
                <View>
                    <Text>
                        Better. Together.
                    </Text>
                </View>
                <View>
                    <MotusButton 
                        onPress={() => this._navigateTo('register')}
                        text={registrationButtonText.en} />
                    <Button 
                        color={colors.secondary}
                        onPress={() => this._navigateTo('login')} 
                        title={loginButtonText.en} />
                </View>
            </View>
        );
    }

    @autobind
    _navigateTo(page) {
        const { navigate } = this.props.navigation;
        navigate(page);
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1, 
        backgroundColor: 'white',
        flexDirection: 'column', 
        justifyContent: "space-around", 
        alignItems: 'center'
    },
    secondaryButton : {
        color: colors.primary
    }
});