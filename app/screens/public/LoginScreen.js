import React, { Component } from 'react';
import {
    View,
    Text,
    Button,
    Alert,
    TouchableOpacity,
    TouchableWithoutFeedback,
    StyleSheet,
    KeyboardAvoidingView,
    Keyboard,
    ScrollView,
    TextInput
} from 'react-native';

import { connect } from 'react-redux';
import { SocialIcon } from 'react-native-elements';
import {
    Control, 
    Form,
    actions,
    Errors
} from 'react-redux-form/native';

import { SafeAreaView } from 'react-navigation';
import autobind from 'autobind-decorator';

import {
    // userAuthRequested,
    loginWithEmail,
    loginWithFacebook
} from '../../services/authentication';

import { 
    MotusLogo,  
    KeyboardReducingImage,
    MotusButton,
    MotusTextInput
} from '../../components';
import { assets } from '../../assets';

const mapStateToProps = ({
    authState: {
        authenticated,
        authenticating,
        error
    } }) => ({ authenticated, authenticating, error });
const mapDispatchToProps = (dispatch) => ({
    submitForm: () => dispatch(actions.submit('login')),
    loginFacebook: () => dispatch(loginWithFacebook()),
    requestEmailAuth: (credentials) => dispatch(loginWithEmail(credentials))
});

@connect(
    mapStateToProps, 
    mapDispatchToProps
)
export class LoginScreen extends Component {

    renderLoginForm() {
        const { submitForm, requestEmailAuth } = this.props;

        return (
            <Form
                model={"login"}
                onSubmit={requestEmailAuth}
                style={{padding: 10, backgroundColor:'white' }}>
                <Control.TextInput
                    label={"Email"}
                    model={".email"}
                    keyboardType="email-address"
                    autoCapitalize={"none"} 
                    autoCorrect={false}
                    component={MotusTextInput}
                    />
                <Control.TextInput
                    label={"Password"}
                    model={".password"}
                    secureTextEntry={true}
                    component={MotusTextInput}
                    />
                
                <MotusButton 
                    text={"Login"}
                    onPress={submitForm} />
            </Form>
        )
    }

    render() {
        const {
            authenticating,
            error
        } = this.props;

        return (
            <KeyboardAvoidingView
            style={{flex:1}}
            behavior="padding">
            <TouchableWithoutFeedback 
                onPress={Keyboard.dismiss} >
            
                <ScrollView
                    contentContainerStyle={styles.container} >                    
                            <KeyboardReducingImage
                                style={{height:310, width:360}}
                                scaleRange={[1,0.5]}
                                source={assets.logos.motus.orange.text.portrait}/>
                            <View style={{ alignSelf: "stretch" }}>
                                {
                                    this.renderLoginForm()
                                }
                                {
                                    error 
                                    && 
                                    <Text>Invalid username/password</Text> 
                                    || undefined
                                }
                            </View>
                            
                            <View 
                                style={{ padding: 20, alignSelf: "stretch"}}>
                                <SocialIcon
                                    type="facebook"
                                    style={styles.buttonStyle}
                                    onPress={this.login}
                                    button
                                    disabled={authenticating}
                                    title="Sign in" />
                                <SocialIcon
                                    type="google-plus-official"
                                    style={styles.buttonStyle}
                                    button 
                                    disabled={authenticating}
                                    title="Sign in" />      
                                </View>    
                </ScrollView>
                </TouchableWithoutFeedback>
            </KeyboardAvoidingView>            
        );
    }

    @autobind
    loginWithPassword(credentials) {
        const { requestEmailAuth } = this.props;
        requestEmailAuth(credentials);
    }

    @autobind
    login() {
        const { loginFacebook } = this.props;
        loginFacebook();
        // if (err) {

        // } else if (res.isCancelled) {

        // } else {
        //     AccessToken.getCurrentAccessToken().then(token => {
        //         const creds = firebaseRn.app().auth.FacebookAuthProvider.credential(token.accessToken);
        //         requestUserAuth(creds);
        //     });
        // }
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: 'white',
        flex: 1,
        justifyContent: "center",
        alignItems: 'center'
    },
    buttonStyle : {
        alignSelf: "stretch"
    }
});