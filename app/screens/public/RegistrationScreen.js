import React, { Component } from 'react';
import { 
    ScrollView, 
    View,
    Text, 
    KeyboardAvoidingView,
    StyleSheet,
    TextInput
} from 'react-native';
import { connect } from 'react-redux';
import autobind from 'autobind-decorator';
import { SocialIcon } from 'react-native-elements';

// import { InitialRegistration } from './components';
import { 
    MotusLogo, 
    KeyboardReducingImage,
    MotusTextInput,
    MotusButton
} from '../../components';

import { registerUser, loginWithFacebook } from '../../services/authentication';
import { assets } from '../../assets';
import {
    Control, 
    Form,
    actions,
    Errors
} from 'react-redux-form/native';
import { FormInput } from '../../core/index';

const formModel = 'register';

const mapStateToProps = () => ({ });
const mapDispatchToProps = (dispatch) => ({
    submitForm : () => dispatch(actions.submit(formModel)),
    register : (user) => dispatch(registerUser(user)),
    registerFb : () => dispatch(loginWithFacebook())
});

@connect(
    mapStateToProps, 
    mapDispatchToProps
)
export class RegistrationScreen extends Component {

    renderRegistrationForm() {
        const { submitForm, register } = this.props;
        return (
            <Form 
                onSubmit={register}
                model={formModel}>
                <Control.TextInput 
                    autoCapitalize={"words"}
                    label="Full Name"
                    model=".displayName"                   
                    autoCorrect={false}
                    component={MotusTextInput} />
                <Control.TextInput 
                    label="Email" 
                    model=".email"
                    keyboardType="email-address"
                    autoCapitalize={"none"} 
                    autoCorrect={false}
                    component={MotusTextInput} />
            <Control.TextInput 
                secureTextEntry={true}
                label="Password"
                model=".password"
                component={MotusTextInput} />
            <MotusButton 
                // disabled={disabled}
                text={"Submit"}
                onPress={submitForm} />
            </Form>
        )
    }

    render() {
        const { registerFb } = this.props;
        return (
            <KeyboardAvoidingView 
                style={{flex:1}}
                behavior={"padding"}>
                <ScrollView 
                    scrollEnabled={false}
                    contentContainerStyle={styles.container}
                    keyboardShouldPersistTaps={"never"}>
                    <KeyboardReducingImage
                        scaleRange={[ 1,.5 ]}
                        source={assets.logos.motus.orange.text.portrait}
                        style={styles.logo}/>
                    <View style={styles.formContainer}>
                        { this.renderRegistrationForm() }
                    </View>
                    <View style={{ padding: 10, alignSelf: "stretch"}}>
                                <SocialIcon
                                    onPress={registerFb}
                                    type="facebook"
                                    style={styles.buttonStyle}
                                    button
                                    title="Continue with Facebook" />
                                <SocialIcon
                                    type="google-plus-official"
                                    style={styles.buttonStyle}
                                    button 
                                    title="Continue with Google" />      
                            </View>          
                </ScrollView>
            </KeyboardAvoidingView>
        )
    }
    
    @autobind
    onRegister(user) {
        const { register } = this.props;
        register(user);
    }
};

const styles = StyleSheet.create({
    container: { 
        flex: 1, 
        flexDirection: 'column', 
        justifyContent: "center", 
        alignItems: 'center',
        backgroundColor: 'white'
    },
    formContainer: { 
        // flex: 1,
        alignSelf: "stretch",
        paddingBottom: 20
    },
    logo: {
        flex: 1,
        height: 320, 
        width: 380
    }
})