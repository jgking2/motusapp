import React, {Component} from 'react';

import { PublicScreen } from './PublicScreen';
import { LoginScreen } from './LoginScreen';
import { RegistrationScreen } from './RegistrationScreen';
import { StackNavigator } from 'react-navigation';
//Simple screen, purely for navigation.

export const PublicStack = StackNavigator({
    Root: {
        screen : PublicScreen
    },
    login : {
        screen : LoginScreen
    },
    register : {
        screen : RegistrationScreen
    }
}, {
    headerMode: "none"
});

// export const PublicStack = withProfileMissing({
//     registrationScreen : ProfileRegistration
// })(PublicNavStack);