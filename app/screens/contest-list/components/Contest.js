import React, {Component} from 'react';
import { 
    View,
    Text,
    TouchableHighlight,
    StyleSheet,
    Platform,
    Dimensions,
    Image
} from 'react-native';
import {connect} from 'react-redux';
import LinearGradient from 'react-native-linear-gradient';
import { BlurView } from 'react-native-blur';
// import { ListItem, Avatar } from 'react-native-elements';
import { AvatarOrInitials, Timestamp, colors } from '../../../components';
import { feedTypes } from '../../../services/feed';
import autobind from 'autobind-decorator';

const messageDefaults = {
    missing : 'No messages yet!',
    [feedTypes.ACTIVITY] : 'Has submitted new ativity!',
    [feedTypes.IMAGE] : 'Sent an image to the contest!'
};

const width = Dimensions.get('screen').width - 20;

const numberOfMessageLines = 1;
const messageElipsisMode = 'tail';

export class Contest extends Component {

    render() {
        let { contest, onTouch, style } = this.props;
        const messages = contest.messages || [];
        const message = messages[messages.length - 1];
        if(!contest) {
            return <Text>Loading...</Text>
        }

        return (
            <TouchableHighlight
                underlayColor={"rgba(12, 12, 12, 0.1)"}
                onPress={() => onTouch && onTouch()}>  
                <View style={[styles.contestContainer, style]}>
                    <ContestImage
                        style={styles.contestImageStyle}
                        photoUrl={contest.photoURL}/>
                    <BlurView
                        blurType="light"
                        style={styles.blurStyle} />
                    {/* <LinearGradient
                        locations={[.05, 1]}
                        start={{x:-3, y: .75}} 
                        style={{                            
                            position:"absolute", 
                            top: 0, 
                            right: 0, 
                            bottom: 0, 
                            left: 0}}
                        colors={['transparent', '#fff']} /> */}
                    <View style={styles.contestTextContainer}>
                        <Text style={[
                            styles.text, 
                            { 
                                textShadowColor:'rgba(0,0,0,.4)',
                                textShadowOffset: {
                                    width: 1,
                                    height: 1
                                },
                                textShadowRadius: 5,
                                color: 'white'
                            }]}>
                            {contest.name}
                        </Text>
                    </View>
                </View>
                
            </TouchableHighlight>
        )
    }

    @autobind
    _extractMessage(message) {
         if(!message) {
             return messageDefaults.missing;
        }
        if(message.content) {
            return message.content;
        }
        return messageDefaults[message.type];
    }
}

const ContestImage = ({ style, photoUrl }) => {
    if(!!photoUrl) {
        return <Image style={style} source={{uri:photoUrl}} />
    }
    else {
        return <View 
            style={[style, { backgroundColor: colors(.5).primary }]} /> //
    }
    const ComponentToRender = !!photoUrl? Image : View;

}

const styles = StyleSheet.create({ 
    contestContainer: {
        // padding: 10,
        overflow : "hidden",
        backgroundColor: '#fff',
        flexDirection: "row",
        justifyContent: "flex-start",
        alignItems: "center",
        borderWidth: StyleSheet.hairlineWidth,
        borderRadius: 5,
        borderColor: '#ddd',
        ...Platform.select({
            ios: {
                shadowColor: 'rgba(0,0,0, .4)',
                shadowOffset: { height: 1, width: 1 },
                shadowOpacity: 1,
                shadowRadius: 1,
            },
            android: {
                backgroundColor: '#fff',
                elevation: 2,
            },
        })
    },
    contestTextContainer: {
        position: "absolute",
        right: 0,
        top: 0,
        left: 0,
        bottom: 0,
        justifyContent: "center",
        alignItems: "center"
    },
    blurStyle: {
        top: 0,
        right: 0,
        left: 0,
        bottom: 0,
        position: 'absolute'
    },
    text: {
        fontSize: 22,
        fontWeight: '600',
        color: 'rgba(0,0,0,.8)',
        backgroundColor: 'transparent'
    },
    contestImageStyle : {
        height: (width / 2) - 70,
        width : width
    }
});