import React, { PureComponent } from 'react';
import {
    FlatList, 
    View, 
    Text, 
    Button, 
    Animated,
    StatusBar,
    StyleSheet,
    TextInput
} from 'react-native';
import { 
    List,
    Icon
} from 'react-native-elements';
import { Header } from 'react-navigation';
import {connect} from 'react-redux';
import autobind from 'autobind-decorator';

import { Contest } from './components';
import { 
    text,
    paddingBottom, 
    InputContainer,
    Form
} from '../../core';
import { 
    MotusMenuToggle,
    MotusBottomButton,
} from '../../components';

const mapStateToProps = ({ 
    feedState, 
    contestState : { 
        contests, 
        loading 
    }
}) => ({ feedState, contests, loading });

@connect(mapStateToProps)
export class ContestListScreen extends PureComponent {
    componentWillMount() {
        StatusBar.setBarStyle("light-content");
    }

    render() {
        const {
            contests,
            loading,
            feedState,
            navigation: {
                navigate,
                state : {                     
                    params = { },
                    ...rest,
                }
            }
        } = this.props;

        if(loading) {
            return <Text>Loading...</Text>
        }

        const contestList = Object.keys(contests).map(key => contests[key]);
        return (
            <View style={styles.container}>
                {/* <Form ref={(ref) => this.formRef = ref}>
                    <InputContainer
                        form={this.formRef}
                        validation={(value) => value.length > 0}
                        changeMethod={"onChangeText"}
                        Component={TextInput} />
                </Form> */}
                <FlatList
                    contentContainerStyle={styles.padBottom}
                    keyExtractor={this._extractKey}
                    data={contestList}
                    ListEmptyComponent={<NoContests />}
                    renderItem={this._renderContest} />
                <MotusBottomButton
                    onPress={() => navigate('joinContest')}
                    text={text.actionInviteCode} />
            </View>
        )
    }

    @autobind
    _goToContest(contest) {
        const{ navigation : { navigate } } = this.props;

        navigate('messages', {
            contest,
            contestId : contest.key
        });
    }

    @autobind
    _renderContest({ item : contest }) {
        
        return (
            <Contest 
                style={styles.contestWrapper}
                onTouch={
                    this._goToContest.bind(this, contest)
                } 
                contest={contest} />
        )
    }

    _extractKey = ({ key }, index) => key || index;
}

ContestListScreen.navigationOptions = ({ navigation }) => {
    const {
        navigate,
        state: {
            params = {}
        }
    } = navigation;

    return {
        drawerLabel: text.titleContest,
        title: text.titleContest,
        headerLeft: (
            <MotusMenuToggle 
                color={"white"}
                onPress={() => navigate("DrawerOpen")} />
        ),
        headerRight: (
            <View style={{flexDirection:"row"}}>
                <Icon
                    containerStyle={{paddingRight:5}}
                    color="white"
                    name="add" 
                    onPress={() => navigate('createContest')}/>   
            </View>  
        )
    }
};

//Probably move elsewhere below
const NoContests = (props) => {
    return (
        <Text>{text.missingContests}</Text>
    )
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white'
    },
    padBottom : {
        paddingBottom : paddingBottom + 40
    },
    contestWrapper: {
        margin: 5,
        marginBottom: 0
    },
    searchContestButton : {
        position: 'absolute',
        bottom: 0,
        right: 0
    }
});