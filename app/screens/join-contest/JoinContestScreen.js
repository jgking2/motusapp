import React, { Component } from 'react';
import { 
    View, 
    Text,
    TextInput,
    Button,
    ActivityIndicator,
    StyleSheet
} from 'react-native';
import { connect } from 'react-redux';
import { 
    FormLabel, 
    FormInput 
} from 'react-native-elements';

import { 
    MotusPayment, 
    MotusButton,
    colors,
    motusStyles
} from '../../components';

import { 
    loadContestFromInvitation,
    joinContest,
    clearLoadedContest,
    contestFeeTypeMap
} from '../../services/contests';

import { text } from '../../core';

const mapStateToProps = ({ 
    contestState : { 
        contests, 
        joining 
    },
    authState : {
        user: { uid }
    }
}) => ({ joining, contests, uid });

const mapDispatchToProps = dispatch => ({
    loadContestFromInvite : (inviteCode) => dispatch(loadContestFromInvitation(inviteCode)),
    dispatchJoinContest : (contestId) => dispatch(joinContest(contestId)),
    clearContest : () => dispatch(clearLoadedContest())
});

//TODO - split this up
//Style this.

//States
//Joining - contest
@connect(mapStateToProps, mapDispatchToProps)
export class JoinContestScreen extends Component {
    static navigationOptions = {
        title : text.titleJoinContest
    }
    state = {
        contestId : undefined
    };
    componentWillMount() {
        const { 
            navigation : {
                state : {
                    params
                }
            },
            loadContestFromInvite
        } = this.props;

        if(params && params.token) {
            this.setState({
                contestId : params.token
            });
        }
    }

    render() {
        const {
            joining : {
                loading,
                error,
                contest
            },
            contests,
            uid
        } = this.props;
        const { contestId } = this.state;

        const currentContest = contests[contestId];

        if(currentContest) {
            const isIndividualEntryFee = (
                currentContest.feeType.type != contestFeeTypeMap.Paid
            );
            const userHasNotPaid = !currentContest.payments[uid];
            const paymentRequired = userHasNotPaid && isIndividualEntryFee;

            if(paymentRequired) {
                return (
                    <View>
                        <Text>{ `This contest has a buy in of ${currentContest.feeType.amount}, please submit payment before proceeding.` }</Text>
                        <MotusPayment
                            amount={currentContest.feeType.amount}
                            identifier={currentContest.key}
                         />
                    </View>
                )
            }

            const text = contest? 
                'Congratulations on the first step! Shall we begin?' : 'Appears you are already a member of this contest!';
            return (
                <View>
                    <Text>Code Entered: {contestId}</Text>
                    <Text>{text}</Text>
                    <MotusButton 
                        text={`Go to ${currentContest.name}`}
                        onPress={this.navigateToContest.bind(this, currentContest)} />
                    <Button title={"Search another"} onPress={this.cancel.bind(this)} />
                </View>
            )
        }

        // if(loading) {
        //     return <ActivityIndicator />
        // }
        const buttonText = loading? 'Searching...' : text.actionSubmitInviteCode;
        if(!contest) {
            return (
                <View style={[{
                    flex:1, 
                    justifyContent: "center",
                    
                    }, motusStyles.padded]}>
                    {
                        <Text
                            style={[ motusStyles.missingText ]}>
                            {error}
                        </Text>
                    }
                    <TextInput 
                        clearButtonMode={"while-editing"}
                        placeholderTextColor={colors(.4).primary}
                        style={styles.input}
                        placeholder={text.placeholderInviteCode}
                        onChangeText={this.setInvitationLookupValue.bind(this)} />
                    <View style={styles.underline}/>
                    <Button 
                        disabled={!contestId || loading}
                        color={colors(1).primary}
                        title={buttonText} 
                        onPress={this.checkCode.bind(this)} />
                
                </View>
            )
        }
        return (
            <View>
                <Text>
                    {contest.name}
                </Text>
                <MotusButton
                    onPress={this.joinContest.bind(this)}
                    text={"Join Contest"} />
                <Button 
                    title={"Cancel"} 
                    onPress={this.cancel.bind(this)} />
            </View>
        )
    }

    cancel() {
        const { clearContest } = this.props;
        this.setState({
            contestId : undefined
        });
        clearContest();
    }

    setInvitationLookupValue(text) {
        this.setState({
            contestId : text
        });
    }

    navigateToContest(contest) {
        const { navigation : { navigate } } = this.props;
        navigate('messages', { 
            contestId : contest.key, 
            contest 
        });
    }

    joinContest() {
        const {
            dispatchJoinContest,
            joining : {
                contest 
            }
        } = this.props;
        dispatchJoinContest(contest.key);
    }

    checkCode() {
        const { contestId } = this.state;
        const { loadContestFromInvite } = this.props;
        loadContestFromInvite(contestId);
    }
}

const styles = StyleSheet.create({
    input : {
        fontSize: 34,
        fontWeight: '100'
    },
    underline : {
        marginTop: 20,
        marginBottom: 20,
        height: StyleSheet.hairlineWidth,
        backgroundColor: colors(.5).primary,
        alignSelf: 'stretch'
    }
});