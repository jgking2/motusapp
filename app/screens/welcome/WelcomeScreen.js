import React, { Component } from 'react';
import { 
    View, 
    Text, 
    Modal, 
    TouchableOpacity,
    StyleSheet 
} from 'react-native';

import { SafeAreaView } from 'react-navigation';
// import { Avatar } from 'react-native-elements';
import Swiper from 'react-native-swiper';
import { connect } from 'react-redux';

import { withModal } from '../../hoc';
import { 
    FetchImage, 
    AvatarOrInitials,
    MotusButton
} from '../../components';
import { 
    updateProfilePicture
} from '../../services/authentication';
import { 
    saveUserProfile
} from '../../services/user-profile';

const mapStateToProps = ({
    authState : { user },
    profileState: { profile, loading }
}) => ({
    user,
    profile,
    loading
});

const mapDispatchToProps = dispatch => ({
    uploadProfilePicture : (url) => dispatch(updateProfilePicture(url)),
    saveProfile : (profile) => dispatch(saveUserProfile(profile))
});

@connect(mapStateToProps, mapDispatchToProps)
export class WelcomeScreen extends Component {
    swiper : Swiper;
    state = {
        dismiss: false
    };
    render() {        
        const { user, profile, loading } = this.props;
        
        const { dismiss } = this.state;
        let avatarProps = {
            large : true,
            rounded: true,
            name: user.displayName,
            photoURL: user.photoURL
        }
        let visible = !loading && (Object.keys(profile || {}).length === 0);
        if(dismiss) {
            visible = false;
        }
        return (
            <Modal visible={visible} animationType="slide">
                <SafeAreaView style={{flex:1}}>
                    <WelcomeHeader user={user} />
                    <Swiper ref={(item) => this.swiper = item} loop={false}>
                        <View style={styles.slideContainer}>
                            <FetchImage
                                onSelect={this.uploadUserProfilePhoto.bind(this)}
                                >
                                <AvatarOrInitials {...avatarProps} />
                            </FetchImage>
                            <Text>
                                Profile Picture
                            </Text>
                        </View>
                        <View>
                            <Text>Phone Number</Text>
                        </View>
                        <View>
                            <Text>Goals</Text>
                        </View>
                    </Swiper>
                    <MotusButton
                        text="Save"
                        onPress={this.saveProfile.bind(this)}
                         />
                    <TouchableOpacity onPress={() => this.setState({ dismiss : true })}>
                        <Text>Maybe Later</Text>
                    </TouchableOpacity>
                </SafeAreaView>
            </Modal>
        )
    }

    uploadUserProfilePhoto({ sourceURL }) {
        const { uploadProfilePicture } = this.props;
        uploadProfilePicture(sourceURL);
    }

    saveProfile(profile) {
        const { saveProfile } = this.props;
        saveProfile({ phone: '123456789' });
    }
}

const PhoneNumber = ({  }) => {

};

const WelcomeHeader = ({ user }) => {
    return (
        <View style={styles.headerContainer}>
            <Text>Welcome to Motus</Text>
            <Text style={styles.headerName}>
                {user.displayName}
            </Text>
            <Text>Tell us a little more about yourself.</Text>
        </View>
    )
}

const styles = StyleSheet.create({
    headerContainer: {
        justifyContent: "center",
        height: 200,
        alignItems: 'center'
    },
    headerName : {
        fontSize: 20
    },
    slideContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    }
})