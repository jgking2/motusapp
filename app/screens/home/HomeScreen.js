import React, { Component } from 'react';
import { 
    View, 
    Text, 
    TouchableOpacity,
    ScrollView,
    StyleSheet,
    ActivityIndicator,
    Image
} from 'react-native';
import { Header } from 'react-navigation';

import { connect } from 'react-redux';

import moment from 'moment';

import { MotusLogo, MotusMenuToggle } from '../../components';
import {
    convert,
    loadMetrics 
} from '../../services/metrics';

const mapStateToProps = ({ 
    metricsState : {
        metrics
    },
    profileState : {
        loading, 
        profile 
    }, authState : { user } 
}) => ({ loading, user, profile, metrics });

const mapDispatchToProps = dispatch => ({
    loadActivity : () => dispatch(loadMetrics())
});

@connect(mapStateToProps, mapDispatchToProps)
export class HomeScreen extends Component {
    
    componentWillMount() {
        const { loadActivity, metrics } = this.props;
        loadActivity();
    }


    get metricsByDate() {
        const { metrics } = this.props;
        return metrics.reduce((prev, currentMetric) => {
            const { timestamp } = currentMetric;
            const key = moment(timestamp).format('YYYY-MM-DD');

            if(!prev[key]) {
                prev[key] = []
            }
            prev[key].push(currentMetric);
            return prev;
        }, {

        });
    }

    render() {
        const { 
            user,
            profile,
            loading,
            metrics
        } = this.props;        

        if(loading) {
            return <ActivityIndicator />
        }
        if(!metrics.length) {
            return (
                <Text>
                    { "No metrics." }
                </Text>
            )
        }
        const groupedMetrics = this.metricsByDate;
        
        return (
            <ScrollView>
            { 
                Object.keys(groupedMetrics)
                    .sort((a, b) => {
                        if(a < b) return 1;   
                        if(b > a) return -1;
                        return 0;
                    })
                    .map(key => {
                        return (
                            <ActivityGroupByDay 
                                key={key}
                                date={key}
                                metrics={groupedMetrics[key]} />
                        )
                    })
            }
            </ScrollView>
        )

        return (
            <View style={{
                flex: 1,
                justifyContent:"center",
                alignItems:"center"
                }}>
                <Text>Submit some activity!</Text>
            </View>
        )
    }
}

HomeScreen.navigationOptions = ({ 
    navigation : { navigate } 
}) => ({
    drawerLabel: 'Dashboard',
    headerLeft: (
        <MotusMenuToggle 
            onPress={() => navigate("DrawerOpen")} />
    )
});

const ActivityGroupByDay = ({ date, metrics }) => {
    //Run - take each
    //Bike - take each
    return (
        <View>
            <Text>
                { moment(date).format('MMMM DD, YYYY') }
            </Text>
            <StepAnalysis metrics={metrics} />
            <RunAnalysis metrics={metrics} />
            <CyclingAnalysis metrics={metrics} />
        </View>
    )
};

const StepAnalysis = ({ metrics }) => {
    const steps = metrics.reduce(
        (prev, { key, type, value }) => {
        if(key === 'steps' || type === 'count') {
            return prev > value? prev : value;
        }
        return prev;
    }, 0);
    
    if(!steps) {
        return (
            <Text>
                { 'No recorded steps.' }
            </Text>
        )
    }
    return (
        <Text>{ steps } </Text>
    )
};

const RunAnalysis = ({ metrics }) => {
    const runs = metrics.filter(({ key }) => {
        return key === 'run';
    });
    return (
        <Text> { JSON.stringify(runs) }</Text>
    )
};

const CyclingAnalysis = ({ metrics }) => {
    const cycling = metrics.filter(({ key }) => {
        return key === 'biking';
    });
    return (
        <Text>{ JSON.stringify(cycling) }</Text>
    )
};

const styles = StyleSheet.create({ 
    header : {
        flexDirection: 'row',
        justifyContent: "center",
        paddingTop: 20
    },
    headerTitle : {
        flex: .43, 
        height: 50
    },
    headerRight : {
        position: "absolute",
        top: 20,
        right: 5,
        height: 50,
        justifyContent: "center"
    }
})