import React, { Component } from 'react';
import { 
    View, 
    Text,
    FlatList,
    Button,
    ScrollView,
    StyleSheet,
    ActivityIndicator,
    TouchableOpacity
} from 'react-native';

import { connect } from 'react-redux';

import { 
    saveMetrics 
} from '../../services/metrics';
import { 
    MotusButton, 
    motusStyles,
    colors
} from '../../components';

const pristineState = 'PRISTINE_STATE';
const success = 'SUCCESS';
const saving = 'SAVING';
const error = 'ERROR';

const mapStateToProps = ({
    metricsState,
    contestState : { contests }
}) => ({ contests, metricsState });

const mapDispatchToProps = dispatch => ({
    saveData: (data, provider, contests) => dispatch(saveMetrics(data, contests, provider))
 });

@connect(mapStateToProps, mapDispatchToProps)
export class ActivityImportScreen extends Component {
    state = {
        selectedContests : {}
    };

    get selectedContests() {
        const { selectedContests } = this.state;
        return Object.keys(selectedContests)
            .map(key => selectedContests[key]);
    }

    get screenState() {
        const { tempId } = this.state;
        const { metricsState } = this.props;
        console.log(metricsState, tempId)
        if(!metricsState[tempId]) {
            return pristineState;
        }
        return metricsState[tempId].status;
    }

    render() {
        const {
            contests
        } = this.props;

        const selecting = !this.selectedContests.length;

        const contestList = Object.keys(contests) 
            .map(key => contests[key]);
                        
        switch(this.screenState) {
            case saving:
                return this.renderLoading();
            case pristineState:
            return (
                <ScrollView 
                    style={[motusStyles.padded, styles.container]}>
                    <Text>
                        Select Contests to share!
                    </Text>
                    <FlatList                    
                        data={contestList}
                        renderItem={this.renderContestItem.bind(this)} />
                    <Button
                        onPress={this.toggleAllContestsSelection.bind(this, selecting)}
                        color={colors(1).primary}
                        title={selecting? "Select All" : "Deselect All"}/>
                    <MotusButton
                        style={styles.buttonBottom}
                        onPress={this.importProviderActivity.bind(this, this.selectedContests)}
                        text={"Import"} />
                </ScrollView>
                )
            case error:
                return (
                    <View>
                        <Text>Something went wrong...</Text>
                    </View>
                )
            case success:
                return (
                    <View>
                        <Text>Done! Go to contests?</Text>
                    </View>
                )
        } 
        
        
    }

    renderLoading() {
        return (
            <ActivityIndicator />
        )
    }

    renderContestItem({ item : contest }) {
        const { selectedContests } = this.state;
        const selected = !!selectedContests[contest.key];
        return (
            <ContestItem 
                onPress={
                    this.selectContest.bind(this, contest)
                }
                selected={selected}
                contest={contest} />
        )
    }

    toggleAllContestsSelection(selecting = true) {
        let update = {};
        if(selecting) {
            const { contests } = this.props;
            update = Object.assign({}, update, contests);
        }
        this.setState({
            selectedContests : update
        });
    }

    selectContest(contest) {
        const { selectedContests } = this.state;
        let selected = Object.assign({}, selectedContests);
        if(selected[contest.key]) {
            delete selected[contest.key];
        }
        else {
            selected[contest.key] = contest;
        }
        this.setState({ selectedContests : selected });
    }

    importProviderActivity(contests = []) {
        const {
            navigation : {
                state : {
                    params
                }
            },
            saveData
        } = this.props;

        const { activity, provider } = params;

        const { tempId } = saveData(
            activity, 
            provider, 
            contests
        );

        this.setState({
            tempId
        });
    }
}

const ContestItem = ({ contest, onPress, selected }) => {
    return (
        <TouchableOpacity onPress={onPress}>
            <View style={[                
                (selected && motusStyles.raised),
                motusStyles.padded,
                styles.contest,
                (selected && styles.contestSelected),       
                motusStyles.roundedEdge]}>
                <Text style={motusStyles.missingText}>
                    { contest.name }
                </Text>
            </View>
        </TouchableOpacity>
    )
};

const styles = StyleSheet.create({
    container : {
        flex : 1
    },
    marginTop: {
        marginTop: 10
    },
    contest : {
        backgroundColor: 'rgba(256,256,256,.2)',
        paddingTop: 20,
        paddingBottom: 20,
        marginTop: 5
    },
    contestSelected : {
        backgroundColor: 'white'
    },
    buttonBottom : {
        position : 'absolute',
        bottom: 0
    }
});