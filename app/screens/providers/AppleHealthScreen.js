import React, { Component } from 'react';
import { 
    View, 
    Text,
    TouchableOpacity,
    StyleSheet,
    Switch
} from 'react-native';
import SafeAreaView from 'react-native-safe-area-view';

import { connect } from 'react-redux';

import { 
    loadLatestData, 
    applehealth 
} from '../../services/providers';
import { 
    config, 
    categories 
} from '../../services/metrics';

import { 
    MotusButton,
    Timestamp,
    Metric,
    colors,
    motusStyles
} from '../../components';
import { select } from 'redux-saga/effects';

const mapStatetoProps = ({ providerState : { 
    linked,
    data,
    local
 } }) => ({
    linked,
    data
});

const mapDispatchToProps = dispatch => ({
    loadHealthkitData : () => dispatch(loadLatestData(applehealth.key))
});

@connect(mapStatetoProps, mapDispatchToProps)
export class AppleHealthScreen extends Component {
    state = {
        selectedActivity : []
    };

    componentWillMount() {
        const { loadHealthkitData } = this.props;
        loadHealthkitData();
    }

    render() {
        const { 
            linked,
            data : {
                applehealth = {}
            },
            navigation : {
                navigate
            }
        } = this.props;
        const { selectedActivity } = this.state;


        const availableActivity = Object.keys(applehealth)
            .filter(key => !!Object.keys(applehealth[key]).length);

        if(!availableActivity.length) {
            return (
                <Text style={[
                    motusStyles.padded, 
                    motusStyles.missingText]}>
                    No data here...
                </Text>
            )
        }

        return (
            <SafeAreaView style={[
                styles.container, 
                motusStyles.padded
            ]}>
                {
                    availableActivity
                        .map(key => {
                            const value = applehealth[key];
                            return (
                                <View key={key}>
                                    <SectionHeader activityKey={key} />
                                    <View style={[
                                        motusStyles.raised, 
                                        motusStyles.roundedEdge,
                                        styles.section]}>

                                        <ActivityEntry 
                                            selected={this.isSelected(value)}
                                            onPress={this.selectActivity.bind(this)}
                                            activity={value} />
                                    </View>
                                </View>
                            )
                        })
                }
                { 
                    selectedActivity.length && 
                    <View style={styles.fixedBottom}>
                        <MotusButton
                            onPress={this.navigateToImport.bind(this)}
                            text={"Import Selected Activity"}/> 
                    </View>
                    || null
                }
            </SafeAreaView>
        )
    }

    isSelected(activity) {
        const { selectedActivity } = this.state;
        return selectedActivity.indexOf(activity) > -1;
    }

    selectActivity(activity) {
        const { selectedActivity } = this.state;
        const index = selectedActivity.indexOf(activity);
        if(index < 0) {
            selectedActivity.push(activity);
        }
        else {
            selectedActivity.splice(index, 1);
        }
        this.setState({
            selectedActivity
        });
    }

    navigateToImport() {
        const { selectedActivity } = this.state;
        const { 
            navigation : {
                navigate
            } 
        } = this.props;

        navigate('import', {
            activity : selectedActivity,
            provider: applehealth.key
        });
    }
}

AppleHealthScreen.navigationOptions = {
    title: 'Apple Health'
};

const SectionHeader = ({ activityKey }) => {
    return (
        <Text style={styles.sectionHeader}>
            {categories[activityKey]}
        </Text>
    )
};

const ActivityEntry = ({ onPress, activity, selected }) => {
    return (
        <TouchableOpacity 
            activeOpacity={1}
            style={styles.activityContainer} 
            onPress={() => onPress(activity)}>
            <View style={styles.activityItem}>
                <View>
                    <Metric 
                        style={styles.activityValue} 
                        metric={activity} /> 
                    <Timestamp
                        date={activity.timestamp} />
                </View>
                <Switch 
                    onTintColor={colors(1).secondary}
                    style={styles.switchStyle}
                    pointerEvents={"none"} 
                    value={selected} />
            </View>
        </TouchableOpacity>
    )
};

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    fixedBottom: {
        flex: 1,
        justifyContent: "flex-end"
    },
    activityContainer : {
        flexDirection: "row"
    },
    section : {
        backgroundColor: '#fff',
        marginTop: 10
    },
    activityItem : {
        padding: 10,
        flex: 1,
        justifyContent : 'space-between',
        flexDirection: "row",
        alignItems: "center"
    },
    activityValue : {
        fontSize: 20,
        fontWeight: "bold"
    },
    switchStyle : {
        // alignSelf: 'flex-end'
    },
    sectionHeader: {
        fontSize: 20,
        // padding : 10,
        fontWeight: "400"
    }
});