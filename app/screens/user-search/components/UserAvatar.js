import React from 'react';
import { 
    StyleSheet,
    Image,
    View,
    TouchableOpacity
} from 'react-native';
import {
    Text
} from 'native-base';
import { AvatarOrInitials } from '../../../components'

export const UserAvatar = (props) => {
    const {
        user : {
            displayName, 
            photoURL 
        },
        onPress,
        user
    } = props;

    if(!photoURL) {
        Avatar = <Text>{'Hey'}</Text>
    }
    return ( 
        <TouchableOpacity onPress={() => onPress && onPress(user)}>
            <View style={styles.container}>
                <AvatarOrInitials photoURL={photoURL} name={displayName} />
                <Text numberOfLines={1} style={{fontSize: 12, maxWidth: 80}}>{displayName}</Text>
            </View>
        </TouchableOpacity>
    )
};

const styles = StyleSheet.create({
    container: {
        padding: 5,
        flexDirection:"column", 
        alignItems:"center"
    },
    avatar: {
        width:30, 
        height:30,
        borderRadius:15
    },
    avatarName: {
        
    }
})