import React from 'react';
import {
    ListItem,
    Thumbnail,
    Left,
    Body,
    Text,
    Right,
    Icon
} from 'native-base';

export const UserSearchResult = (props) => {
    const { onSelect, user, selected } = props;
    const { photoURL, displayName, username } = user;
    const source = { uri : photoURL };
    
    return (
        <ListItem onPress={() => onSelect && onSelect(user)} avatar>
            <Left>
                <Thumbnail small source={source} />
            </Left>
            <Body>
                <Text>
                    { displayName }
                </Text>
                <Text note>
                    { username }
                </Text>
            </Body>
            <Right />
                {/* { selected && <Icon color="blue" name="checkmark" /> }
            </Right> */}
        </ListItem>
    )
}