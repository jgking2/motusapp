import React, {Component} from 'react';
import {
    // Modal,
    View,    
    FlatList,
    TouchableOpacity,
    LayoutAnimation,
    KeyboardAvoidingView
} from 'react-native';
import {
    Container, 
    Header, 
    Footer,
    Title, 
    Right, 
    Body, 
    Left,
    Input,
    Button,
    Item,
    Icon,
    List,
    ListItem,
    Text,
    Thumbnail
} from 'native-base';
import {withModal} from '../../hoc/withModal';
import {connect} from 'react-redux';
import {
    requestUserSearch,
    clearSearchResults
} from '../../services/users';
import { 
    UserAvatar, 
    UserSearchResult 
} from './components';

const mapStateToProps = ({ 
    usersState : users, 
    authState : { 
        user : me 
    } 
}) => ({ users, me });

const mapDispatchToProps = dispatch => {
    return {
        submitUserSearch : (criteria) => dispatch(requestUserSearch(criteria)),
        clearResults : () => dispatch(clearSearchResults())
    }
};

//TODO - lock the submit button at the bottom.
@connect(
    mapStateToProps, 
    mapDispatchToProps
)
export class SearchMotusUsers extends Component {
    state = {};

    componentWillUnmount() {
        const { clearResults } = this.props;
        clearResults();
    }

    //Add check for prop selectedUsers
    componentWillMount() {
        const { selectedUsers } = this.props;
        this.state.selectedUsers = selectedUsers || [];
    }

    // shouldComponentUpdate(nextProps, nextState) {
    //     const listChanged = nextProps.users !== this.props.users;
    //     return listChanged;
    // }

    render() {
        const { selectedUsers } = this.state;
        const {
            submitUserSearch,
            users,
            onUsersSelected,
            me,
            ...addtlProps
        } = this.props;

        const searchResults = users.filter(usr => {
            let notMe = usr.uid !== me.uid;
            return notMe && selectedUsers.indexOf(usr) === -1;
        });

        return (
            <View style={{flex: 1, flexDirection:"column" }}>      
                <View style={{flexGrow:0}}>          
                    <SearchHeader onSearch={submitUserSearch} {...addtlProps} />
                </View>
                {  
                    selectedUsers &&
                
                    <FlatList style={{flexGrow:0}}
                        horizontal={true}
                        data={selectedUsers}
                        renderItem={({item}) => <UserAvatar onPress={this.onUserRemoval.bind(this)} user={item}/>}
                        keyExtractor={this._extractKey}/>
                }
                <List style={{flex:10}}>
                    <FlatList
                        keyExtractor={this._extractKey}
                        data={searchResults}
                        renderItem={this._renderSearchResult.bind(this)}
                        />
                </List>
                { ( selectedUsers.length || undefined) &&
                    <KeyboardAvoidingView style={{flex:20, flexDirection:'column', justifyContent: 'flex-end'}}>
                        <Button onPress={this._onUsersSelected.bind(this)} full>
                            <Text>Invite {selectedUsers.length} friends!</Text>
                        </Button>
                    </KeyboardAvoidingView>
                }
            </View>
        )
    }

    _onUsersSelected() {
        const { onUsersSelected, close } = this.props;
        const { selectedUsers } = this.state;
        onUsersSelected && onUsersSelected(selectedUsers);
        close && close();
    }

    _renderSearchResult({item}) {
        return <UserSearchResult onSelect={this.onUserSelection.bind(this)} user={item} />
    }    

    _updateSelectedUsers(selectedUsers) {
        this.setState({ selectedUsers });
    }

    onUserSelection(user) {
        const { multiSelect } = this.props;
        const { selectedUsers } = this.state;
        if(multiSelect) {
            this._updateSelectedUsers([user, ...selectedUsers])
        }
        else {
            this._updateSelectedUsers([user]);
        }
    }

    onUserRemoval(user) {
        const index = this.state.selectedUsers.indexOf(user);
        const selectedUsers = [...this.state.selectedUsers];
        selectedUsers.splice(index, 1);
        this._updateSelectedUsers(selectedUsers);
    }

    search() {
        let {submitUserSearch} = this.props;
        let {criteria} = this.state;
        
        submitUserSearch(criteria);
    }

    _extractKey = (item, index) => item.uid;
}



//Extracting to prevent list re-render on state change.
class SearchHeader extends Component {
    state = {};

    shouldComponentUpdate(nextProps, nextState) {
        const searchTextNowInput = !!nextState.criteria;
        const searchTextPreviouslyInput = !!this.state.criteria;
        return searchTextNowInput !== searchTextPreviouslyInput;
    }

    render() {
        const { close } = this.props;
        const { criteria } = this.state;
        return (
            <Header searchBar rounded>                                        
                <Item>
                    <Icon name="ios-search"/>
                    <Input onChangeText={this.setCriteria.bind(this)} placeholder="Search" />
                    <Icon name="ios-people"/>
                </Item> 
                { close && //Close only present if it's a modal
                    <Button transparent onPress={() => close()}>
                        <Text>Close</Text>
                        {/* <Icon name="ios-close"/> */}
                    </Button>
                }
            </Header>
        )
    }

    setCriteria(text) {
        const { onSearch, clearResults } = this.props;  

        this.setState({
            criteria: text
        });
        onSearch(text);
    }
}

export const SearchMotusUsersModal = withModal()(SearchMotusUsers)