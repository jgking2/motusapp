import React, {Component} from 'react';
import { 
    StyleSheet,
    Button, 
    View, 
    FlatList, 
    TouchableOpacity,
    Text
} from 'react-native';
import SafeAreaView from 'react-native-safe-area-view';
import { connect } from 'react-redux';
import Icon from 'react-native-vector-icons/Ionicons'

import { UserPreferencesForm } from './UserPreferencesForm';

import {
    MotusMenuToggle,
    AvatarOrInitials,
    FetchImage,
    MotusButton,
    MotusBottomButton,
    motusStyles
 } from '../../components';
 import { 
     updateProfilePicture 
} from '../../services/authentication';
import {
    updateUserProfile,
} from '../../services/user-profile';

const mapStateToProps = ({
    profileState : {
        profile
    },
    authState : { 
        user 
    }
}) => ({ user, profile });

const mapDispatchToProps = dispatch => ({
    // uploadProfilePhoto : (url) => dispatch(updateProfilePicture(url)),
    updateProfile : (profile) => dispatch(updateUserProfile(profile))
});

@connect(mapStateToProps, mapDispatchToProps)
export class PreferencesScreen extends Component {
    state = {
        stagedImage : undefined
    };

    render() {
        const { user, integrations, profile } = this.props;
        const { displayName, photoURL } = user;

        if(!user){
            return (
                <View>
                    <Text>Loading</Text>
                </View>
            )
        }
        return (
            <View style={[ styles.container ]}>                
                <UserPreferencesForm 
                    onSubmit={this.savePersonalInformation.bind(this)} />  
            </View>
        )
    }

    savePersonalInformation(profile) {
        const { updateProfile } = this.props;
        updateProfile(profile);
    }
}

PreferencesScreen.navigationOptions = ({navigation}) => {
    const { navigate } = navigation;

    return {
        title: 'My Account',
        headerLeft: (
            <MotusMenuToggle 
                onPress={() => navigate("DrawerOpen")} />
        )
    }
};

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    personalInformation : {
        padding: 20,
        alignItems: "center"
    },
    fullName : {
        fontSize: 14
    },
    profilePhoto: {
        alignSelf:"center",
        paddingTop: 10,
        paddingBottom: 10
    },
    note: {
        // fontSize: 8,
        
        fontStyle: "italic"
    }
})