import React, { Component } from 'react';
import { View, StyleSheet } from 'react-native';

import { 
    FetchImage,
    AvatarOrInitials
} from '../../components';

export const ProfilePictureUpload = ({ value, name, onChange }) => {        
        const avatarProps = {
            large : true, 
            name,
            rounded: true,
            photoURL : value
        };

        return (
                <FetchImage
                    compressImageQuality={0.5} 
                    onSelect={({ path }) => onChange(path)} >
                <View 
                    style={styles.profilePhoto}       pointerEvents="none">
                    <AvatarOrInitials
                        {...avatarProps} />
                </View>
            </FetchImage>
        )
    
};
// export class ProfilePictureUpload extends Component {
//     render() {
//         const { value, name, onChange } = this.props;
        
//         const avatarProps = {
//             large : true, 
//             name,
//             rounded: true,
//             photoURL : value
//         };
//         console.log(value, name, onChange);
//         return (
//              <FetchImage
//                 compressImageQuality={0.5} 
//                 onSelect={({ path }) => onChange(path)} >
//                 <View style={styles.profilePhoto} pointerEvents="none">
//                     <AvatarOrInitials
//                         {...avatarProps} />
//                 </View>
//             </FetchImage>
//         )
//     }
// };

const styles = StyleSheet.create({
    profilePhoto: {
        alignSelf:"center",
        paddingTop: 10,
        paddingBottom: 10
    }
});