import React, {Component} from 'react';
import { 
    TextInput, 
    View,
    ScrollView, 
    StyleSheet, 
    Picker, 
    TouchableOpacity,
    KeyboardAvoidingView
} from 'react-native';
// import {
//     Item,
//     Input,
//     Label,
//     Button,
//     Text,
//     ListItem,
//     Right,
//     Radio
// } from 'native-base';
import { connect } from 'react-redux';
import { SafeAreaView } from 'react-navigation';
import {
    TextFormInput,
    RadioFormInput,
    FetchImage,
    AvatarOrInitials,
    MotusBottomButton,
    MotusTextInput,
    motusStyles
} from '../../components';
import {
    Control, 
    Form,
    actions,
    Errors
} from 'react-redux-form/native';

const formName = 'profileForm';
import { ProfilePictureUpload } from './ProfilePictureUpload';

const phoneFormatter = (number) => {
  if (!number) return '';
  // NNN-NNN-NNNN
  const splitter = /.{1,3}/g;
  number = number.substring(0, 10);
  return number.substring(0, 7).match(splitter).join('-') + number.substring(7);
};
const phoneParser = (number) => number ? number.replace(/-/g, '') : '';


const mapStateToProps = ({
    authState : { user },
    profileState: { profile }
}) => ({
    initialValues: {
        ...user,
        ...profile
    }
 });

 const mapDispatchToProps = (dispatch) => ({
    setDefaults : (values) => {
        const action = actions.change(formName, values);
        dispatch(action);
    },
    triggerSubmit : () => {
        dispatch(actions.submit(formName))
    }

 });

@connect(mapStateToProps, mapDispatchToProps)
export class UserPreferencesForm extends Component {
    componentWillMount() {
        const { 
            initialValues, 
            setDefaults 
        } = this.props;

        setDefaults(initialValues);
    }

    render() {
        const {  
            initialValues : {
                photoURL,
                displayName,
                username,
                phone
            },
            onSubmit
        } = this.props;
        console.log(username);
        return (
            <Form
                keyboardShouldPersistTaps={"never"}
                style={styles.container}
                onSubmit={onSubmit}
                model={formName} >

                <Control
                    model={".photoURL"}
                    name={displayName}
                    component={ProfilePictureUpload} 
                    />
                <ScrollView
                    style={[styles.container, motusStyles.padded]}>
                    <Control.TextInput
                        label={"Username"}
                        model={".username"}
                        component={MotusTextInput}
                        />
                    <Control.TextInput 
                        label={"Phone Number"}
                        keyboardType="phone-pad"
                        model={".phone"}
                        component={MotusTextInput}
                        />
                </ScrollView>
                <KeyboardAvoidingView
                    behavior={"position"}
                    keyboardVerticalOffset={64}
                >
                    <MotusBottomButton 
                        onPress={this.triggerSubmit.bind(this)} 
                        text={"Save"} />
                </KeyboardAvoidingView>
            </Form>
        )
    }

    triggerSubmit() {
        const { triggerSubmit } = this.props;
        triggerSubmit();
    }
}


const styles = StyleSheet.create({
    container : {
        flex: 1
    },
    sitBottom : {
        flex: 1,
        justifyContent: 'flex-end'
    }
});