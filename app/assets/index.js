import motusBlueTextLandscape from './img/motus-blue-landscape-text.png';
import motusBlueTextPortrait from './img/motos-logo-blue-text-portrait.png';
import fitbit from './img/fitbit-logo.png';
import motusOrangeTextPortrait from './img/motus-orange-portrait-text.png';

const assets = {
    logos : {
        fitbit,
        motus : {
            blue : {
                text: {
                    landscape : motusBlueTextLandscape,
                    portrait : motusBlueTextPortrait
                }
            },
            orange : {
                text: {
                    portrait : motusOrangeTextPortrait
                }
            }
        }
    }
};

export { assets };