import React, { Component } from 'react';
import { StyleSheet } from 'react-native';
import { NavigationActions } from 'react-navigation';
import Icon from 'react-native-vector-icons/Ionicons';

export const MotusMenuToggle = (props) => {
    return <Icon
        name="ios-menu"
        color={"white"}
        size={30}
        style={styles.menu}
        {...props}  />
};

const styles = StyleSheet.create({
    menu: {
        paddingLeft: 15,
        fontWeight: "100"
    }
});

