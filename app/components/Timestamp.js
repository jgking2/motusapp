import React, { Component } from 'react';
import { Text } from 'react-native';
import moment from 'moment';

//TODO wrap this in interval update.
export const Timestamp = ({ date, ...addtnl }) => {
    return (
        <Text {...addtnl} >
            { date && moment(date).fromNow() }
        </Text>
    )
};
