import React, { Component } from 'react';
import { 
    Text, 
    TouchableOpacity,
    StyleSheet,
    Platform
} from 'react-native';

import { MotusGradient } from './MotusGradient';

export const MotusButton = ({ text, onPress, disabled }) => {
    return (
        <TouchableOpacity disabled={disabled} style={styles.container} onPress={onPress}>
            <MotusGradient style={styles.wrapper}>
                <Text style={styles.text}>
                    { text }
                </Text>
            </MotusGradient>
        </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    container : {
        flexDirection:"row", 
        alignSelf: "center"
    },
    text: {
        // fontSize: 20,
        textAlign: 'center',
        margin: 10,
        // fontWeight: "300",
        color: '#ffffff',
        backgroundColor: 'transparent'
    },
    wrapper : {
        flex: 1,
        padding:5,
        // margin: 15,
        borderRadius: 36,
        ...Platform.select({
            ios: {
                shadowColor: 'rgba(0,0,0, .4)',
                shadowOffset: { height: 1, width: 1 },
                shadowOpacity: 1,
                shadowRadius: 1,
            },
            android: {
                backgroundColor: '#fff',
                elevation: 2,
            },
        })
    }
});