import { 
    StyleSheet,
    Platform
} from 'react-native';

//Todo make a set of style components possibly?

export const motusStyles = StyleSheet.create({
    padded : {
        padding: 10
    },
    missingText: {
        fontSize: 20,
        fontWeight: '200'
    },
    title : {

    },
    subtitle : {

    },
    raised : {
        ...Platform.select({
            ios: {
                shadowColor: 'rgba(0,0,0, .4)',
                shadowOffset: { height: 1, width: 1 },
                shadowOpacity: 1,
                shadowRadius: 1,
            },
            android: {
                backgroundColor: '#fff',
                elevation: 2,
            },
        })
    },
    roundedEdge : {
        borderRadius : 5
    }
});