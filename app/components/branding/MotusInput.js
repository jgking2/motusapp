import React from 'react';
import { TextInput, StyleSheet } from 'react-native';
import { colors } from './colors';

export const MotusInput = ({ ...args, inputRef }) => {
    return (
        <TextInput 
            ref={inputRef}
            clearButtonMode={"while-editing"}
            placeholderTextColor={colors(.4).primary}
            style={styles.input}
            {...args} />
    )
};

const styles = StyleSheet.create({
    input : {
        fontSize: 34,
        fontWeight: '100'   
    }
});