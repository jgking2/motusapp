import React from 'react';
import { StyleSheet } from 'react-native';
import { Header } from 'react-navigation';
import { BlurView } from 'react-native-blur';
import { MotusGradient } from './MotusGradient';

export const MotusHeader = (props) => {
    return (
        <MotusGradient opacity={.8}>
            {/* <BlurView 
                    blurType="light"
                    style={headerStyles.blur} /> */}
            <Header {...props} />
        </MotusGradient>
    )
}

const headerStyles = StyleSheet.create({
    // headerTitleStyle: {
    //     shadowColor: 'rgba(0,0,0, .4)',
    //     shadowOffset: { height: 1, width: 1 },
    //     shadowOpacity: 1,
    //     shadowRadius: 1
    // },
    headerStyle: {
        backgroundColor:"transparent",
    },
    blur : {
        position:"absolute",
        left: 0, right: 0, top: 0, bottom: 0
    }
});

const headerTintColor = 'white';

export const headerOptions = Object.assign(headerStyles, { headerTintColor });

