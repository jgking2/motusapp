import React, {Component} from 'react';
import {View} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import { colors } from './colors';

const start = { x:1, y:.9 };
const defaultOpacity = .8;


export const MotusGradient = ({
    children, opacity = defaultOpacity, ...addtlProps}) => {
    const colorRange = [ 
        colors(opacity).secondary, 
        colors(opacity).primary 
    ];
    return (
        <LinearGradient
            style={{backgroundColor:"white"}}
            start={start} 
            {...addtlProps}
            colors={colorRange}>
            {children}
        </LinearGradient>
    )
};