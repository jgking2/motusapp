import React, { Component } from 'react';
import { StyleSheet } from 'react-native';
import { ButtonGroup } from 'react-native-elements';

import { colors } from './colors';

export const MotusTabs = ({ selectedIndex = 0, onPress, buttons }) => {

return (
        <ButtonGroup 
            buttonStyle={styles.button}
            selectedTextStyle={styles.selectedText}
            textStyle={{ color: 'rgba(0,0,0,.3)' }}
            innerBorderStyle={{
                color: "transparent",
                borderRadius : 5
            }}
            containerStyle={styles.container}
            selectedBackgroundColor={'transparent'}
            disableSelected={true}
            selectedIndex={selectedIndex}
            onPress={onPress}
            buttons={buttons}>
        </ButtonGroup>
    )
}

const styles = StyleSheet.create({
    container: {
        backgroundColor:"transparent",
        borderColor: "transparent"
    },
    button: { 
        borderTopRightRadius: 5,
        borderBottomRightRadius:5
    },
    selectedText: {
        // color: 'white',
        padding: 10,
        color: colors(1).primary,
        alignSelf: "stretch",
        textAlign: "center",
        textAlignVertical : "center",
        fontSize: 16,
        letterSpacing: 1,
        fontWeight: '600',
        // textDecorationLine: "underline"
        // flex: 1
    },
    // innerBorder : 
});

//Old style
{/* <ButtonGroup 
buttonStyle={{ 
    borderTopRightRadius: 5,
    borderBottomRightRadius:5
}}
selectedTextStyle={{
    // color: 'white',
    padding: 10,
    color: colors(1).primary,
    alignSelf: "stretch",
    textAlign: "center",
    textAlignVertical : "center",
    fontSize: 16,
    letterSpacing: 1,
    fontWeight: '600',
    // textDecorationLine: "underline"
    // flex: 1
}}
textStyle={{
    color: 'rgba(0,0,0,.3)'
}}
innerBorderStyle={{
    color: "transparent",
    borderRadius : 5
}}
containerStyle={{
    backgroundColor:"transparent",
    borderColor: "transparent"
}}
selectedBackgroundColor={'transparent'}
disableSelected={true}
selectedIndex={selectedIndex}
onPress={this.changePage.bind(this)}
buttons={['Manage', 'Add Device']}>
</ButtonGroup> */}