import React, { Component } from 'react';
import { Keyboard, Animated } from 'react-native';

import PropTypes from 'prop-types';

export class KeyboardReducingImage extends Component {
    componentWillMount() {
        console.log(this.props);
        const { scaleRange } = this.props;
        const [ startingValue, endingValue ] = scaleRange;

        this.setState({
            endingValue,
            startingValue,
            scaleValue : new Animated.Value(startingValue)
        });

        this.keyboardWillShowSub = Keyboard
            .addListener("keyboardWillShow", this.keyboardWillShow.bind(this));

        this.keyboardWillHideSub = Keyboard
            .addListener("keyboardWillHide", this.keyboardWillHide.bind(this));
    }

    componentWillUnmount() {
        this.keyboardWillHideSub.remove();
        this.keyboardWillShowSub.remove();
    }

    keyboardWillHide = ({ duration }) => {
        const { startingValue, scaleValue } = this.state;
        Animated.timing(scaleValue, {
            duration,
            toValue: startingValue,
            useNativeDriver : true
        }).start();
    }

    keyboardWillShow = ({ duration }) => {
        const { endingValue, scaleValue } = this.state;
        Animated.timing(scaleValue, {
            duration,
            toValue: endingValue,
            useNativeDriver : true
        }).start();
    }

    // state = {
    //     animation: Animated.Value(0)
    // }




    render() {
        const { scaleValue } = this.state;
        const { source, style } = this.props;
        console.log(scaleValue);
        return (
            <Animated.Image
                style={[{
                    transform: [{ 
                        scale : scaleValue 
                    }] 
                    }, style]
                }
                source={source} />
        )
    }
}

KeyboardReducingImage.PropTypes = {

}