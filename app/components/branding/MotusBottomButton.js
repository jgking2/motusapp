import React, { Component } from 'react';
import { 
    Text, 
    TouchableOpacity,
    StyleSheet,
    Platform
} from 'react-native';
import { BlurView } from 'react-native-blur';
import { MotusGradient } from './MotusGradient';

import { colors } from './colors';
import { paddingBottom } from '../../core';

export const MotusBottomButton = ({ 
    text, 
    onPress, 
    disabled
 }) => {
    return (
        <TouchableOpacity 
            disabled={disabled} 
            style={[styles.container, styles.wrapper]} 
            onPress={onPress}>

            <BlurView
                blurType="light"
                style={styles.blurStyle} />
            <Text style={styles.text}>
                { text }
            </Text>
        </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    container : {        
        position: "absolute",
        bottom: 0,
        right: 0,
        left: 0
    },
    text: {
        fontWeight: 'bold',
        textAlign: 'center',
        margin: 15,
        color: 'white',//colors(1).primary,
        backgroundColor: 'transparent'
    },
    blurStyle: {
        position: "absolute",
        top: 0,
        bottom: 0,
        left: 0,
        right: 0
    },
    wrapper : {
        flex: 1,
        paddingBottom,
        backgroundColor: colors(.8).primary,
        // margin: 15,
        ...Platform.select({
            ios: {
                shadowColor: 'rgba(0,0,0, .4)',
                shadowOffset: { height: 1, width: 1 },
                shadowOpacity: 1,
                shadowRadius: 1,
            },
            android: {
                backgroundColor: '#fff',
                elevation: 2,
            },
        })
    }
});