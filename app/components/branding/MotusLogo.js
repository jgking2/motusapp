import React, { Component } from 'react';
import { Text, View, Image, Animated } from 'react-native';
import PropTypes from 'prop-types';

import { assets } from '../../assets';

export const MotusLogo = ({ includeText, textLogoOrientation, primary }) => {
    const color = primary? "orange" : "blue";
    const textLogo = includeText? "text" : "logoonly";
    var logo;
    if(includeText) {
        logo = assets.logos.motus[color].text[textLogoOrientation];
    }
    else {
        // logo = assets.logos.motus.blue.
    }
    return (
        <Image 
            resizeMode={"cover"}
            style={{height: undefined, width: undefined, flex: 1}} 
            source={logo} />
    )
}

MotusLogo.defaultProps = {
    includeText : true,
    textLogoOrientation : 'landscape',
    primary : true
};

MotusLogo.propTypes = {
    includeText : PropTypes.bool,
    textLogoOrientation : PropTypes.oneOf('landscape', 'portrait'),
    primary : PropTypes.bool
};