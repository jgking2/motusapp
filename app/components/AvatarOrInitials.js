import React, { Component } from 'react';

import { Avatar } from 'react-native-elements';

export const AvatarOrInitials = ({ photoURL, name, ...addtnl }) => {    
    const title = extractInitials(name);

    let avatarProps = {
        ...addtnl,
        title
    };
    if(photoURL) {
        avatarProps.source = {
            uri : photoURL,
            cache : "force-cache"
        };
    }
    return <Avatar {...avatarProps} />
};

const extractInitials = (name, limit = 2) => {
    const initials = name && name.split(' ')
        .filter(( _ , index) => index < limit).map(part => part[0]).join('');
    return initials;
};