import React, { Component } from 'react';
import {
    Text
} from 'react-native';

import { 
    convert, 
    config 
} from '../services/metrics';

/**
 * 
 * @param {{ metric : { type, value, duration } }} param0 
 */
export const Metric = ({ metric, unit, digits = 1, ...props}) => {
    console.log(metric)
    const { type, value } = metric;    
    const converter = convert[metric.type];
    let text = value;
    if(converter) {
        let defaultUnit = config.defaults[type];
        text = converter.to(unit || defaultUnit, value);
    }
    return (
        <Text {...props}>
            { text.toFixed(Number.isInteger(text)? 0 : digits) }
        </Text>
    )
};