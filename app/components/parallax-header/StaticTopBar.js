import React, { Component } from 'react';
import { StyleSheet, Animated, View, Text } from 'react-native';
import { styles } from './styles';

export const StaticTopBar = ({ children, ...additionalProps }) => {
    return (
        <View style={styles.staticTopContainer}>
            { children }
            {/* <DefaultNavbar {...additionalProps}  /> */}
        </View>
    )
};

const DefaultNavbar = ({ title, animatedValue, heightRange }) => {
    const [ maxHeight, minHeight ] = heightRange;
    const threshold = maxHeight - (maxHeight * .1);
    const opacity = animatedValue.interpolate({
        inputRange: [threshold, maxHeight],
        outputRange: [0, 1],
        extrapolate: 'clamp'
    });
    return (
        <Animated.View style={[{ opacity }, styles.staticTopInterior]}>
            <Text style={{
                color:'white', 
                flex: 1, 
                fontSize:16, 
                textAlign:"center"}}>
                { title }
            </Text>
        </Animated.View>
    )
};