import React, { Component } from 'react';
import { Animated, Text } from 'react-native';
import { styles } from './styles';
import { STATUS_BAR_HEIGHT } from './constants';

export const Title = ({ animatedValue, title, heightRange }) => {
    const [maxHeight, minHeight] = heightRange;
    const threshold = maxHeight - (maxHeight * .1);

    const finalLocation = ((maxHeight - minHeight) / 2) + STATUS_BAR_HEIGHT; 

    const translateY = animatedValue.interpolate({
        inputRange: [0, maxHeight],
        outputRange: [0, finalLocation],
        extrapolate:"clamp"
    });
    const scale = animatedValue.interpolate({
        inputRange: [0, maxHeight],
        outputRange: [1, .8],
        extrapolateRight:"clamp"
    });
    return (
        <Animated.View 
            pointerEvents="none"
            style={[styles.titleContainer, { 
                transform: [
                    { scale },
                    { translateY }                    
                ]
                }]}>
            <Text style={{
                color: "white", 
                fontSize: 20
                 }}>{title}</Text>
        </Animated.View>
    )
};
