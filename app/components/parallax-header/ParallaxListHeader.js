import React, { Component } from 'react';
import { View, Animated, Dimensions } from 'react-native';
import PropTypes from 'prop-types';
import { CollapsibleHeader } from './CollapsibleHeader';
import { styles } from './styles';
import { ADDTL_HEIGHT } from './constants';

const { height : SCREEN_HEIGHT } = Dimensions.get("window");

const defaultScrollThrottle = 16;

export class ParallaxListHeader extends Component {
    shouldComponentUpdate(nextProps) {
        const receivedNewValue = this.props.animatedValue != nextProps.animatedValue;
        return true;
    }

    static defaultProps = {
        title: '',
        heightRange : [ 150, 80 ],
        expandedHeaderColor: 'red',
        collapsedHeaderColor : 'blue',
        // animatedValue : new Animated.Value(0),
        scrollEventThrottle : defaultScrollThrottle,
        bottomToolbarComponent : <View />,
        topComponent: <View />
    };

    // componentWillMount() {
    //     let { animatedValue } = this.props;

    //     this.setState({ 
    //         scrollY : animatedValue,
    //         scrollEventListener
    //     });
    // }

    render() {
        const {
            scrollEventThrottle,
            listComponent,
            style,
            heightRange,
            animatedValue,
            ...addtnlProps
        } = this.props;
        animatedValue.addListener(() => console.log('update!'))
        const adjustedHeightRange = heightRange.map((height, index) => {
            return index === 0? height : height + ADDTL_HEIGHT;
        });

        return (
            <CollapsibleHeader
                heightRange={adjustedHeightRange}
                {...addtnlProps} 
                scrollValue={animatedValue} />
        )
    }
}

ParallaxListHeader.propTypes = {
    title: PropTypes.string,
    heightRange: PropTypes.arrayOf(PropTypes.number),
    animatedValue: PropTypes.object,
    listComponent : PropTypes.object,
    topComponent : PropTypes.object,
    expandedHeaderComponent : PropTypes.object,
    expandedHeaderColor: PropTypes.string,
    collapsedHeaderColor : PropTypes.string,
    collapsedHeaderComponent : PropTypes.object,
    scrollEventThrottle : PropTypes.number,
    bottomToolbarComponent : PropTypes.object
};