import React, { Component } from 'react';
import { Animated, View, Text } from 'react-native';

import { StaticTopBar } from './StaticTopBar';
import { Title } from './Title';
import { styles } from './styles';

export class CollapsibleHeader extends Component {    
    componentWillMount() {
        const { scrollValue, heightRange } = this.props;
        const [maxHeight, minHeight] = heightRange;

        const inputRange = [0, maxHeight];

        const translateValue = scrollValue.interpolate({
            inputRange,
            outputRange: [0, -(maxHeight - minHeight)],
            extrapolate: "clamp"
        });

        const fadeIn = scrollValue.interpolate({
            inputRange,
            outputRange : [0, 1]
        });

        const fadeOut = scrollValue.interpolate({
            inputRange,
            outputRange: [1, 0]
        });

        this.setState({
            translateValue,
            fadeOut,
            fadeIn
        });
    };

    render() {
        const {
            heightRange,
            scrollValue,
            expandedHeaderColor,
            expandedHeaderComponent,
            collapsedHeaderColor,
            collapsedHeaderComponent,
            bottomToolbarComponent,
            topComponent,
            title
        } = this.props;

        const { 
            translateValue, 
            fadeIn,
            fadeOut
        } = this.state;
    
        const {
            expand,
            expandedHeaderBackground,
            collapsedHeaderBackground
         } = styles;

        const [ maxHeight, minHeight ] = heightRange;

        const passThroughProps = {
            title,
            heightRange,
            animatedValue : scrollValue
        };

        return (
            <View
                pointerEvents={"box-none"} 
                style={{                
                backgroundColor: 'transparent',
                height: maxHeight}}>
                <Animated.View style={{ flex: 1,
                    transform: [{ translateY:translateValue }]
                }}>
                    <BackgroundLayer
                        style={collapsedHeaderBackground} 
                        color={collapsedHeaderColor} 
                        interiorStyle={expand}>
                        { 
                            collapsedHeaderComponent || 
                            <DefaultBackground color={collapsedHeaderColor} /> 
                        }
                    </BackgroundLayer>
                    <BackgroundLayer
                        style={[
                            expandedHeaderBackground, 
                            { height: maxHeight },
                            { opacity: fadeOut }]} 
                        color={expandedHeaderColor} 
                        interiorStyle={expand}>
                        { 
                            expandedHeaderComponent || 
                            <DefaultBackground color={expandedHeaderColor} /> 
                        }
                    </BackgroundLayer>     
                    <Title {...passThroughProps} />

                    <BottomToolbar animatedValue={scrollValue}>
                        { bottomToolbarComponent }
                    </BottomToolbar>
                </Animated.View>

                <StaticTopBar {...passThroughProps}>
                    { topComponent }
                </StaticTopBar>

            </View>
        )
    };
}

const BottomToolbar = ({ animatedValue, children }) => {
    const styles = {
        position: 'absolute',
        bottom: 0,
        backgroundColor: 'transparent'
    };
    return (
        <View style={styles}>
            { children }
        </View>
    )
};

const BackgroundLayer = ({ style, color, children, interiorStyle }) => {
    const interior = React.cloneElement(children, { style : interiorStyle });
    return (
        <Animated.View style={style}>
            { interior }
        </Animated.View>
    )
};

const DefaultBackground = ({ color }) => 
    <View style={[styles.headerInterior, { backgroundColor : color }]} />

