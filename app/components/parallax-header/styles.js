import { StyleSheet } from 'react-native';

import { STATUS_BAR_HEIGHT } from './constants';

export const styles = StyleSheet.create({
    container: {
        flex: 1, 
        // flexDirection: "column-reverse" 
    },
    contentContainer: {
        // flex:1,
    },
    staticTopContainer : {
        position: 'absolute',
        top: 0,
        right: 0,
        left: 0,
        // backgroundColor: 'red'
    },
    staticTopInterior : {
        flexDirection: "row",
        flex: 1
    },
    headerInterior : {
        flex: 1
    },
    expandedHeaderBackground : {
        flex: 1,
        position: "absolute",
        top: 0,
        left: 0, 
        right: 0,
        backgroundColor: 'transparent'
    },
    collapsedHeaderBackground : {
        flex: 1,
        top: 0,
        backgroundColor: 'transparent'
    },
    titleContainer: {        
        position: "absolute",
        top: 0,
        bottom: 0,
        right: 0,
        left: 0,
        justifyContent: 'center',
        backgroundColor: "transparent",
        alignItems: "center"
    },
    expand : {
        flex: 1
    }
});