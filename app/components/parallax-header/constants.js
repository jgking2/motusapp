import { Dimensions, Platform } from 'react-native';
const {
    height: SCREEN_HEIGHT,
} = Dimensions.get('window');

const IS_IPHONE_X = SCREEN_HEIGHT === 812;
export const STATUS_BAR_HEIGHT = Platform.OS === 'ios' ? (IS_IPHONE_X ? 44 : 20) : 0;
export const NAV_BAR_HEIGHT = Platform.OS === 'ios' ? (IS_IPHONE_X ? 88 : 64) : 45;

export const ADDTL_HEIGHT = IS_IPHONE_X? 24 : 0;

export const SCROLL_EVENT_THROTTLE = 16;
export const DEFAULT_HEADER_MAX_HEIGHT = 200;
export const DEFAULT_HEADER_MIN_HEIGHT = NAV_BAR_HEIGHT;
export const DEFAULT_EXTRA_SCROLL_HEIGHT = 50;
export const DEFAULT_BACKGROUND_IMAGE_SCALE = 1.5;
export const DEFAULT_NAVBAR_COLOR = '#3498db';
export const DEFAULT_BACKGROUND_COLOR = '#303F9F';
export const DEFAULT_TITLE_COLOR = 'white';