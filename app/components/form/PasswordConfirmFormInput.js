import React from 'react';
import { View } from 'react-native';
import {  FormInput, FormLabel, FormValidationMessage } from 'react-native-elements';
// import {
//     Item,
//     Input,
//     Label
// } from 'native-base';

//Used in conjunction with redux-form.
export const PasswordConfirmFormInput = (props) => {
    const { 
        input,
        label,
        keyboardType,
        meta,
        style,
        matchValue,
        ...addtlProps
    } = props;

    const succ = meta.touched && input.value === matchValue;
    // const err =  meta.touched && meta.invalid;

    return (
        <View>
            { label && <FormLabel>{ label }</FormLabel> }
            <FormInput
                autoCapitalize={"none"}
                autoCorrect={false}
                {...addtlProps}
                keyboardType={keyboardType || "default"}
                onBlur={input.onBlur}
                onFocus={input.onFocus}
                value={input.value} 
                onChangeText={input.onChange}
                />
            {/* <FormValidationMessage>
                {err}
            </FormValidationMessage> */}
        </View>
    )
}