import React from 'react';
import { View, Text, TextInput, StyleSheet } from 'react-native';
import { colors } from '../branding';


export const MotusTextInput = ({
        label,
        containerStyle,
        labelStyle,
        ...inputProps }) => {
    return (
        <View style={[styles.inputContainer, containerStyle]}>
            {
                label && <Text
                    style={[styles.label, labelStyle]}>
                    { label }
                </Text> || undefined
            }
            <TextInput {...inputProps}/>
        </View>
    )
}

const styles = StyleSheet.create({
    label : {
        fontSize: 16,
        color: colors(.9).primary
    },
    inputContainer : {
        paddingVertical: 8,
        borderBottomColor: colors(.9).primary,
        borderBottomWidth : StyleSheet.hairlineWidth
    }
})