import React from 'react';
import { 
    View, 
    TextInput,
    Text
} from 'react-native';
import { 
    FormInput, 
    FormValidationMessage, 
    FormLabel 
} from 'react-native-elements';

//Used in conjunction with redux-form.
export const TextFormInput = (props) => {
    const { 
        input,
        label,
        keyboardType,
        meta,
        style,
        ...addtlProps
    } = props;

    const succ = meta.touched && meta.valid;
    const err =  meta.touched && meta.invalid;

    return (
        <View>
            { label && <Text>{ label }</Text>}
            {/* { label && <FormLabel>{ label }</FormLabel> } */}
            <TextInput
                autoCapitalize={"none"}
                autoCorrect={false}
                {...addtlProps}
                keyboardType={keyboardType || "default"}
                onBlur={input.onBlur}
                onFocus={input.onFocus}
                value={input.value} 
                onChangeText={input.onChange}
                />
            <FormValidationMessage>
                {err}
            </FormValidationMessage>
        </View>
        // <Item floatingLabel success={succ} error={err} {...style}>
        //     { label && <Label>{label}</Label>}
        //     <Input    
                
                
        //          />
        // </Item>
    )
}