//TODO - split this up to pull from ios and android versions.
import DatePicker from 'react-native-datepicker';
import { StyleSheet, Dimensions } from 'react-native';
import React from 'react';

//Used in conjunction with redux-form.
export const DateFormInput = ({ input, label, meta, ...addtlProps }) => {
    const succ = meta.touched && meta.valid;
    const err =  meta.touched && meta.invalid;

    return (
        // <Item floatingLabel success={succ} error={err} {...style}>
        //     { label && <Label>{label}</Label>}
            
            <DatePicker 
                confirmBtnText={"Done"}
                cancelBtnText={"Cancel"}
                placeholder={label}
                showIcon={false}
                date={input.value}
                format={"MM-DD-YYYY"}
                {...addtlProps}
                // customStyles={{
                //     dateTouchBody: {
                //         flexDirection : 'column',
                //         height : 60,
                //         alignItems: 'flex-start',
                //         justifyContent: 'flex-start',                     borderColor :'#999',
                //         borderBottomWidth :  StyleSheet.hairlineWidth, 
                //         flex: 1
                //     },
                //     dateInput : {                    
                //         flex : 1,
                //         borderWidth : 0   
                //     },
                //     placeholderText : {
                //         color : '#333'
                //     }
                // }}       
                
                onDateChange={(val) => {
                    input.onChange(val);
                    input.value = val;
                }}/>
    )
}