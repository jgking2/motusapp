import ImagePicker from 'react-native-image-crop-picker';
// import ImagePicker from 'react-native-image-picker';
import {
    View, 
    Text,
    TouchableOpacity
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import React from 'react';

const defaultOptions = {
    width: 300,
    height: 300,
    cropping : true,
    includeBase64 : true,
    mediaType: 'photo'
};

const DefaultIcon = (props) => {
    const { useCamera } = props;
    const iconName = `ios-${useCamera? 'camera': 'image'}`
    return (
        <Icon name={iconName} />
    )
};

/** {BETA} Tweakage going down */
export const FetchImage = (props) => {
    const { 
        useCamera, 
        onSelect, 
        onCancel,
        children,
        options,
        ...addtlProps
    } = props;
    const method = useCamera? 'openCamera' : 'openPicker';
    const optionsForImage = Object.assign({}, defaultOptions, options);
    const onPress = () => ImagePicker[method](optionsForImage)
        .then(res => {
            console.log(res);
            onSelect && onSelect(res);
        })
        .catch(err => { 
            onCancel && onCancel(err);
        });

    return (
        <TouchableOpacity onPress={onPress}>
            <View>
                { children || <DefaultIcon { ...useCamera }/> }
            </View>
        </TouchableOpacity>
    )
};