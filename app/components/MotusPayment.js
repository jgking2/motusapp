import React, { Component } from 'react';
import { 
    View, 
    Text,
    Button
} from 'react-native';
import { connect } from 'react-redux';
import { submitPayment, loadToken } from '../services/payment';

const mapStateToProps = ({ paymentState : { token } }) => ({ token });
const mapDispatchToProps = dispatch => ({
    getToken : () => dispatch(loadToken()),
    processPayment : (amount, contestId) => dispatch(submitPayment(amount, contestId))
});
//TODO eventually move the actual payment elsewhere.
@connect(mapStateToProps, mapDispatchToProps)
export class MotusPayment extends Component {
    componentWillMount() {
        //TODO - rename this, more like init - or swirl around payment itself.
        const { getToken } = this.props;
        getToken();
    }

    render() {
        const { 
            amount, 
            token : { 
                loading 
            }
        } = this.props;

        return (
            <View>
                <Button 
                    disabled={loading} 
                    title={`Pay ${amount}`} onPress={this._submitPayment.bind(this)} />
            </View>
        )
    }

    _submitPayment() {
        const { amount, identifier, processPayment } = this.props;     
        processPayment(amount, identifier);
    }
}