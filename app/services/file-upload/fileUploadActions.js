export const UPLOAD_FILE = "UPLOAD_FILE";
export const FILE_UPLOAD_COMPLETE = "FILE_UPLOAD_COMPLETE";
export const FILE_UPLOAD_PROGRESS = "FILE_UPLOAD_PROGRESS";
export const FILE_UPLOAD_ERROR = "FILE_UPLOAD_ERROR";

 /**
  * 
  * @param {string} file The local path of the file to upload.
  * @param {string} name 
  * @param {string?} category 
  */
export const uploadFile = (file, category, name = generateName()) => {
    const tempId = +new Date();
    // name = name || generateName();    
    return {
        type: UPLOAD_FILE,
        file,
        category,
        name,
        tempId
    }
}

export const fileUploadedSuccessfully = (url, tempId) => {
    return {
        type : FILE_UPLOAD_COMPLETE,
        url,
        tempId
    }
};

/**
 * @returns {string}
 */
const generateName = () => {
    let date = new Date();
    let random = date.valueOf() + +(Math.random() * Math.pow(10, 4)).toFixed(0)
    return random.toString();
}