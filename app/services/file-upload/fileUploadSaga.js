import {takeEvery, put, call, take} from 'redux-saga/effects';
import {delay, eventChannel} from 'redux-saga';

import {firebase} from '../firebaseService';
import { 
    fileUploadedSuccessfully,
    UPLOAD_FILE, 
    FILE_UPLOAD_COMPLETE,
    FILE_UPLOAD_ERROR
} from './index';
//Currently a "happy path function" expand to emit more information.
function *uploadFile({ file, category, name, tempId }) {
    let path = `${category}/${name}`;

    const storageRef = firebase.storage().ref();
    const uploadTask = storageRef.child(path).putFile(file);

    //TODO Add more robust events. https://firebase.google.com/docs/storage/web/upload-files
    // uploadTask.on(firebase.storage.TaskEvent.STATE_CHANGED, )
    const { res, err } = yield uploadTask.then(res => {
        return { res }
    }).catch(err => {
        console.info(err);
        return { err };
    });
    if(res) {
        console.log(res);
        const url = res.downloadURL;
        yield put(fileUploadedSuccessfully(url, tempId));
    }
    console.info(res, err);
    // const error = uploadTask.catch(err => {
    //     console.info(err);
    //     put({ type : FILE_UPLOAD_ERROR });
    // });
    
    // yield put({ url : 'hellothere!', tempId });
}

export function* fileUploadSaga() {
    yield takeEvery(UPLOAD_FILE, uploadFile);
    // while(true) {
    //     let { file, category, name } = yield take(UPLOAD_FILE);
    //     yield uploadFile;
    //     // yield call(signOut);
    //     // yield put({ type : USER_LOGGED_OUT});
    // }
}