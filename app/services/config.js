const comments = {
    name : 'comments'
};

const feed = {
    name : 'feed',
    comments
};

const notifications = {
    name : 'notifications'
};

const contests = {
    name : 'contests',
    feed,
    notifications
};

const metrics = {
    name : 'metrics'
};

const devices = {
    name : 'devices'
};

const profiles = {
    name : 'user-profiles',
    devices,
    metrics
};

export const dataStructure = {
    contests,
    profiles
};