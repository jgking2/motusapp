import {
    LOAD_USER_PROFILE,
    USER_PROFILE_LOADED,
    USER_PROFILE_UPDATE_RECEIVED,
    USER_PROFILE_SAVE_SUCCESS
} from './index';
import {
    USER_LOGGED_OUT
} from '../authentication';

//TODO maybe add loading states?
let defaultProfile = {}
const initialState = {
    loading: true,
    profile : defaultProfile
};

export const profileState = (state = initialState, { 
    type, profile = defaultProfile }
) => {
    switch(type) {
        case USER_PROFILE_LOADED:
        case USER_PROFILE_SAVE_SUCCESS:
        case USER_PROFILE_UPDATE_RECEIVED:
            return updateState(state, { profile });
        default:
            return state;
    }
};

const updateState = (state, updates) => {
    const updatedState = Object.assign(state, {
        loading : false
    }, updates);
    return updatedState;
};