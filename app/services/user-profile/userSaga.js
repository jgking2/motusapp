import {
    takeEvery, 
    put, 
    call, 
    take, 
    all, 
    fork,
    cancel,
    race
} from 'redux-saga/effects';
import {delay, eventChannel} from 'redux-saga';

import {
    USER_REQUIRES_REGISTRATION,
    USER_PROFILE_LOADED,
    USER_PROFILE_UPDATE_RECEIVED,
    NEW_USER_PROFILE,
    USER_PROFILE_SAVE_SUCCESS,
    UPDATE_USER_PROFILE,
    updateUserProfileError,
    updateUserProfileSuccess,
    userJoinedContest
} from './index';
import {
    USER_LOGIN_SUCCESS,
    REGISTER_USER_SUCCESS,
    USER_AUTHENTICATED,
    USER_LOGGED_OUT
} from '../authentication';

import { uploadFile } from '../file-upload';
import {
    updateUserProfile,
    getUserProfileChannel,
    saveUserProfile,
    joinContest
} from './userService';

/**
 * If the url is not external, and exists we need to upload.
 * @param {string} url 
 */
const shouldUpload = (url = '') => {
    const isExternalUrl = url && url.match(/^(http|https):\/\//g);    
    return url.length && !isExternalUrl;
};

function* onNewUserProfile({ profile }) {
    try {
        yield call(saveUserProfile, profile);
        yield put({
            type: USER_PROFILE_SAVE_SUCCESS,
            profile
        });
    } catch(err) {
        //Emit error saving profile.
    }
}

function *onUpdateUserProfile({ profile }) {
    try {
        const { phone, photoURL = '', username } = profile;
        let fieldsToUpdate = { phone, username };
        
        if(shouldUpload(photoURL || '')) {
            const { tempId : imageTempId } = yield put(
                uploadFile(photoURL, 'contest-photo')
            );
            response = yield take(
                action => action.tempId === imageTempId
            );
            url = response.url;
            fieldsToUpdate['photoURL'] = url;
        }
        //Grabs all fields that exist.
        const update = Object.keys(fieldsToUpdate)
            .reduce((prev, curr) => {
                const val = fieldsToUpdate[curr];
                if(val) {
                    prev[curr] = val;
                }
                return prev;
            }, {});
            
        yield call(updateUserProfile, update);
        yield put(updateUserProfileSuccess(profile));
    }
    catch(err) {
        yield put(updateUserProfileError(profile));
    }
}

//Split this up potentially.
function* watchUserProfile() {
    const profileChannel = getUserProfileChannel();
    //Watches for changes to the profile.
    while(true) {
        let { profile } = yield take(profileChannel);
        yield put({
            type : USER_PROFILE_UPDATE_RECEIVED,
            profile
        });
    }
}

//When the user is authenticated, spawn these sagas.
function* onUserAuthentication() {
    yield takeEvery(NEW_USER_PROFILE, onNewUserProfile);
    yield takeEvery(UPDATE_USER_PROFILE, onUpdateUserProfile);
    yield fork(watchUserProfile);
}

export function* userProfileSaga() {
    while(true) {
        yield take(USER_AUTHENTICATED);

        const task = yield fork(onUserAuthentication);

        yield take(USER_LOGGED_OUT);
        yield cancel(task);
    }
}