
import firebase from 'react-native-firebase';
import {delay, eventChannel} from 'redux-saga';

import { dataStructure } from '../config';

const {
    name : userProfileCollection
} = dataStructure.profiles;

export const getUserProfileChannel = () => eventChannel(emit => {
    const profileRef = _getProfileRef();
    const unsubscribe = profileRef
        .onSnapshot(snapshot => {
            emit({
                profile : snapshot.data()
            });
        });
    return () => unsubscribe();
});

export const saveUserProfile =  async (profile) => {
    const profileRef = _getProfileRef();
    const result = await profileRef.set(profile);
}

export const updateUserProfile = async (updates) => {
    const profileRef = _getProfileRef();
    const result = await profileRef.update(updates);
};

const _getProfileRef = () => {
    const { uid } = firebase.app().auth().currentUser;
    const profile = firebase.firestore()
        .collection(userProfileCollection)
        .doc(uid);
    return profile;
};