export const LOAD_USER_PROFILE = 'LOAD_USER_PROFILE';
export const USER_PROFILE_LOADED = 'USER_PROFILE_LOADED';
export const NEW_USER_PROFILE = 'NEW_USER_PROFILE';

export const UPDATE_USER_PROFILE = 'UPDATE_USER_PROFILE';
export const UPDATE_USER_PROFILE_SUCCESS = 'UPDATE_USER_PROFILE_SUCCESS';
export const UPDATE_USER_PROFILE_ERROR = 'UPDATE_USER_PROFILE_ERROR';

export const USER_PROFILE_SAVE_SUCCESS = 'USER_PROFILE_SAVE_SUCCESS';
export const USER_PROFILE_UPDATE_RECEIVED = 'USER_PROFILE_UPDATE_RECEIVED';

export const loadUserProfile = () => { 
    return {
        type: LOAD_USER_PROFILE
    }
};

export const updateUserProfile = (profile) => ({
    type : UPDATE_USER_PROFILE,
    profile
});

export const updateUserProfileError = (profile) => ({
    type : UPDATE_USER_PROFILE_ERROR
});

export const updateUserProfileSuccess = (profile) => ({
    type : UPDATE_USER_PROFILE_SUCCESS,
    profile
});

export const saveUserProfile = (profile) => {
    return {
        type: NEW_USER_PROFILE,
        profile
    }
};