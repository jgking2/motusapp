export const USER_AUTH_REQUESTED = 'USER_AUTH_REQUESTED';
export const USER_AUTHENTICATED = 'USER_AUTHENTICATED';
export const USER_NOT_AUTHENTICATED = 'USER_NOT_AUTHENTICATED';
export const USER_AUTHENTICATED_ERROR = 'USER_AUTHENTICATED_ERROR';

//Used to notify services of firebase auth, prior to motus complete auth.
export const USER_LOGIN_SUCCESS = 'USER_LOGIN_SUCCESS';
export const USER_LOGGED_OUT = 'USER_LOGGED_OUT';
export const USER_LOGOUT_REQUEST = 'USER_LOGOUT_REQUEST';

export const RESET_PASSWORD = 'RESET_PASSWORD';

export const REGISTER_USER = 'REGISTER_USER';
export const REGISTER_USER_SUCCESS = 'REGISTER_USER_SUCCESS';
export const REGISTER_USER_FAILED = 'REGISTER_USER_FAILED';

export const LOGIN_EMAIL = 'LOGIN_WITH_EMAIL';
export const LOGIN_FACEBOOK = 'LOGIN_FACEBOOK';
// export const LOGIN_EMAIL_SUCCESS = 'LOGIN_EMAIL_SUCCESS';
// export const LOGIN_EMAIL_FAILED = 'LOGIN_EMAIL_FAILED';

//Let us begin motus-ing.
export const USER_READY = 'USER_READY';
export const USER_SIGNED_IN_PENDING_PROFILE = 'USER_SIGNED_IN_PENDING_PROFILE';
export const UNREGOGNIZED_USER = 'UNREGOGNIZED_USER';

export const UPDATE_PROFILE_PICTURE = 'UPDATE_PROFILE_PICTURE';
export const AUTH_PROFILE_UPDATED = 'AUTH_PROFILE_UPDATED';
/**
 * @param {{ email, password }}
 * @returns { type, credentials : { email, password } }
 */
export const loginWithEmail = (credentials) => {
    return {
        type : LOGIN_EMAIL,
        credentials
    }
};

export const loginWithFacebook = () => ({
    type : LOGIN_FACEBOOK
});

export const authProfileUpdated = (user) => ({
    type : AUTH_PROFILE_UPDATED,
    user
});

export const updateProfilePicture = (photoURL) => {
    return {
        type: UPDATE_PROFILE_PICTURE,
        photoURL
    }
};

/**
 * 
 * @param {{ email, password, displayName }} user 
 */
export const registerUser = (user) => {
    return {
        type : REGISTER_USER,
        user
    }
};

export const loginSuccess = () => ({
    type : LOGIN_EMAIL_SUCCESS  
});

/**
 * 
 * @param {string} message 
 */
export const authFailure = (message) => ({
    type : USER_AUTHENTICATED_ERROR,
    message
});

/**
 * 
 * @param {{token, provider, secret}} credential 
 */
export const userAuthRequested = (credential) => {
    //TODO look into mapping this elsewhere.
    const payload = {
        credential
    };
    let response = {
        type: USER_AUTH_REQUESTED,
        payload
    };
    return response;
};

export const userLogout = () => {
    return {
        type: USER_LOGOUT_REQUEST
    }
};

export const userAuthenticated = user => {
    return {
        type: USER_AUTHENTICATED,
        user
    }
};

export const resetPassword = (email) => ({ 
    type : RESET_PASSWORD, 
    email 
});