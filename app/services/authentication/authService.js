import firebase from 'react-native-firebase';
import { eventChannel } from 'redux-saga';

/**
 * Watches the state of firebase for the user auth status, we can then broadcast this to the root container to react appropriately.
 * @returns {Channel<T>}
 */
export const createAuthStateChannel = () => {
    const authStateChannel = eventChannel(emit => {
        let subscription = firebase.auth().onAuthStateChanged(
            (auth) => {
                return emit({ user : auth && extractUser(auth)})
            },
            (err) => emit({ err })
        );
        return () => {
            subscription.unsubscribe();
        }
    });
    return authStateChannel;
}

export const getUser = async () => {
    const user = firebase.app().auth().currentUser;
    return extractUser(user);
}

export const validateUser = async () => {
    const { currentUser } = firebase.app().auth();
    return currentUser.reload();
};

/**
 * 
 * @param {{ email, password }} param0 
 */
export async function signInEmailPassword({ email, password }) {
    const { auth } = firebase.app();
    const response = await auth().signInWithEmailAndPassword(email, password);
    return response;        
}

/**
 * Sends the async request to the server, and exposes the results for consumption.
 * @param {{ providerId, secret, token }} credential 
 * @returns {Promise<any>} 
 */
export async function submitAuthRequestToFirebase(credential) {
    const { auth } = firebase.app();
    const response = await auth().signInWithCredential(credential);
    return response;
}

/**
 * 
 * @param {string} email
 * @param {string} password
 */
export async function createPasswordAuthenticationUser(email, password) {
    const { auth } = firebase.app();
    const response = await auth().createUserWithEmailAndPassword(email, password);
    return response;
}


/**
 * 
 * @param {{ displayName, photoURL }} param0 
 */
export const updateUserProfile = async ({ displayName, photoURL }) => {
    const { currentUser } = firebase.app().auth();
    const update = {
        displayName,
        photoURL
    };
    const response = await currentUser.updateProfile(update);
    return extractUser(response);
};

/**
 * 
 * @param {string} password 
 */
export async function updatePassword(password) {
    const { currentUser } = firebase.app().auth();
    const response = await currentUser.updatePassword(password);
    return response;
}

/**
 * 
 * @param {string} emailAddress 
 */
export async function resetPassword(emailAddress) {
    const { auth } = firebase.app();
    const response = await auth().sendPasswordResetEmail(emailAddress);    
    return response;
}

/**
 * 
 */
export async function sendEmailVerification() {
    const { auth } = firebase.app();
    const response = await auth().currentUser.sendEmailVerification();    
    return response;
}

export async function signOut() {
    const { auth } = firebase.app();
    const response = await auth().signOut();
    return response;
}


//Extracts these properties from value passed in.
export const extractUser = ({
    displayName, 
    photoURL, 
    email, 
    uid
}) => ({ displayName, photoURL, email, uid });