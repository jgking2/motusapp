import {
    takeEvery, 
    put, 
    call, 
    take, 
    fork, 
    all
} from 'redux-saga/effects';
import {
    
} from 'redux-saga';

import {
    USER_AUTH_REQUESTED, 
    USER_AUTHENTICATED,
    USER_NOT_AUTHENTICATED, 
    USER_LOGIN_SUCCESS,
    USER_AUTHENTICATED_ERROR,
    USER_LOGOUT_REQUEST,
    USER_LOGGED_OUT,
    LOGIN_EMAIL,
    LOGIN_FACEBOOK,
    USER_SIGNED_IN_PENDING_PROFILE,
    USER_READY,
    CHECK_USER_AUTHENTICATION,
    RESET_PASSWORD,
    REGISTER_USER,
    REGISTER_USER_SUCCESS,
    REGISTER_USER_FAILED,
    UPDATE_PROFILE_PICTURE,
    authProfileUpdated,
    authFailure
} from './index';

import {
    signOut,
    submitAuthRequestToFirebase,
    resetPassword,
    createPasswordAuthenticationUser,
    updatePassword,
    updateUserProfile,
    signInEmailPassword,
    createAuthStateChannel,
    sendEmailVerification,
    validateUser
} from './authService';

import { loginFb } from './facebookService';

import {
    USER_REQUIRES_REGISTRATION,
    USER_PROFILE_LOADED
} from '../user-profile';

import {
    uploadFile
} from '../file-upload';

function* onEmailAuthentication({ credentials }) {
    try {
        yield signInEmailPassword(credentials);
    }
    catch(err) {
        yield put(authFailure(err));
    }
}

function* onFacebookLogin() {
    try {
        const credentials = yield call(loginFb);
        yield call(authenticateThirdPartyWithFirebase, credentials);
    }
    catch(err) {
        yield put(authFailure(err));
    }
}

function* onUserRegistration({ user }) {
    let type = REGISTER_USER_SUCCESS;
    try {
        const { email, password, displayName } = user;
        const photoURL = '';        
        const { uid } = yield createPasswordAuthenticationUser(email, password);
        yield updateUserProfile({ 
            displayName, 
            photoURL 
        });
        yield put({
            type,  
            user : {
                uid,
                email,
                displayName,
                photoURL
            }
        });
    }
    catch(err) {
        type = REGISTER_USER_FAILED;
        yield put({
            type,
            err
        });
    }
}

function* onSuccessfulRegistration({ user }) {
    yield call(sendEmailVerification);
}

function* authenticateThirdPartyWithFirebase(credentials) {
    try {
        // console.info(payload);
        console.log(credentials);
        yield call(submitAuthRequestToFirebase, credentials);
        
        //Success case is handled with the event channel, no need to submit.
    }
    catch(err) {
        console.log(err);
        yield put(authFailure(err));
    }
}

function* onLogout() {
    try {
        yield call(signOut);
        //TODO - add cleanup here
        yield put({ 
            type : USER_LOGGED_OUT
        });
    }    
    catch(err) {
        //Is this even making a network call?  -should- never hit.
    }
}

function* onPasswordReset(email) {
    try {
        yield resetPassword(email);
    }
    catch( err ) {
        console.info(err);
    }
}

/**
 * Self sustaining, just watches the event stream from firebase auth.
 */
function* onAuthStateChange({ user, err }) {
    if(!!user) {
        yield 
        yield put({
            type: USER_AUTHENTICATED,
            user
        });
    }
    else {
        yield put({
            type : USER_NOT_AUTHENTICATED
        });
    }
};

//Ensures stale login doesn't persist.
function* ensureUserValidity() {
    try {
        yield call(validateUser);
    }
    catch(err) {
        yield call(onLogout);
    }
}

function* uploadPhoto({ photoURL }) {
    const { tempId } = yield put(uploadFile(photoURL, 'profile-phtoo'));
    // const { tempId } = yield put(uploadFile(uri, 'contest'));
    const { url } = yield take((action) => action.tempId === tempId);
    const result = yield call(updateUserProfile, { photoURL : url });
    yield put(authProfileUpdated(result));
}

export function* authSaga() {
    const authChannel = createAuthStateChannel();    

    //Watch for authentication.
    // yield takeEvery(USER_AUTH_REQUESTED, authenticateThirdPartyWithFirebase);
    yield takeEvery(authChannel, onAuthStateChange);
    yield takeEvery(USER_LOGOUT_REQUEST, onLogout);
    yield takeEvery(LOGIN_EMAIL, onEmailAuthentication);
    yield takeEvery(LOGIN_FACEBOOK, onFacebookLogin);
    yield takeEvery(RESET_PASSWORD, onPasswordReset);
    yield takeEvery(UPDATE_PROFILE_PICTURE, uploadPhoto);
    yield takeEvery(REGISTER_USER, onUserRegistration);
    yield takeEvery(REGISTER_USER_SUCCESS, onSuccessfulRegistration);

    yield takeEvery(USER_AUTHENTICATED, ensureUserValidity);
}