import {
    USER_AUTHENTICATED,
    USER_NOT_AUTHENTICATED,
    USER_AUTH_REQUESTED,
    LOGIN_EMAIL,
    LOGIN_FACEBOOK,
    REGISTER_USER_SUCCESS,
    USER_AUTHENTICATED_ERROR,
    USER_LOGGED_OUT,
    IS_USER_AUTHENTICATED,
    AUTH_PROFILE_UPDATED
} from './index';

import {
    USER_REQUIRES_REGISTRATION
} from '../user-profile';

const initialState = {
    //Initial load...
    initializing: true,
    authenticated: false,
    authenticating : false,
    user : { },
    error : ''
};

export const authState = (state = initialState, { type, user, message }) => {
    switch(type) {
        case LOGIN_EMAIL:
        case LOGIN_FACEBOOK:
        case USER_AUTH_REQUESTED:
            var updates = {
                initializing: false,
                authenticated: false,
                authenticating : true
            };
            return updateState(state, updates);
        case AUTH_PROFILE_UPDATED:
        case USER_AUTHENTICATED:
            var updates = {
                user,
                initializing: false,
                authenticated: true,
                authenticating : false,
                error : ''
            };
            return updateState(state, updates);
        case USER_NOT_AUTHENTICATED:
            var updates = {
                user : {},
                authenticated : false, 
                authenticating : false,
                initializing: false
            };
            return updateState(state, updates);
        case USER_LOGGED_OUT:
            var updates = {
                user : {},
                authenticated : false
            };
            return updateState(state, updates);
        case USER_REQUIRES_REGISTRATION: 
            var updates = {
                authenticating : false,
                authenticated : false,
                profileMissing : true
            };
            return updateState(state, updates);
        case REGISTER_USER_SUCCESS:
            var updates = { user };
            return updateState(state, updates);
        case USER_AUTHENTICATED_ERROR:
            var updates = {
                error : message, 
                authenticating : false 
            };
            return updateState(state, updates)
        default:
            return state;
    }
}


const updateState = (state, updates) => {
    const newState = Object.assign({}, state, updates);
    return newState;
};