import FBSDK from 'react-native-fbsdk';
import firebase from 'react-native-firebase';
/**
 * @returns {Promise<{ providerId, token, clientId }>} the client credentials
 */
export const loginFb = async () => {
    const { LoginManager, AccessToken } = FBSDK;
    const { FacebookAuthProvider } = firebase.app().auth;
    const { error } = await LoginManager.logInWithReadPermissions();
    if(error) {
        throw error;
    }
    const { accessToken, ...res } = await AccessToken.getCurrentAccessToken();
    return FacebookAuthProvider.credential(accessToken);
};