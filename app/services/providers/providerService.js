import firebase from 'react-native-firebase';
import { eventChannel } from 'redux-saga';

import { dataStructure } from '../config';

const {
    profiles: {
        name : profileCollection,
        devices: {
            name : deviceCollection
        }
    }
} = dataStructure;

export const getUserDeviceChannel = () => {    
    return eventChannel(dispatch => {
        const providerRef = _getProviderRef();

        const unsubscribe = providerRef
            .onSnapshot(snapshot => {
                const devices = {};
                snapshot.forEach(child => {
                    devices[child.id] = { 
                        key : child.id,
                        ...child.data()
                    }
                });
                dispatch(devices);
            });
        return () => unsubscribe()
    });
};

export const loadDevices = async () => {
    const providerRef = _getProviderRef();
    await providerRef.get()
        .then(snapshot => snapshot.data());
};

/**
 * 
 * @param {string} providerKey 
 */
export const addDevice = async (providerKey) => {
    const { serverTimestamp } = firebase.firestore.FieldValue;
    const providerRef = _getProviderRef()
        .doc(providerKey);
    await providerRef.set({
        linkedAt : serverTimestamp()
    });
};

const _getProviderRef = () => {
    const { uid } = firebase.app().auth().currentUser;
    const providerRef = firebase.firestore()
        .collection(profileCollection)
        .doc(uid)
        .collection(deviceCollection);
    return providerRef;
}