import { assets } from '../../../assets';

const url = 'https://www.fitbit.com/oauth2/authorize';

const scopes = [
    'activity',
    'heartrate',
    'location',
    'nutrition',
    'profile',
    'settings',
    'sleep',
    'social',
    'weight'
];
const clientId = '228CFZ';
const providerName = 'fitbit';
const redirectUrl = 'motus://activities/fitbit';

export const oauth = {
    key : providerName,
    scope : scopes,
    url,
    clientId,
    redirectUrl,
    responseType : 'token'
};

export const fitbit = {
    name : 'Fitbit',
    key : 'fitbit',
    logo : assets.logos.fitbit,
    connectText: 'Connect your Fitbit',
    oauth
};