const logo = {
    uri : 'https://images.apple.com/v/ios/health/a/images/health_icon_large_2x.png' 
};

export const applehealth = {
    name : 'Apple Health',
    key: 'applehealth',
    connectText: 'Connect with Health',
    logo
};