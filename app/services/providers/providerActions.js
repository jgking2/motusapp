export const AUTHORIZE_PROVIDER = 'AUTHORIZE_PROVIDER';
export const PROVIDER_LINKED = 'PROVIDER_LINKED';
export const USER_DEVICES_LOADED = 'USER_DEVICES_LOADED';
export const LOAD_LATEST_DATA = 'LOAD_LATEST_DATA';
export const DATA_LOAD_ERROR = 'DATA_LOAD_ERROR';
export const DATA_LOAD_SUCCESS = 'DATA_LOAD_SUCCESS';
export const IMPORT_DATA = 'IMPORT_DATA';

export const importData = (metrics, provider, contests = []) => ({
    type : IMPORT_DATA,
    metrics,
    contests,
    provider
});

//TODO - ensure we check the last pull.
export const loadLatestData = (provider, lastCheck = new Date()) => ({
    type : LOAD_LATEST_DATA,
    provider
});

export const dataLoadError = (message, provider) => ({
    type : DATA_LOAD_ERROR,
    message,
    provider
});

export const dataLoadSuccess = (data, provider) => ({
    type : DATA_LOAD_SUCCESS,
    data,
    provider
});

export const authorizeProvider = (provider) => ({
    type : AUTHORIZE_PROVIDER,
    provider
});

export const devicesLoaded = (devices) => ({
    type : USER_DEVICES_LOADED,
    devices
});

export const providerLinked = (provider) => ({
    type : PROVIDER_LINKED,
    provider
});