import AppleHealthKit, { Constants } from 'rn-apple-healthkit';
import { convert, config } from '../../metrics';


const {  
    Permissions : PERMS 
} = Constants;

const options = {
    permissions: {
        read: [
            PERMS.Height,
            PERMS.Weight,
            PERMS.StepCount,
            PERMS.DateOfBirth,
            PERMS.BodyFatPercentage,
            PERMS.NikeFuel,
            PERMS.DistanceWalkingRunning,
            PERMS.DistanceCycling,
            PERMS.FlightsClimbed
        ],
        write: [ ]//"Weight", "StepCount", "BodyMassIndex"]
    }
};

export const connect = function () : Promise<any> {
    // AppleHealthKit.
    return new Promise((resolve : Function, reject : Function) => {
        AppleHealthKit.initHealthKit(options, (err, res) => {
            if(err) {            
                return reject(err);
            }
            return resolve({ res });
        });
    });
};

/**
 * 
 * @param {Date} date 
 * @returns {Promise<any>}
 */
export const getSteps = (date = new Date()) => {
    let options;
    if(date instanceof Date)
        options = {
            date: date.toISOString()
        };
    return new Promise((resolve, reject) => {
        AppleHealthKit.getStepCount(options, (err, { value, startDate } = {}) => {
            if(err) {
                //TODO Look into how to determine if it's an error, or unable to load.
                return resolve({ });
            }
            //res.value is actual steps, wrap this into the activity format.
            return resolve({
                value,
                type : config.keys.count,
                timestamp : startDate
            });
        });
    });
};

/**
 * 
 * @param {{ unit : string, date: Date }} param0 
 * @returns {Promise<any>}
 */
export const getCycling = ({ unit = 'km', date = new Date() } = {})  => {
    //Check unit is valid.
    return new Promise((resolve, reject) => {
        let option = {
            unit : unit,
            date : date.toISOString()
        };
        AppleHealthKit
            .getDistanceCycling(
                options, (err, { value, startDate, endDate } = {}) => {
                    if(err) {
                        //TODO Look into how to determine if it's an error, or unable to load.
                        return resolve({ });
                    }
                    const duration = new Date(startDate) - new Date(endDate);
                    let data = {
                        value,
                        type : config.keys.distance,
                        timestamp : endDate
                    };
                    if(duration) data.duration = duration;
                    return resolve(data);
                });
    });
};


/**
 * @returns {Promise<any>}
 */
export const getWeight = ()  => {
    return new Promise((resolve, reject) => {
        AppleHealthKit
            .getLatestWeight(null, (err, { value, startDate, endDate }) => {
                if(err) {
                    //TODO Look into how to determine if it's an error, or unable to load.
                    return resolve({ });
                }

                const mass = convert.mass.from(value, 'pound');
                
                return resolve({
                    value,
                    type : config.keys.mass,
                    timestamp : startDate
                });
            });
        });
};

/**
 * @returns {Promise<any>}
 */
export const getSex = () => {
    return new Promise((resolve, reject) => {
        AppleHealthKit.getBiologicalSex(null, (err, res) => {
            if(err) {
                return reject(err);
            }
            return resolve(res);
        })
    });
};

/**
 * @returns {Promise<any>}
 */
export const getDob = () => {
    return new Promise((resolve, reject) => {
        AppleHealthKit.getDateOfBirth(null, (err, res) => {
            if(err) {
                return reject(err);
            }
            return resolve(res);
        })
    });
};


/**
 * @returns {Promise<any>}
 */
export const getBodyFatPercentage = () => {
    return new Promise((resolve, reject) => {
        AppleHealthKit.getLatestBodyFatPercentage(null, (err, res) => {
            if(err) {
                return reject(err);
            }
            return resolve(res);
        });
    });
};