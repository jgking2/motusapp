
import { 
    takeLatest, 
    put, 
    call, 
    fork, 
    take,
    takeEvery,
    all
} from 'redux-saga/effects';
import { 
    LOAD_LATEST_DATA, 
    dataLoadError, 
    dataLoadSuccess 
} from '../providerActions';
import { getSteps, getWeight, getCycling, connect } from './appleHealthService';

import { applehealth } from '../config';

function* retrieveHealthKitSnapshot() {
    yield call(connect);
    try {
        const [ steps, cycling ] = yield all([
            getSteps(),
            getCycling()
        ]);
        
        yield put(dataLoadSuccess({
            steps,
            cycling
        }, applehealth.key));
    }
    catch(err) {
        console.log(err);
        yield put(dataLoadError(applehealth.key));
    }
}

export function* applehealthSaga() {
    yield takeEvery(
        action => action.type === LOAD_LATEST_DATA 
            && action.provider === applehealth.key,
        retrieveHealthKitSnapshot);
}