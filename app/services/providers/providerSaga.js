
import { 
    takeLatest, 
    put, 
    call, 
    fork, 
    take,
    takeEvery,
    cancel
} from 'redux-saga/effects';
import { 
    IMPORT_DATA,
    AUTHORIZE_PROVIDER,
    devicesLoaded
} from './providerActions';
import { 
    USER_AUTHENTICATED, 
    USER_LOGGED_OUT 
} from '../authentication';
import {
    //Maybe rename this, could get confusing as this grows.
    OAUTH_REQUEST_SUCCESS
} from '../oauth';

import {
    addDevice, 
    getUserDeviceChannel 
} from './providerService';

import { 
    connect,
    applehealthSaga
} from './applehealth';
//Centralize the state of providers, and the user state.

function* saveProviderMetrics({ metrics, provider, contests }) {
    console.log(metrics, provider);
}

    //TODO - expand this for other providers.
function* onProviderAuthRequest({ provider }) {

    try {
        yield call(connect);
        //Now store on the user profile.
        yield call(addDevice, provider);
    }
    catch(err) {
        console.log(err);
    }
}

function* onOauthLink({ provider }) {
    yield call(addDevice, provider.key);
}

function* onDevicesLoaded(devices) {
    yield put(devicesLoaded(devices));
}

//Split this up potentially.
function* watchUserProviders() {
    const deviceChannel = getUserDeviceChannel();
    yield takeEvery(deviceChannel, onDevicesLoaded);
    yield takeEvery(IMPORT_DATA, saveProviderMetrics);
}

/**
 * 
 * @param {string} provider 
 */
function* addUserDevice(provider) {
    yield call(addDevice, provider);
}

function* authenticatedProviderSaga() {
    yield fork(watchUserProviders);
    yield takeEvery(
        AUTHORIZE_PROVIDER, 
        onProviderAuthRequest
    );
    //Grab all the successful oauth links.
    yield takeEvery(OAUTH_REQUEST_SUCCESS, onOauthLink);
    //
    yield fork(applehealthSaga);
}

export function* providerSaga() {

    while(true) {
        yield take(USER_AUTHENTICATED);

        const task = yield fork(authenticatedProviderSaga);

        yield take(USER_LOGGED_OUT);
        yield cancel(task);
    }    
}