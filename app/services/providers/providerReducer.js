import { 
    USER_DEVICES_LOADED, 
    DATA_LOAD_SUCCESS 
} from './providerActions';

import { 
    fitbit, 
    applehealth
} from './config';

const initialState = {
    loading: true,
    local: {
        applehealth
    },
    oauth : {
        fitbit
    },
    linked : { },
    data : {}
};

export const providerState = (state = initialState, action) => {
    switch(action.type) {
        case DATA_LOAD_SUCCESS:
            const { provider, data } = action;
            const update = {
                data : { 
                    ...state.data,
                    [provider] : data
                }
            };
            const test = Object.assign({}, state, update);
            console.log(test);
            return test;
        case USER_DEVICES_LOADED:
            const updates = {
                linked : action.devices,
                loading: false
            };
            return Object.assign({}, state, updates);
        default:
            return state;
    }
}