import { dataStructure } from '../config';
import firebase from 'react-native-firebase';

const { 
    profiles : {
        name : profileCollection,
        metrics : {
            name : metricsCollection
        }
    }
} = dataStructure;

/**
 * Takes an array of user metrics, and stores them in a bulk atomic operation.
 * @param {object[]} metrics 
 */
export const saveMetrics = async ( metrics ) => {
    const db = firebase.firestore();
    const collection = _getMetrics();
    const { serverTimestamp } = firebase.firestore.FieldValue;
    const batch = db.batch();
    
    metrics.forEach(metric => {
        batch.set(collection.doc(), {
            timestamp : serverTimestamp(),
            ...metric
        })
    });

    await batch.commit();
};

/**
 * Store the user metric to the users profile, the saga will emit results based on success or fail.
 * @param {object} metric 
 */
export const saveMetric = async (metric) => {
    const { serverTimestamp } = firebase.firestore.FieldValue;
    const collection = _getMetrics();

    await collection.add({
        timestamp : serverTimestamp(),
        ...metric
    });
};

//TODO add a subscription model for this as well.
//Also, add a date range possibly
export const loadMetrics = async () => {
    const db = _getMetrics();
    const res = await db.limit(50).get();
    const metrics = [];
    res.forEach(childSnapshot => {
        metrics.push(childSnapshot.data())
    });
    return metrics;
};

const _getMetrics = () => {
    const { uid } = firebase.app().auth().currentUser;
    const metrics = firebase.firestore()
        .collection(profileCollection)
        .doc(uid)
        .collection(metricsCollection);
    return metrics;
};