import { generateId } from '../../core';

export const SAVE_ACTIVITY_METRIC = 'SAVE_ACTIVITY_METRIC';
export const SAVE_BIOMETRIC = 'SAVE_BIOMETRIC';

export const SAVE_METRIC_LIST = 'SAVE_METRIC_LIST';

export const LOAD_METRICS = 'LOAD_METRICS';
export const LOAD_METRICS_SUCCESS = 'LOAD_METRICS_SUCCESS';
export const LOAD_METRICS_ERROR = 'LOAD_METRICS_ERROR';

export const METRIC_SAVE_SUCCESS = 'METRIC_SAVE_SUCCESS';
export const METRIC_SAVE_ERROR = 'METRIC_SAVE_ERROR';

export const loadMetrics = () => ({
    type : LOAD_METRICS
});

export const loadMetricsSuccess = (metrics) => ({
    type : LOAD_METRICS_SUCCESS,
    metrics
});

export const loadMetricsError = (error) => ({
    type : LOAD_METRICS_SUCCESS,
    error
});

export const saveActivityMetric = (metric, contests = []) => ({
    type : SAVE_ACTIVITY_METRIC,
    metric,
    contests,
    tempId : generateId()
});

export const saveBioMetric = (metric, contests = []) => ({
    type : SAVE_BIOMETRIC,
    metric,
    contests    
});

/**
 * Saves metrics in bulk.
 * @param {object[]} metrics 
 * @param {*} contests 
 */
export const saveMetrics = (metrics, contests = []) => ({
    type : SAVE_METRIC_LIST,
    metrics,
    contests,
    tempId : generateId()
});

export const metricSaveSuccess = (metric, contests = [], tempId) => ({
    type : METRIC_SAVE_SUCCESS,
    metric,
    contests,
    tempId
});

export const metricSaveError = (error, tempId) => ({
    type : METRIC_SAVE_ERROR,
    tempId,
    error
});