import { firebase } from '../firebaseService';
import { 
    put, 
    take, 
    all, 
    fork, 
    cancel, 
    call,
    takeEvery 
} from 'redux-saga/effects';

import {  
    LOAD_METRICS,
    SAVE_ACTIVITY_METRIC,
    SAVE_BIOMETRIC,
    SAVE_METRIC_LIST,
    loadMetricsError,
    loadMetricsSuccess,
    metricSaveError,
    metricSaveSuccess
} from './metricsActions';

import { 
    USER_AUTHENTICATED,
    USER_LOGGED_OUT
} from '../authentication';
import {
    saveMetric,
    saveMetrics,
    loadMetrics
} from './metricsService';

function* loadUserMetrics() {
    try {
        const metrics = yield call(loadMetrics);
        yield put(loadMetricsSuccess(metrics));
    }
    catch(err) {
        console.warn(err);
        yield put(loadMetricsError(err));
    }
}

function* saveUserMetric({ tempId, metric, contests }) {
    try {
        yield call(saveMetric, metric);
        yield put(metricSaveSuccess(metric, contests, tempId));
    }
    catch(err) {
        yield put(metricSaveError(err, tempId));
    }
}

//Merge with the singular call.
function* saveMetricList({ tempId, metrics, contests }) {
    try {
        yield call(saveMetrics, metrics);
        //Broadcast this.
        const successBroadcasts = metrics.map(metric => {
            return put(metricSaveSuccess(metric, contests, tempId));
        });
        yield all(successBroadcasts);
    }
    catch(err) {
        console.log(err);
        yield put(metricSaveError(err, tempId));
    }
}

function* takeMetricActions() {
    yield takeEvery(SAVE_METRIC_LIST, saveMetricList);
    yield takeEvery([ SAVE_ACTIVITY_METRIC, SAVE_BIOMETRIC ], saveUserMetric);
}

export function* metricsSaga() {
    //Re-evaluate the flow of these guys.
    yield takeEvery(LOAD_METRICS, loadUserMetrics);
    while(true) {
        yield take(USER_AUTHENTICATED);        
        const task = yield fork(takeMetricActions);
        yield take(USER_LOGGED_OUT);
        yield cancel(task);
    }
}