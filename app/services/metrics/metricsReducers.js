import {
    SAVE_ACTIVITY_METRIC,
    SAVE_BIOMETRIC,
    SAVE_METRIC_LIST,
    METRIC_SAVE_ERROR,
    METRIC_SAVE_SUCCESS,
    LOAD_METRICS_SUCCESS
} from './metricsActions';

const initialState = { 
    metrics : []
};

export const metricsState = (state = initialState, action) => {
    switch(action.type) {
        case LOAD_METRICS_SUCCESS:
            return Object.assign(
                {},
                state,
                {
                    metrics : action.metrics
                }
            );
        case SAVE_METRIC_LIST:
        case SAVE_ACTIVITY_METRIC:
        case SAVE_BIOMETRIC:
            return Object.assign(
                {},
                state,
                { [action.tempId] : { status : 'SAVING' } }
            )
        case METRIC_SAVE_ERROR:
            return Object.assign(
                {},
                state,
                { [action.tempId] : { status : 'ERROR', error : action.error } }
            )
        case METRIC_SAVE_SUCCESS:
            return Object.assign(
                {},
                state,
                { [action.tempId] : { status : 'SUCCESS' } }
            );
        default:
            return state;
    }
};