//All conversions 

//We will store all metrics in
//The metric system, so g, meters.
const meter = {
    mile : .00062137,
    inch : 39.37,
    yard : 1.0936,
    foot : 3.2808399
};

const kilogram = {
    pound :  2.20462,
    stone : 0.157473
};

export const config = {
    keys : {
        distance : 'distance',
        mass : 'mass',
        count : 'count'
    },
    defaults : {
        distance : 'mile',
        mass : 'pound'
    }
};

//Helper functions.
const from = (metricMap, unit, value) =>  value / metricMap[unit];
const to = (metricMap, unit, value) => value * metricMap[unit];

export const convert = {
    mass : {
        from : (unit, value) => from(kilogram, unit, value),
        to : (unit, value) => to(kilogram, unit, value)
    },
    distance : {
        from : (unit, value) => from(meter, unit, value),
        to : (unit, value) => to(meter, unit, value),
    }
};