import {eventChannel } from 'redux-saga';
import { fork, put, take, cancel, call, all, takeLatest } from 'redux-saga/effects';
import moment from 'moment';

import { firebase } from '../../firebaseService';
import { manager } from '../index';
import { 
    LOAD_FITBIT_ACTIVITIES, 
    FITBIT_ACTIVITIES_LOADED,
    LOAD_FITBIT_SUBSCRIPTION_DETAILS,
    FITBIT_SUBSCRIPTION_DETAILS_LOADED 
} from './fitbitActions';
import { fitbitConfig } from './index';


const rootPath = 'user-trackers/';
/**
 * {string}
 */
const provider = 'fitbit';

function* getAuthData() {
    const authData = yield manager.getProviderAuthData(provider);
    if(!authData) {
        const data = yield manager.authenticate(provider);
        return data;
    }
    return authData;
}
/**
 * 
 * @param { { activities : []} } param0 
 */
function* loadFitbitSubscriptionDetail({ activities }) {

    const authData = yield call(getAuthData);
    const { get_auth_header, api_url, get_activity_url } = fitbitConfig.provider;
    const headers = get_auth_header(authData);

    let requests = activities.map(act => {
        let url = get_activity_url(act);
        return fetch(url, { headers }).then(res => res.json().then(json => json));
    });

    let data = yield Promise.all(requests);

    yield put({
        type : FITBIT_SUBSCRIPTION_DETAILS_LOADED,
        data
    });
}

function* loadActivities({ lastSync }) {
    const authData = yield call(getAuthData);
    const { get_auth_header, api_url } = fitbitConfig.provider;
    const headers = get_auth_header(authData);
    const route = lastSync? 'list' : 'recent';
    const afterDate = moment(lastSync).utc().format('YYYY-MM-DDThh:mm:ss');
    let qs = `?afterDate=${afterDate}&offset=2&sort=desc&limit=20`;   
    const url = `${api_url}/user/-/activities/${route}.json${qs}`;

    const { activities, error } = yield fetch(url, { headers })
        .then(response => response.json().then(json => { 
            if(json.activities) 
                return json;
            return { activities : json }
        }))
        .catch(err => ({ error : err }));

    yield put({ 
        type : FITBIT_ACTIVITIES_LOADED,
        activities
    });
}

export function* fitbitSaga() {
    yield takeLatest(LOAD_FITBIT_ACTIVITIES, loadActivities);
    yield takeLatest(LOAD_FITBIT_SUBSCRIPTION_DETAILS, loadFitbitSubscriptionDetail);
}