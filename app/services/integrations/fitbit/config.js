const url = 'https://www.fitbit.com/oauth2/authorize';

const scopes = ['activity','heartrate','location','nutrition','profile','settings','sleep','social','weight'];
const clientId = '228CFZ';
const providerName = 'fitbit';
const redirectUrl = 'motus://activities/fitbit';

export const fitbitProvider = {
    key : providerName,
    scope : scopes,
    url,
    clientId,
    redirectUrl,
    responseType : 'token'
};

const fitbitApiVersion = '1';
const defaultHeader = { 'Accept-Language': 'en_US' };


const scopeDelimiter = ' ';
const authorizeUrlQs = `?response_type=token&client_id=${clientId}&scope=${scopes.join(scopeDelimiter)}`;
const rootApi = `https://api.fitbit.com`;
const provider = {
    auth_version: '2.0',
    scopes,
    request_token_url: 'url',
    authorize_url: encodeURI(`https://www.fitbit.com/oauth2/authorize${authorizeUrlQs}`) + '&redirect_uri=motus://activities/fitbit',
    access_token_url: `${rootApi}/oauth2/token`,
    api_url: `${rootApi}/${fitbitApiVersion}`,
    callback_url: ({app_name}) => `${app_name}://callback`,
    subscription_url : uid => `${rootApi}/${fitbitApiVersion}/user/-/apiSubscriptions/${uid}.json`,
    get_auth_header : authData => {
        return {
            Authorization : `Bearer ${authData.accessToken}`,
            ...defaultHeader
        }
    },
    get_activity_url : (activity) => {
        const path = `${rootApi}/${fitbitApiVersion}/user/-/${activity.collectionType}/date/${activity.date}.json`;
        return path;
    }
};

export const fitbitConfig = {
    provider,
    clientId,
    providerName,
    scopes,
    scopeDelimiter,
    defaultHeader
};