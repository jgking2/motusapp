import {
    LOAD_FITBIT_ACTIVITIES,
    FITBIT_ACTIVITIES_LOADED,
    LOAD_FITBIT_SUBSCRIPTION_DETAILS,
    FITBIT_SUBSCRIPTION_DETAILS_LOADED
} from './fitbitActions';

const initialState = {}; 

export const fitbitState = (state = initialState, action) => {
    switch(action.type) {
        case LOAD_FITBIT_ACTIVITIES:
            return Object.assign({}, state, { 
                activities : undefined
            });
        case FITBIT_ACTIVITIES_LOADED:
            const { activities } = action;
            return Object.assign({}, state, {
                activities
            });
        case LOAD_FITBIT_SUBSCRIPTION_DETAILS:
            return Object.assign({}, state, {
                subscriptionData : undefined
            });
        case FITBIT_SUBSCRIPTION_DETAILS_LOADED:
            return Object.assign({}, state, {
                subscriptionData : action.data
            });
        default:
            return state;
    }
};