export const LOAD_FITBIT_SUBSCRIPTION_DETAILS  = 'LOAD_FITBIT_SUBSCRIPTION_DETAILS';
export const FITBIT_SUBSCRIPTION_DETAILS_LOADED = 'FITBIT_SUBSCRIPTION_DETAILS_LOADED';

export const LOAD_FITBIT_ACTIVITIES = 'LOAD_FITBIT_ACTIVITIES';
export const FITBIT_ACTIVITIES_LOADED = 'FITBIT_ACTIVITIES_LOADED';

/**
 * 
 * @param {{ collectionType, ownerType, date }[]} activities 
 */
export const loadFitbitSubscriptionDetails = (activities) => {
    return {
        type : LOAD_FITBIT_SUBSCRIPTION_DETAILS,
        activities
    }
};

/**
 * 
 * @param {Date} lastSync 
 */
export const loadExtraActivities = (lastSync) => {
    return {
        type : LOAD_FITBIT_ACTIVITIES,
        lastSync
    }
};
