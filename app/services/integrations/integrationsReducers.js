import { 
    LOAD_USER_INTEGRATIONS,
    USER_INTEGRATIONS_LOADED,
    INTEGRATION_ACTIONS_RECEIVED
} from './integrationsActions';

let initialState = {
    loading: true,
    integrations: []
};

export const integrationsState = (state = initialState, action) => {
    switch(action.type) {
        case LOAD_USER_INTEGRATIONS:
            return initialState;
        case USER_INTEGRATIONS_LOADED:
            return {
                loading : false,
                integrations : [...action.integrations]
            }
        case INTEGRATION_ACTIONS_RECEIVED:
            let integrations = [...state.integrations];
            integrations.map(intgrtn => {
                let target = action.activities[intgrtn.provider] || [];
                intgrtn.events = Object.assign({}, target);
                return intgrtn;
            });
            return Object.assign({}, state, {
                integrations: integrations
            })
        default:
            return state;
    }
}