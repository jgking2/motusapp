//LOAD DEVICES
//ADD DEVICE

export const LOAD_USER_INTEGRATIONS = 'LOAD_USER_INTEGRATIONS';
export const USER_INTEGRATIONS_LOADED = 'USER_INTEGRATIONS_LOADED';
export const ADD_USER_INTEGRATION = 'LOAD_USER_INTEGRATION';
export const INTEGRATION_ACTIONS_RECEIVED = 'INTEGRATION_ACTIONS_RECEIVED';

export const loadUserIntegrations = () => { 
    return {
        type: LOAD_USER_INTEGRATIONS
    }
};

export const addUserIntegration = (provider, data) => {
    // console.info(provider, data);
    return {
        type: ADD_USER_INTEGRATION,
        provider,
        data
    }
};