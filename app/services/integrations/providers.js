import { fitbitConfig } from './fitbit';
import { OauthManager } from './oauth';

//Configure oauth core.
const manager = new OauthManager("motus");
manager.setProvider(fitbitConfig.providerName, fitbitConfig.provider);

const providers = manager.getAllProviders();

export {
    providers,
    manager
}