import { 
    Linking, 
    AsyncStorage 
} from 'react-native';

let appName;
let providers = {};

const hyphenToCamelCase = (text) => text.replace(/(_)([a-zA-Z])/g, 
    (match, p1, p2, index, text) => p2.toUpperCase());

export class OauthManager {
    constructor(appName) {
        appName = appName;
    }

    setProvider(provider, config) {
        providers[provider] = config;
    }
    /**
     * 
     * @param {string} name 
     */
    getProvider(name) {
        return Object.freeze(providers[name]);
    }
    /**
     * return {[String]}
     */
    getAllProviders() {
        return Object.keys(providers);
    }

    authenticate(provider) {
        return new Promise((resolve, reject) => {
            const config = providers[provider];   
            //additional timestamp parameter is to prevent an iOS issue when/if the user presses cancel on the redirect.
            const url = `${config.authorize_url}&timestamp=${new Date().valueOf()}`;
            Linking.openURL(url)
                .catch(err => null);
            Linking.addEventListener('url', (event) => {
                const { url } = event;               
                let qs = url.split('#')[1];
                //Format here similar to key=value&key2=value2
                const params = qs.split('&').map(val => {
                    let split = val.split('=');
                    return {[hyphenToCamelCase(split[0])] : split[1]};
                }).reduce((prev, curr) => {
                    return Object.assign(prev, curr);
                });
                Linking.removeEventListener('url');
                _setProviderAuthData(provider, params)
                    .then(res => {
                        resolve(params);
                    });
            });
        });
    }

    //TODO move this to actual integration.
    subscribe(uid, provider) {
        let config = providers[provider];
        return _getProviderAuthData(provider).then(data => {
            const header = new Headers(config.get_auth_header(data));
            const url = config.subscription_url(uid);
            return fetch(url, {
                headers: header,
                method: 'POST'
            }).then(res => console.info(res));
        });
    }

    /**
     * 
     * @param {string} provider 
     * @returns {Promise<{ accessToken, expiresIn, scope, tokenType, userId }>}
     */
    getProviderAuthData(provider) {
        return _getProviderAuthData(provider);
    }
}

/**
 * 
 * @param {string} provider 
 * @returns {Promise<{ accessToken, expiresIn, scope, tokenType, userId }>}
 */
const _getProviderAuthData = (provider) => {
    return AsyncStorage.getItem(provider)
        .then(res => {
            return JSON.parse(res);
        });
}


const _setProviderAuthData = (provider, data) => {
    return AsyncStorage
        .setItem(provider, JSON.stringify(data));
};

