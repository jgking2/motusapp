import { Constants } from 'rn-apple-healthkit';
const {  
    Permissions : PERMS 
} = Constants;

export const options = {
    permissions: {
        read: [
            PERMS.Height,
            PERMS.Weight,
            PERMS.StepCount,
            PERMS.DateOfBirth,
            PERMS.BodyFatPercentage,
            PERMS.NikeFuel,
            PERMS.DistanceWalkingRunning,
            PERMS.DistanceCycling,
            PERMS.FlightsClimbed
        ],
        write: [ ]//"Weight", "StepCount", "BodyMassIndex"]
    }
};