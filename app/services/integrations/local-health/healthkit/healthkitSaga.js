//@flow
import { put, take, call, takeEvery } from 'redux-saga/effects';
import { eventChannel  } from 'redux-saga';
import { Alert } from 'react-native';

import { initialize, getSteps } from './healthkit.service';
import { 
    LOAD_HEALTHKIT_STEPS, 
    HEALTHKIT_STEPS_LOADED,
    HEALTHKIT_STEPS_FAILED,
    INITIALIZE_HEALTHKIT,
    HEALTHKIT_PERMISSIONS_GRANTED,
    HEALTHKIT_PERMISSIONS_DENIED
} from './healthkitActions';


function* initializeHealthKit() {
    try {
        const { success } = yield initialize();
        yield put({
            type : HEALTHKIT_PERMISSIONS_GRANTED
        });
    } catch(error) {
        Alert.alert('errror', JSON.stringify(error));
        yield put({
            type : HEALTHKIT_PERMISSIONS_DENIED
        });
    }
}

/**
 * 
 * @param {Date} date 
 */
function* loadSteps({ date }: { date : Date }) {
    try {
        const { steps } = yield getSteps(date);
        yield put({
            type : HEALTHKIT_STEPS_LOADED,
            steps
        });
    }
    catch(error) {
        yield put({
            type : HEALTHKIT_STEPS_FAILED,
            error
        });
    }
}

export function* healthKitSaga() {
    yield takeEvery(INITIALIZE_HEALTHKIT, initializeHealthKit);
    yield takeEvery(LOAD_HEALTHKIT_STEPS, loadSteps);
}