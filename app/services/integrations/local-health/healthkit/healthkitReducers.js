//@flow
import {
    LOAD_HEALTHKIT_STEPS,
    HEALTHKIT_PERMISSIONS_GRANTED,
    HEALTHKIT_PERMISSIONS_DENIED,
    HEALTHKIT_STEPS_LOADED,
    HEALTHKIT_STEPS_FAILED
} from './healthkitActions';

export type HealthKitState = {
    authorized?: boolean;
    steps?: any;
};

let initialState : HealthKitState = {
    authorized : undefined,
    steps : {
        loading: false,
        failure : false,
        value : []
    }
};

export const healthKitState = (state : HealthKitState = initialState, action : any) : HealthKitState => {
    let authorized;
    switch(action.type) {
        case HEALTHKIT_STEPS_LOADED:
            const { steps } = action;
            return Object.assign({}, state, { 
                steps : { 
                    loading : false, 
                    failure : false, 
                    value : steps 
                }
            });
        case LOAD_HEALTHKIT_STEPS:
            return Object.assign({}, state, { steps : { loading : true } });
        case HEALTHKIT_PERMISSIONS_GRANTED:
            authorized = true;
            return Object.assign({}, state, { authorized });
        case HEALTHKIT_PERMISSIONS_DENIED:
            authorized = false;
            return Object.assign({}, state, { authorized });
        case HEALTHKIT_STEPS_FAILED:
            const { error } = action;
            console.info(error);
            return Object.assign({}, state, { 
                steps : { 
                    failure : { 
                        error
                    }
                }
            });
        default:
            return state;
    }
}