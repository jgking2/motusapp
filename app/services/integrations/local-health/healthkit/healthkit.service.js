//@flow
import AppleHealthKit from 'rn-apple-healthkit';
import { options } from './permissions';

export const initialize = function () : Promise<any> {
    return new Promise((resolve : Function, reject : Function) => {
        AppleHealthKit.initHealthKit(options, (err, res) => {
            if(err) {            
                return reject(err);
            }
            return resolve({ res });
        });
    });
};

//Pass the specified to grab steps.
export const getSteps = (date : Date = new Date()) : Promise<any> => {
    let options;
    if(date instanceof Date)
        options = {
            date: date.toISOString()
        };
    return new Promise((resolve, reject) => {
        AppleHealthKit.getStepCount(options, (err, res) => {
            if(err) {
                return reject(err);
            }
            //res.value is actual steps, wrap this into the activity format.
            return resolve({ steps : res.value });
        });
    });
};

//
export const getCycling = ({ unit, date } : { unit : String, date : Date }) : Promise<any> => {
    //Check unit is valid.
    return new Promise((resolve, reject) => {
        let option = {
            unit : unit || 'mile',
            date : (date || new Date()).toISOString()
        };
        AppleHealthKit.getDistanceCycling(options, (err, res) => {
            if(err) {
                return reject(err);
            }
            //Format this bad boy.
            console.info(res);
            return resolve(res);
        });
    });
};

//Can tweak this to take a range, potentially store user weight in healthkit for privacy?
export const getWeight = () : Promise<any> => {
    return new Promise((resolve, reject) => {
        console.info('err, weigh');
        AppleHealthKit.getLatestWeight(null, (err, weight) => {
            console.warn('result---');
            if(err) {
                return reject(err);
            }
            console.info(weight);
            return resolve(weight);
        })
    });
};

export const getSex = () : Promise<any> => {
    return new Promise((resolve, reject) => {
        AppleHealthKit.getBiologicalSex(null, (err, res) => {
            if(err) {
                return reject(err);
            }
            console.info(res);
            return resolve(res);
        })
    });
};

export const getDob = () : Promise<any> => {
    return new Promise((resolve, reject) => {
        AppleHealthKit.getDateOfBirth(null, (err, res) => {
            if(err) {
                return reject(err);
            }
            //{ age, value }
            console.info(res);
            return resolve(res);
        })
    });
};

export const getBodyFatPercentage = () : Promise<any> => {
    return new Promise((resolve, reject) => {
        AppleHealthKit.getLatestBodyFatPercentage(null, (err, res) => {
            if(err) {
                return reject(err);
            }
            //{ startDate, value, endDate }
            console.info(res);
            return resolve(res);
        });
    });
};