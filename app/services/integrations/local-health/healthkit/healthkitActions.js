//Initialization actions
export const INITIALIZE_HEALTHKIT = 'INITIALIZE_HEALTHKIT';
export const HEALTHKIT_PERMISSIONS_DENIED = 'HEALTHKIT_PERMISSIONS_DENIED';
export const HEALTHKIT_PERMISSIONS_GRANTED = 'HEALTHKIT_PERMISSIONS_GRANTED';
export const LOAD_HEALTHKIT_STEPS = 'LOAD_HEALTHKIT_STEPS';
export const HEALTHKIT_STEPS_LOADED = 'HEALTHKIT_STEPS_LOADED';
export const HEALTHKIT_STEPS_FAILED = 'HEALTHKIT_STEPS_FAILED';

export const loadSteps = (date) => {
    return {
        type : LOAD_HEALTHKIT_STEPS,
        date
    }
};

export const initializeHealthkit = () => {
    return {
        type : INITIALIZE_HEALTHKIT        
    }
};
