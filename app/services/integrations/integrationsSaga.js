import {USER_PROFILE_LOADED} from '../user-profile';
import {USER_LOGGED_OUT} from '../authentication';
import { USER_INTEGRATIONS_LOADED, ADD_USER_INTEGRATION, INTEGRATION_ACTIONS_RECEIVED } from './integrationsActions';
import {eventChannel} from 'redux-saga';
import { AsyncStorage } from 'react-native';
import { fork, put, take, cancel, call, all } from 'redux-saga/effects';
import { firebase } from '../firebaseService';
import { manager } from './index';

const rootPath = 'user-trackers/';

const getUserIntegrationsEventChannel = (uid) => {
    let path = `${rootPath}${uid}`;
    return eventChannel(emit => {
        const dbRef = firebase.database().ref(path);
        dbRef.on('value', snapshot => {
            const integrations = [];
            snapshot.forEach(child => {
                integrations.push({
                    key : child.key,
                    ...child.val()
                })
            });
            emit(integrations);
        });
        return () => dbRef.off();
    });
}

const getIntegrationActionsEventChannel = (uid) => {
    let path = `user-subscribed-events/${uid}`;
    return eventChannel(emit => {
        const dbRef = firebase.database()
            .ref(path)
            .limitToLast(50);

        dbRef.on('value', snapshot => {
            //TODO - come take a look at this.
            let deviceActions = snapshot.toJSON();
            // let deviceActions = { };
            // snapshot.forEach(child => {
            //     let provider = child.val().source;
            //     if(!deviceActions[provider]) {
            //         deviceActions[provider] = [];
            //     }                
            //     deviceActions[provider].push(child.val());
            // });
            if(deviceActions) emit(deviceActions);
        });
        return () => dbRef.off();
    });
}

/**
 * 
 * @param {{providerName, provider}} provider 
 * @param {{ expiresIn, expiresAt, accessToken, userId, tokentype}} data 
 */
// function storeLocalSubscription(provider, data) {
//     return AsyncStorage.setItem(provider, JSON.stringify(data));
// }

function saveUserDeviceSubscription(uid, integration) {
    let path = `${rootPath}/${uid}`;
    const dbRef = firebase.database().ref(path);
    return dbRef.push(integration);
}

function* addDeviceSubscription(uid) {
    while(true) {
        let { provider, data } = yield take(ADD_USER_INTEGRATION);
        data.timestamp = firebase.database.ServerValue.TIMESTAMP;
        yield call(manager.subscribe, uid, provider);
        yield call(saveUserDeviceSubscription, uid, { provider, ...data});
    }
}

function* watchIntegrations(uid) {
    let channel = getUserIntegrationsEventChannel(uid);
    while(true) {
        let integrations = yield take(channel);
        yield put({
            type : USER_INTEGRATIONS_LOADED,
            integrations
        });
    }
}

//See how this grows, and come back to tweak / refactor.  Even the naming sucks, but this should work.
function* watchIntegrationActivity(uid) {
    let channel = getIntegrationActionsEventChannel(uid);
    while(true) {
        let activities = yield take(channel);
        yield put({
            type : INTEGRATION_ACTIONS_RECEIVED,
            activities
        })
    }
}

export function* userIntegrationsSagas() {
    while(true) {        
        const { user, profile } = yield take(USER_PROFILE_LOADED);
        let subscriptionEventsTask = yield fork(watchIntegrations, user.uid);
        let addIntegrationListenerTask = yield fork(addDeviceSubscription, user.uid);
        let watchActivityTask = yield fork(watchIntegrationActivity, user.uid);
        yield take(USER_LOGGED_OUT);
        yield all([
            cancel(watchActivityTask),
            cancel(subscriptionEventsTask),
            cancel(addIntegrationListenerTask)
        ])
    }
}