import { 
    CONTEST_UPDATE, 
    CONTEST_CREATED, 
    CONTESTS_LOADING,
    CREATE_CONTEST,
    INVITATION_CONTEST_LOADED,
    INVITATION_CONTEST_LOAD_FAILURE,
    LOAD_CONTEST_FROM_INVITATION,
    CLEAR_LOADED_CONTEST
} from './contestActions';

const defaultJoining = { loading : false, contest: undefined, error : '' };

const initialState = {
    loading: true,
    contests: { },
    creating : {},
    joining: defaultJoining
 };

export const contestState = (state = initialState, { 
    type, 
    tempId, 
    contests,
    contest,
    contestId
}) => {
    let update;
    switch(type) {
        case CONTESTS_LOADING: 
            update = {
                loading: true,
                contests : { }
            };
            return Object.assign({}, state, update);
        case CONTEST_UPDATE:
            update = {
                loading : false,
                contests: {
                    ...state.contests,
                    ...contests
                }
            };
            return Object.assign({}, state, update);
        case CREATE_CONTEST:
            update = {
                creating: { [tempId] : contest }
            };
            return Object.assign({}, state, update);
        case CONTEST_CREATED: 
            update = {
                creating: {
                    ...state.creating,
                    [tempId] : contest
                }
            };
            return Object.assign({}, state, update);
        case LOAD_CONTEST_FROM_INVITATION:
            update = {
                joining : {
                    loading: true
                }
            }
            return Object.assign({}, state, update);
        case INVITATION_CONTEST_LOADED:
            let error = contest && undefined || 'Unable to find contest, try again.';
            update = {
                joining: {
                    loading : false,
                    contest,
                    error
                }
            }
            return Object.assign({}, state, update);
        case INVITATION_CONTEST_LOAD_FAILURE: 
            update = {
                joining : {
                    loading : false,
                    contest : undefined,
                    error : 'Unable to find a contest with the provided information.'
                }
            }
            return Object.assign({}, state, update);
        case CLEAR_LOADED_CONTEST:
            update = {
                joining : defaultJoining
            };
            return Object.assign({}, state, update);
        default:
            return state;
    }
};