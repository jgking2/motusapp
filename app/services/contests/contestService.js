import { eventChannel } from 'redux-saga';
import firebase from 'react-native-firebase';

import { dataStructure } from '../config';


const { 
    name : collectionName 
} = dataStructure.contests;

/**
 * 
 * @param {string} contestId 
 * @param {string} uid 
 * @returns {string}
 */
export const generateInvitationToken = (contestId) => {
    const { uid } = firebase.app().auth().currentUser;
    const rawString = `${(+new Date()).toString(16)}.${uid}.${contestId}`;
    const token = btoa(rawString);
    return token;
};

/**
 * 
 * @param {string} contestId 
 * @param {object} update The key value pairs to 
 */
export const updateContest = async (contestId, update) => {
    const contestRef = firebase.firestore()
        .collection(collectionName)
        .doc(contestId);
    
    await contestRef.update(update);
};

/**
 * 
 * @param {string} contestId 
 * @returns {void}
 */
export const joinContest = async (contestId) => {    
    const members = createContestMembersObj(true);
    const contestRef = firebase.firestore()
                .collection(collectionName)
                .doc(contestId);

    await contestRef
        .update(members);
};

/**
 * TODO maybe change how this works.
 * @param {string} contestId 
 * @returns {any}
 */
export const loadContestFromInvitation = async (contestId) => {    
    const members = createContestMembersObj();
    const contestRef = firebase.firestore()
                .collection(collectionName)
                .doc(contestId);

    const contestSnapshot = await contestRef.get();
    let contest = contestSnapshot.data();
    if(!!contest) {
        return {
            ...contest,
            key : contestSnapshot.id
        }
    }
    return undefined
};

//Create contest then broadcast with Id.
export const createContest = async (contest) => {
    const { uid : createdBy } = firebase.app().auth().currentUser;
    const { serverTimestamp } = firebase.firestore.FieldValue;
    const members = createContestMembersObj();
    const saveData = {
        timestamp : serverTimestamp(),
        createdBy,
        members,
        ...contest
    };

    const db = firebase.firestore().collection(collectionName);
    return await db.add(saveData)
        .then(res => Object.assign({}, contest, { key : res.id }));
};

/**
 * Listens for live details coming in about the contest, and generates a channel to emit the data.
 * @param {string} contestId 
 */
export const watchUserContests = () => {    
    const { uid } = firebase.app().auth().currentUser;

    return eventChannel(dispatch => {

        const userContestQuery = firebase.firestore()
            .collection(collectionName)
            .orderBy(`members.${uid}`);
        
        const unsubscribe = userContestQuery
            .onSnapshot(snapshot => {
                let contests = { };
                snapshot.forEach(childSnapshot => {
                    const { id : key } = childSnapshot;
                    contests[key] = {
                        key,
                        ...childSnapshot.data()
                    }
                });
                dispatch({ contests });
            });
        return () => unsubscribe();
    });
};

/**
 * @returns {Promise<[]>}
 */
export const loadUserContests = async () => {
    const { uid } = firebase.app().auth().currentUser;
    const { serverTimestamp } = firebase.firestore.FieldValue;
    const userContestQuery = firebase.firestore()
                .collection(collectionName)
                .orderBy(`members.${uid}`)
                .get();

    const userContests = await userContestQuery        
        .then(snapshot => {
            const contests = { };
            snapshot.forEach(childSnapshot => {
                contests[childSnapshot.id] = childSnapshot.data()
            });
            return contests;
        });

    return userContests;
};

/**
 * @returns {{ members: {[uid] : { displayName, photoURL }} }}
 */
const createContestMembersObj = (isUpdate = false) => {
    const { 
        uid, 
        displayName, 
        photoURL 
    } = firebase.app().auth().currentUser;
    const { serverTimestamp } = firebase.firestore.FieldValue;

    const prefix = isUpdate? `members.` : '';
    return {
        [`${prefix}${uid}`] : {
            displayName,
            photoURL,
            joined : serverTimestamp()
        }
    }
};