import { 
    put, 
    take, 
    fork, 
    cancel, 
    call, 
    takeEvery 
} from 'redux-saga/effects';

import {
    CONTEST_UPDATE, 
    CONTEST_DETAIL_REQUEST, 
    CREATE_CONTEST, 
    CONTEST_CREATED,
    JOIN_CONTEST,
    UPDATE_CONTEST,
    invitationTokenGenerated,
    invitationContestLoaded,
    contestsLoading,
    unableToLoadContestInvitation,
    LOAD_CONTEST_FROM_INVITATION,
    joinContestSuccess,
    joinContestError
} from './contestActions';

import { 
    USER_AUTHENTICATED, 
    USER_LOGGED_OUT 
} from '../authentication';

import {
    uploadFile
} from '../file-upload';

import { 
    joinContest,
    createContest, 
    watchUserContests,
    loadUserContests,
    generateInvitationToken,
    loadContestFromInvitation,
    updateContest
} from './contestService';

function* generateInviteToken({ contestId }) {
    const token = yield call(generateInvitationToken, contestId);
    yield put(invitationTokenGenerated(token, contestId));
}

function* onUpdateContest({ contestId, update = {}, imageUri }) {
    try {
        let addtl = { };
        if(imageUri) {
            const {  tempId : imageTempId } = yield put(
                uploadFile(imageUri, 'contest-photo')
            );
            response = yield take(
                (action) => action.tempId === imageTempId
            );
            addtl.photoURL = response.url;
        }
        yield call(updateContest, contestId, Object.assign({}, update, addtl));
        //Put info out there
    }
    catch(err) {
        //Put info out there
    }
}

function* getContestFromInvitation({ contestId }) {
    try {
        const contest = yield call(loadContestFromInvitation, contestId);
        yield put(invitationContestLoaded(contest));
    }
    catch (err) {
        yield put(unableToLoadContestInvitation());
    }
}

function* onContestDataStream({ contests }) {
    yield put({
        type : CONTEST_UPDATE,
        contests
    });
}

function* onCreateContestRequest({ contest, imageUri, tempId }) {
    //If a photo is attached, upload it!
    // let contestToUpload = contest;
    let url;
    if(imageUri) {
        const {  tempId : imageTempId } = yield put(
            uploadFile(imageUri, 'contest-photo')
        );
        response = yield take(
            (action) => action.tempId === imageTempId
        );
        url = response.url;
    }

    let addtionalData = imageUri? { photoURL : url } : {}; 
    
    const contestToUpload = Object.assign({}, contest, addtionalData);
    
    const newContest = yield call(createContest, contestToUpload);

    yield put({
        type : CONTEST_CREATED,
        contest : newContest,
        tempId : tempId
    });
}

function* addContestMember({ contestId }) {
    let responseAction = joinContestSuccess(contestId);
    try {
        yield call(joinContest, contestId);
    }
    catch (err) {
        console.log(err);
        responseAction = joinContestError(contestId, err);
    }
    yield put(responseAction);
}

/**
 * 
 */
function* beginContestSagas() {
    const contestChannel = watchUserContests();
    yield put(contestsLoading());
    yield takeEvery(contestChannel, onContestDataStream);
    yield takeEvery(CREATE_CONTEST, onCreateContestRequest);
    yield takeEvery(LOAD_CONTEST_FROM_INVITATION, getContestFromInvitation);   
    yield takeEvery(JOIN_CONTEST, addContestMember); 
    yield takeEvery(UPDATE_CONTEST, onUpdateContest);
}

/**
 * Watches for the list of contests.
 */
export function* contestSaga() {
    let contestWatchers;

    while(true) {
        yield take(USER_AUTHENTICATED);

        let task = yield fork(beginContestSagas);

        yield take(USER_LOGGED_OUT);
        yield cancel(task);
    }
}