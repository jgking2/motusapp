import { generateId } from '../../core';

export const contestFeeTypeMap = {
    Sponsored : 1,
    Paid: 2,
    Free : 3
};

// export const SUBSCRIBE_TO_CONTESTS = 'SUBSCRIBE_TO_CONTESTS';
export const CONTESTS_LOADING = 'CONTESTS_LOADING';
export const CONTEST_UPDATE = 'CONTEST_UPDATE';
export const CONTEST_DETAIL_REQUEST = 'CONTEST_DETAIL_REQUEST';
export const CREATE_CONTEST = 'CREATE_CONTEST';
export const CONTEST_CREATED = 'CONTEST_CREATED';

export const UPDATE_CONTEST = 'UPDATE_CONTEST';
export const UPDATE_CONTEST_SUCCESS = 'UPDATE_CONTEST_SUCCESS';
export const UPDATE_CONTEST_ERROR = 'UPDATE_CONTEST_ERROR';

export const LOAD_CONTEST_FROM_INVITATION = 'LOAD_CONTEST_FROM_INVITATION';
export const INVITATION_CONTEST_LOADED = 'INVITATION_CONTEST_LOADED';
export const INVITATION_CONTEST_LOAD_FAILURE = 'INVITATION_CONTEST_LOAD_FAILURE';
export const CLEAR_LOADED_CONTEST = 'CLEAR_LOADED_CONTEST';

export const GENERATE_INVITATION_TOKEN = 'GENERATE_INVITATION_TOKEN';
export const INVITATION_TOKEN_GENERATED = 'INVITATION_TOKEN_GENERATED';

export const JOIN_CONTEST = 'JOIN_CONTEST';
export const JOIN_CONTEST_SUCCESS = 'JOIN_CONTEST_SUCCESS';
export const JOIN_CONTEST_ERROR = 'JOIN_CONTEST_ERROR';

//Removes the contest from the Join Contest screen.
export const clearLoadedContest = () => ({
    type : CLEAR_LOADED_CONTEST
});

export const joinContest = (contestId) => ({
    type : JOIN_CONTEST,
    contestId
});

export const joinContestSuccess = (contestId) => ({
    type : JOIN_CONTEST_SUCCESS,
    contestId
});

export const joinContestError = (contestId, error) => ({
    type : JOIN_CONTEST_ERROR,
    contestId,
    error
});

export const contestsLoading = () => ({
    type : CONTESTS_LOADING
});

export const loadContestFromInvitation = (contestId) => ({
    type : LOAD_CONTEST_FROM_INVITATION,
    contestId
});

export const invitationContestLoaded = (contest) => ({
    type : INVITATION_CONTEST_LOADED,
    contest
});

export const unableToLoadContestInvitation = () => ({
    type : INVITATION_CONTEST_LOAD_FAILURE
});

//Likely pass more info eventually.
export const generateInvitationToken = (contestId) => ({
    type : GENERATE_INVITATION_TOKEN,
    contestId
});

export const invitationTokenGenerated = (token, contestId) => ({
    type : INVITATION_TOKEN_GENERATED,
    token,
    contestId
});

//Request contest details
export const requestContestDetail = (contestId) => {
    return {
        type: CONTEST_DETAIL_REQUEST,
        contestId
    }
}

export const updateContest = (contest, update, imageUri = '') => ({
    type : UPDATE_CONTEST,
    contestId: contest.key,
    update,
    imageUri
});

/**
 * 
 * @param {{ }} contest 
 * @param {string | Blob} imageUri 
 */
export const createContest = (contest, imageUri) => {
    return {
        type : CREATE_CONTEST,
        contest,
        imageUri,
        tempId : generateId()
    }
};