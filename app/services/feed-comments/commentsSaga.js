import { 
    take, 
    takeEvery, 
    put, 
    fork,
    call,
    cancel
} from 'redux-saga/effects';

import { 
    SUBMIT_FEED_COMMENT,
    SUBSCRIBE_TO_FEED_COMMENTS,
    UNSUBSCRIBE_TO_FEED_COMMENTS,
    submitFeedCommentSuccess,
    feedCommentsLoaded
} from './commentActions';
import { 
    loadComments, 
    submitComment, 
    getFeedCommentsChannel 
} from './commentService';

import { USER_AUTHENTICATED, USER_LOGGED_OUT } from '../authentication';

function *submitFeedComment({ comment, feedItem, contest }) {
    try {

        yield call(
            submitComment, 
            comment, 
            feedItem, 
            contest.key
        );

        yield put(
            submitFeedCommentSuccess(comment, feedItem, contest)
        );
    }
    catch(err) {
        console.warn(err);
    }
}

function* subscribeToComments({ feedId, contestId }) {
    const commentsChannel = getFeedCommentsChannel(feedId, contestId);
    const task = yield takeEvery(commentsChannel, onCommentsLoaded, feedId);
    yield take(UNSUBSCRIBE_TO_FEED_COMMENTS);
    yield cancel(task);
}

function* onCommentsLoaded(feedId, comments) {
    yield put(feedCommentsLoaded(feedId, comments));
}

function* feedCommentsInit() {
    yield takeEvery(SUBMIT_FEED_COMMENT, submitFeedComment);
    yield takeEvery(SUBSCRIBE_TO_FEED_COMMENTS, subscribeToComments);
}

//TODO reduce code, and make the process a bit more clear.
export function* feedCommentsSaga() {
    while(true) {
        yield take(USER_AUTHENTICATED);

        const task = yield fork(feedCommentsInit);

        yield take(USER_LOGGED_OUT);
        yield cancel(task);
    }
}