import firebase from 'react-native-firebase';
import { eventChannel } from 'redux-saga';

import { dataStructure } from '../config';

//Extracts name from structure.
const {
    contests: {
        name : contestCollection,
        feed: {
            name : feedCollection,
            comments : {
                name : commentsCollection
            }
        }
    }
} = dataStructure;

const commentLoadLimit = 20;

/**
 * 
 * @param {string} comment 
 * @param {{ key }} feedItem 
 * @param {string} contestId 
 */
export const submitComment = async (comment, feedItem, contestId) => {
    const { uid } = firebase.app().auth().currentUser;
    const { serverTimestamp } = firebase.firestore.FieldValue;

    const insertData = {
        uid,
        timestamp : serverTimestamp(),
        comment
    };

    const feedRef = _getContestFeed(contestId);
    const feedCommentRef = feedRef.doc(feedItem.key)
        .collection(commentsCollection);
    
    const result = await feedCommentRef.add(insertData);
};

//TODO - flesh this out, but we can use this to load more at the top of scrolls.
/**
 * 
 * @param {string} feedId 
 * @param {string} contestId 
 * @param {number} skip 
 * @returns {Promise<object>}
 */
export const loadComments = async (feedId, contestId, skip = 0) => {
    const feedRef = _getContestFeed(contestId);
    const feedCommentRef = feedRef.doc(feedId)
        .collection(commentsCollection);
    const comments = await feedCommentRef.limit(commentLoadLimit)
        .get()
        .then(snapshot => {
            const results = {  };
            snapshot.forEach(childSnapshot => {
                results[childSnapshot.id] = childSnapshot.data();
            });
            return results;
        });
    return comments;
}

/**
 * 
 * @param {string} feedId 
 * @param {string} contestId 
 * @param {*} skip 
 */
export const getFeedCommentsChannel = (feedId, contestId, skip = 0) => {
    return eventChannel(dispatch => {
        const feedRef = _getContestFeed(contestId);
        const feedCommentRef = feedRef.doc(feedId)
            .collection(commentsCollection);
        const unsubscribe = feedCommentRef.limit(commentLoadLimit)
            .onSnapshot(snapshot => {
                const results = {  };
                snapshot.forEach(childSnapshot => {
                    results[childSnapshot.id] = childSnapshot.data();
                });
                dispatch(results);
            });
        return () => unsubscribe();
    });
}

/**
 * 
 * @param {string} contestId 
 * @returns {{ add:function, doc:function, get:function }}
 */
const _getContestFeed = (contestId) => {
    const feed = firebase.firestore()
        .collection(contestCollection)
        .doc(contestId)
        .collection(feedCollection);
    return feed;
};