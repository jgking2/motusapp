import { 
    FEED_COMMENTS_LOADED
} from './commentActions';

//TODO verify the feed item exists.
const initialState = { };

export const feedCommentsState = (state = initialState, { type, feedId, comments }) => {
    switch(type) {
        case FEED_COMMENTS_LOADED:
            const update = {
                [feedId] : {
                    ...state[feedId],
                    ...comments
                }
            };
            return Object.assign({}, state, update);
            return state;
        default:
            return state;
    }
}