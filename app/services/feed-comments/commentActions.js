export const LOAD_FEED_COMMENTS = 'LOAD_FEED_COMMENTS';
export const SUBSCRIBE_TO_FEED_COMMENTS = 'SUBSCRIBE_TO_FEED_COMMENTS';
export const UNSUBSCRIBE_TO_FEED_COMMENTS = 'UNSUBSCRIBE_TO_FEED_COMMENTS';
export const FEED_COMMENTS_LOADED = 'FEED_COMMENTS_LOADED';
export const SUBMIT_FEED_COMMENT = 'SUBMIT_FEED_COMMENT';
export const SUBMIT_FEED_COMMENT_SUCCESS = 'SUBMIT_FEED_COMMENT_SUCCESS';

export const subscribeToFeedComments = (feedId, contestId) => ({
    type : SUBSCRIBE_TO_FEED_COMMENTS,
    feedId,
    contestId
});

//Maybe specify feed / contest later?
export const unsubscribeToFeedComments = () => ({
    type : UNSUBSCRIBE_TO_FEED_COMMENTS
});

export const submitFeedComment = (comment, feedItem, contest) => ({
    type : SUBMIT_FEED_COMMENT,
    comment,
    feedItem,
    contest
});

export const submitFeedCommentSuccess = (comment, feedItem, contest) => ({
    type : SUBMIT_FEED_COMMENT_SUCCESS,
    comment,
    feedItem,
    contest
});

export const feedCommentsLoaded = (feedId, comments) => ({
    type : FEED_COMMENTS_LOADED,
    feedId,
    comments
});