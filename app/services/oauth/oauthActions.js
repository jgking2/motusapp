import { provider } from './structures';
//Actions
//LOGIN
//LOGOUT

export const REQUEST_OAUTH_DIALOG = 'REQUEST_OAUTH_DIALOG';
export const OAUTH_REQUEST_SUCCESS = 'OAUTH_REQUEST_SUCCESS';
export const OAUTH_REQUEST_ERROR = 'OAUTH_REQUEST_ERROR';
export const MAKE_OAUTH_REQUEST = 'MAKE_OAUTH_REQUEST';

/**
 * 
 * @param {provider} provider 
 */
export const requestOauthDialog = (provider) => ({
    type : REQUEST_OAUTH_DIALOG,
    provider
});

/**
 * Allows retrying request, handling refresh token etc., this may get a bit bigger.
 * @param {RequestInfo} request 
 * @param {string} provider 
 */
export const makeOauthRequest = (request, provider, returnType) => ({
    type : MAKE_OAUTH_REQUEST,
    request,
    provider,
    returnType
});

/**
 * 
 * @param {provider} provider 
 */
export const oauthRequestSuccess = (provider) => ({
    type : OAUTH_REQUEST_SUCCESS,
    provider
});

export const oauthRequestError = (data) => ({
    type : OAUTH_REQUEST_ERROR
});