import { 
    put, 
    take, 
    takeLatest,
    call
} from 'redux-saga/effects';

import {  } from '../authentication';
import { 
    REQUEST_OAUTH_DIALOG, 
    oauthRequestError, 
    oauthRequestSuccess 
} from './oauthActions';
import { provider } from './structures';
import { authenticate } from './oauthService';


function* handleOauthRequest({ provider }) {
    try {
        const response = yield call(authenticate, provider);
        console.log(provider);
        yield put(oauthRequestSuccess(provider));
    }
    catch(err) {
        console.log(err);
    }
}

function* initOauth() {

}

export function* oauthSaga() {
    yield takeLatest(
        REQUEST_OAUTH_DIALOG, 
        handleOauthRequest
    );
}