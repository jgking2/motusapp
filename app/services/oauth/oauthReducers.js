import { 
    OAUTH_REQUEST_SUCCESS
} from './oauthActions';

const initialState = {}

export const oauthState = (state = initialState, action) => {
    switch(action.type) {
        case OAUTH_REQUEST_SUCCESS:
            return state;
        default:
            return state;
    }
};