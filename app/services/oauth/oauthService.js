import { 
    Linking, 
    AsyncStorage 
} from 'react-native';

import { provider } from './structures';

const defaultScopeDelimiter = ' ';

/**
 * 
 * @param {provider} provider 
 */
export const authenticate = (provider) => {
    return new Promise((resolve, reject) => {
        
        //additional timestamp parameter is to prevent an iOS issue when/if the user presses cancel on the redirect.
        const callUrl = generateUrlFromProvider(
            provider,
            `&timestamp=${new Date().valueOf()}`
        );

        Linking
            .openURL(callUrl)
            .catch(err => {
                console.log(err);
            });
        

        Linking
            .addEventListener('url', (event) => {
                const { url } = event;               
                let qs = url.split('#')[1];
                //Format here similar to key=value&key2=value2
                const params = qs.split('&').map(val => {
                    let split = val.split('=');
                    return {[hyphenToCamelCase(split[0])] : split[1]};
                }).reduce((prev, curr) => {
                    return Object.assign(prev, curr);
                });
                Linking.removeAllListeners("url");
                resolve(params);
            });
    });
}

/**
 * 
 * @param {string} provider 
 * @returns {Promise<{ accessToken, expiresIn, scope, tokenType, userId }>}
 */
const _getProviderAuthData = (provider) => {
    return AsyncStorage.getItem(provider)
        .then(res => {
            return JSON.parse(res);
        });
}


const _setProviderAuthData = (provider, data) => {
    return AsyncStorage
        .setItem(provider, JSON.stringify(data));
};

/**
 * 
 * @param {provider} provider 
 * @returns {string}
 */
const generateUrlFromProvider = (provider, addtl = '') => {
    const { 
        scope, 
        scopeDelimiter, 
        url, 
        redirectUrl,
        clientId,
        responseType
    } = provider;

    const scopes = scope
        .join(scopeDelimiter || defaultScopeDelimiter);
    const oauthUrl = `${url}?scope=${scopes}&response_type=${responseType}&client_id=${clientId}&redirect_uri=${redirectUrl}${addtl}`;
    return oauthUrl;
};

const hyphenToCamelCase = (text) => text.replace(/(_)([a-zA-Z])/g, 
(match, p1, p2, index, text) => p2.toUpperCase());
