import createSagaMiddleWare from 'redux-saga';
import { all, fork } from 'redux-saga/effects';
import { Alert } from 'react-native';

import { authSaga } from './authentication';
import { userProfileSaga } from './user-profile';
import { contestSaga } from './contests';
import { messagesSaga } from './messages';
import { usersSaga } from './users'
import { activitySaga } from './activity';
//Pulls in google fit, or healthkit.
import googleFitSaga from './integrations/local-health/google-fit';

import { fitbitSaga } from './integrations/fitbit';
import { fileUploadSaga } from './file-upload';
import { invitationSaga } from './invitations';
import { notificationsSaga } from './notifications';
import { connectivitySaga } from './connectivity';

export function* rootSaga() {
    try {
        yield all([
            authSaga(),
            userProfileSaga(),
            contestSaga(),
            messagesSaga(),
            usersSaga(),
            userIntegrationsSagas(),
            activitySaga(),
            fileUploadSaga(),
            invitationSaga(),
            googleFitSaga(),
            fitbitSaga(),
            notificationsSaga(),
            connectivitySaga()
        ])
    } catch ( err ) {
        Alert.alert('Send this to Joe', JSON.stringify(err));
    }
}