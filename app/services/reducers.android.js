import { combineReducers } from 'redux';
import { reducer } from 'redux-form'

import { authStatus } from './authentication';
import { userState } from './user-profile';
import { contestState } from './contests';
import { messageState } from './messages';
import { usersState } from './users';
import { integrationsState } from './integrations';
import { invitationsState } from './invitations';
import { fitbitState } from './integrations/fitbit';

export let appReducers = combineReducers({
    authStatus,
    userState,
    contestState,
    messageState,
    usersState,
    integrationsState,
    invitationsState,
    fitbitState,
    form: reducer
});