import {
    takeEvery, 
    put, 
    call, 
    take, 
    all, 
    fork,
    cancel,
    race,
    takeLatest
} from 'redux-saga/effects';
import { delay, eventChannel } from 'redux-saga';

import {
    savePublicUserProfile,
    searchUsers,
    getPublicProfile
} from './usersService';
import {
    USER_SEARCH_RESULTS,
    USER_SEARCH_ERROR,
    REQUEST_USER_SEARCH,
    CREATE_PUBLIC_PROFILE,
    clearSearchResults
} from './index';
import {
    USER_PROFILE_SAVE_SUCCESS
} from '../user-profile';

//A bit higher for slower mobile typing speed.
//This is used to slow the search requests to the server.
const searchDebounce = 300; 

function* onRequestUserSearch({ criteria }) {
    //Might make more sense to insert a cancel command to stop this.
    if(!criteria) {
        return yield put(clearSearchResults());
    }
    yield delay(searchDebounce);
    try {
        const users = yield call(searchUsers, criteria);
        yield put({
            type: USER_SEARCH_RESULTS,
            users
        });;
    }
    catch(err) {
        //
        console.warn(err);
    }
}

function* onNewUserRegistration({ profile }) {
    try {
        const publicProfile = getPublicProfile(profile);
        yield call(savePublicUserProfile, publicProfile);
        //Say successful?  No bindings, so not tripping yet.
    }
    catch(err) {
        //TODO - log error?
        console.warn('onNewUserRegistration - Error : ', err);
    }
}

export function* usersSaga() {
    yield takeEvery(USER_PROFILE_SAVE_SUCCESS, onNewUserRegistration);
    yield takeLatest(REQUEST_USER_SEARCH, onRequestUserSearch);
}