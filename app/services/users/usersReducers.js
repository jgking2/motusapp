import {
    USER_SEARCH_ERROR,
    USER_SEARCH_RESULTS,
    CLEAR_SEARCH_RESULTS
} from './index';

//TODO maybe add loading states?
const initialState = [];

export const usersState = (state = initialState, action) => {
    switch(action.type) {
        case CLEAR_SEARCH_RESULTS:
            return [];
        case USER_SEARCH_RESULTS:
            return [
                ...action.users
            ];
        case USER_SEARCH_ERROR:
            return initialState;
        default:
            return state;
    }
};