// import {firebase} from '../firebaseService';

export const REQUEST_USER_SEARCH = 'REQUEST_USER_SEARCH';
export const CLEAR_SEARCH_RESULTS = 'CLEAR_SEARCH_RESULTS';
export const USER_SEARCH_RESULTS = 'USER_SEARCH_RESULTS';
export const USER_SEARCH_ERROR = 'USER_SEARCH_ERROR';
export const CREATE_PUBLIC_PROFILE = 'CREATE_PUBLIC_PROFILE';


export const clearSearchResults = () => {
    return {
        type: CLEAR_SEARCH_RESULTS
    }
}

export const requestUserSearch = (criteria) => {
    return {
        type: REQUEST_USER_SEARCH,
        criteria
    }
};

export const createPublicProfile = (profile) => {
    return {
        type: CREATE_PUBLIC_PROFILE,
        profile
    }
};