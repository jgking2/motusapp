import firebase from 'react-native-firebase';

const collectionName = 'users';
const resultLimit = 10;

/**
 * 
 * @param {{  }} profile 
 * @returns {Promise}
 */
export const savePublicUserProfile = async (profile) => {
    const db = firebase.firestore().collection(collectionName);
    return await db.add(profile);
};

/**
 * Extremely simple search, we can pretty easily -and will- expand this moving forward.
 * @param {string} criteria 
 * @returns {Promise<[]>}
 */
export const searchUsers = async (criteria) => {
    const db = firebase.firestore()
        .collection(collectionName);
        
    return await db
        .where('username', '>', criteria.toLowerCase())
        .limit(resultLimit)
        .get()
        .then(snapshot => {
            const matches = [];
            snapshot.forEach(res => {
                matches.push(res.data())
            });
            return matches;
        });
};

export const getPublicProfile = (profile) => {
    const { currentUser } = firebase.app().auth();
    return extractPublicProfile(currentUser, profile);
};
/**
 * Destructures the two objects, and generates a public profile.
 * @param {{uid,displayName,email,photoURL}} FirebaseUser user object
 * @param {{username,phone}} MotusProfile motus profile object.
 */
const extractPublicProfile = ({
    uid,
    displayName,
    email,
    photoURL
}, {
    username,
    phone
}) => ({
    uid,
    displayName,
    email,
    photoURL,
    username,
    phone
});