import createSagaMiddleWare from 'redux-saga';
import { all, fork } from 'redux-saga/effects';
import { Alert } from 'react-native';

import { authSaga } from './authentication';
import { userProfileSaga } from './user-profile';
import { contestSaga } from './contests';

import { metricsSaga } from './metrics';
import { contestFeedSaga } from './feed';
import { feedCommentsSaga } from './feed-comments';
import { providerSaga } from './providers';
import { usersSaga } from './users'
import { paymentSaga } from './payment';
import { oauthSaga } from './oauth';
import { fitbitSaga } from './integrations/fitbit';
import { fileUploadSaga } from './file-upload';
import { notificationsSaga } from './notifications';

export function* rootSaga() {
    try {
        //Maybe fork these, error in one takes the whole engine down.
        yield all([
            authSaga(),
            userProfileSaga(),
            contestSaga(),
            feedCommentsSaga(),
            usersSaga(),
            fileUploadSaga(),
            oauthSaga(),
            providerSaga(),
            fitbitSaga(),
            notificationsSaga(),
            contestFeedSaga(),
            paymentSaga(),
            metricsSaga()
        ])
    } catch ( err ) {
        Alert.alert('Send this to Joe', JSON.stringify(err));
    }
}