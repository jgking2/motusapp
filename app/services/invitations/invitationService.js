import firebase from 'react-native-firebase';
import { eventChannel } from 'redux-saga';

const baseRoute = `user-invitations`;

/**
 * 
 * @param {string} contestId 
 * @param {string} uid 
 * @returns {string}
 */
export const generateInvitationToken = (contestId) => {
    const { uid } = firebase.app().auth().currentUser;
    const rawString = `${(+new Date()).toString(16)}.${uid}.${contestId}`;
    const token = btoa(rawString);
    return token;
};

export const getContestInvitationEventChannel = ( ) => {
    const { uid } = firebase.app().auth().currentUser;
    // let path = `${baseRoute}/${uid}`;
    return eventChannel(emit => {
        const invitationRef = _getInvitationsRef();
        const unsubscribe = invitationRef
            .where("to", "=", uid)
            .onSnapshot(snapshot => {
                const invitations = [];
                snapshot.forEach(child => {
                    invitations.push({
                        key : child.id,
                        ...child.data()
                    });
                });
                emit({invitations});
            });
        return () => unsubscribe();            
    });
}

/**
 * 
 * @param {string} contestId 
 * @param {string} message 
 * @param {string} recipient 
 */
export const sendInvitationToContest = async (contestId, message, recipient) => {
    const sender = firebase.app().auth().currentUser;
    const { serverTimestamp } = firebase.firestore.FieldValue;

    const invitation = createInvitation(contestId, sender, message, recipient);
    const invitationRef = _getInvitationsRef(recipient);
    const result = await invitationRef.add({
        ...invitation,
        timestamp : serverTimestamp()
    });
    return result;
};

/**
 * TODO re-evaluate this process.
 * @param {{key, inviter: {}, contestId}} invitation 
 * @param {string} uid
 */
export const acceptUserContestInvitation = async (invitation) => {
    const { uid } = firebase.app().auth().currentUser;
    const { FieldValue } = firebase.firestore;
    const invitationsRef = _getInvitationsRef();
    const result = await invitationsRef.doc(invitation.key)
        .update({ 
            accepted : FieldValue.serverTimestamp()
        });
    return result;
};


const _getInvitationsRef = () => {
    const db = firebase
        .firestore()
        .collection('invitations');
    return db;
};

/**
 * 
 * @param {string} key contest
 * @param {*} param1 user
 * @param {string} messsage Message to the user
 */
const createInvitation = (
    contestId, {
        displayName, 
        email, 
        phone, 
        photoURL, 
        uid 
    }, 
    message,
    recipientId
) => ({
    contestId,
    to: recipientId,
    from : {
        displayName,
        email,
        phone,
        photoURL,
        uid
    },
    message
});