import { 
    GENERATE_INVITATION_TOKEN,
    INVITATION_TOKEN_GENERATED
} from './invitationActions';

let initialState = {};

export const invitationsState = (state = initialState, { type, contestId, token }) => {
    switch(type) {
        case INVITATION_TOKEN_GENERATED:
            return Object.assign({}, state, { [contestId] : token });
        default:
            return state;
    }
}