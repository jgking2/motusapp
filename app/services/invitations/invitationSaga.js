import { 
    take, 
    call, 
    put, 
    fork, 
    all, 
    cancel, 
    takeEvery 
} from 'redux-saga/effects';

import {
    USER_AUTHENTICATED,
    USER_LOGGED_OUT
} from '../authentication';

import {
    GENERATE_INVITATION_TOKEN,
    INVITATION_TOKEN_GENERATED,
    invitationTokenGenerated
} from './invitationActions';
import { 
    generateInvitationToken
} from './invitationService';

function* generateInviteToken({ contestId }) {
    const token = yield call(generateInvitationToken, contestId);
    yield put(invitationTokenGenerated(token, contestId));
}

function* acceptContestInvitation({ invitation }) {
    // yield call(acceptUserContestInvitation(invitation));
}

function* invitationsInit() {
    yield takeEvery(GENERATE_INVITATION_TOKEN, generateInviteToken);
}

export function* invitationSaga() {
    while(true) {
        yield take(USER_AUTHENTICATED);
        const task = yield fork(invitationsInit);
        yield take(USER_LOGGED_OUT);
        yield cancel(task);
    }
}