// export const SEND_USER_CONTEST_INVITATION = 'SEND_USER_INVITATION';
// export const SEND_USERS_CONTEST_INVITATION = 'SEND_USERS_CONTEST_INVITATION';
// export const SENDING_USER_CONTEST_INVITATION = 'SENDING_USER_INVITATION';
// export const USER_CONTEST_INVITATION_SENT_SUCCESSFULLY = 'USER_INVITATION_SENT_SUCCESSFULLY';
// export const USER_CONTEST_INVITATION_FAILED_TO_SEND = 'USER_INVITATION_FAILED_TO_SEND';
// export const ACCEPT_USER_CONTEST_INVITATION = 'ACCEPT_USER_CONTEST_INVITATION';
// export const REJECT_USER_CONTEST_INVITATION = 'REJECT_USER_CONTEST_INVITATION';
// export const INVITATIONS_LOADED = 'CONTEST_INVITATIONS_LOADED';

export const GENERATE_INVITATION_TOKEN = 'GENERATE_INVITATION_TOKEN';
export const INVITATION_TOKEN_GENERATED = 'INVITATION_TOKEN_GENERATED';

//Likely pass more info eventually.
export const generateInvitationToken = (contestId) => ({
    type : GENERATE_INVITATION_TOKEN,
    contestId
});

export const invitationTokenGenerated = (token, contestId) => ({
    type : INVITATION_TOKEN_GENERATED,
    token,
    contestId
});

// export const invitationsLoaded = (invitations) => ({
//     type : INVITATIONS_LOADED,
//     invitations
// });

// export const acceptContestInvitation = ({ contestId }) => {
//     return {
//         type : ACCEPT_USER_CONTEST_INVITATION,
//         contestId
//     }
// };

// export const rejectContestInvitation = (invitation) => {
//     return {
//         type : REJECT_USER_CONTEST_INVITATION,
//         invitation
//     }
// }

// export const sendContestInvitation = (users, contestId, message = '') => ({
//     type : SEND_USER_CONTEST_INVITATION,
//     users,
//     contestId,
//     message
// });