import { combineReducers } from 'redux';
// import { reducer } from 'redux-form'
import { combineForms, createForms } from 'react-redux-form';
import { authState } from './authentication';
import { profileState } from './user-profile';
import { contestState } from './contests';
import { feedState } from './feed';
import { feedCommentsState } from './feed-comments';
import { paymentState } from './payment';
import { usersState } from './users';
import { oauthState } from './oauth';
import { providerState } from './providers';
import { metricsState } from './metrics';

export let appReducers = combineReducers({
    authState,
    profileState,
    contestState,
    paymentState,
    feedState,
    feedCommentsState,
    usersState,
    oauthState,
    providerState,
    metricsState,
    // form: reducer,
    ...createForms({
        contest: { name : '' },
        profileForm : { },
        login : {},
        register : {}
    })
    // myForms: combineForms({
    //     contest : { name : '' }
    // }, 'myForms')
});