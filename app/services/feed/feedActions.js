export const RECEIVED_FEED = 'RECEIVED_FEED';
export const NEW_FEED_ENTRY = 'NEW_FEED_ENTRY';
export const REQUEST_FEED = 'REQUEST_FEED';
export const REMOVE_FEED_SUBSCRIPTION = 'REMOVE_FEED_SUBSCRIPTION';

//Message submissions
export const SUBMIT_MESSAGE = 'SUBMIT_MESSAGE';
export const MESSAGE_SUBMIT_SUCCESS = 'MESSAGE_SUBMIT_SUCCESS';
export const SUBMIT_PICTURE = 'SUBMIT_PICTURE';
export const PICTURE_SUBMIT_SUCCESS = 'PICTURE_SUBMIT_SUCCESS';

//Feed interactions
export const TOGGLE_FEED_LIKE = 'TOGGLE_FEED_LIKE';
export const FEED_LIKE_SUCCESS = 'TOGGLE_FEED_LIKE';

import { feedTypes } from './feedService';

export const toggleFeedLike = (feedItem, contest) => ({
    type : TOGGLE_FEED_LIKE,
    feedItem,
    contest
});

export const feedLikeSuccess = (feedItem, contest) => ({
    type : FEED_LIKE_SUCCESS,
    feedItem,
    contest
});

export const submitMessage = (contest, message) => ({
    type: SUBMIT_MESSAGE,
    contest,
    message
});

export const submitPicture = (contest, feedItem) => {
    return {
        type: SUBMIT_PICTURE,
        contest,
        feedItem : {
            type : feedTypes.IMAGE,
            ...feedItem
        }
    }
};

export const loadFeed = (contestId) => {
    return {
        type: REQUEST_FEED,
        contestId
    }
};

export const removeFeedSubscription = () => {
    return {
        type: REMOVE_FEED_SUBSCRIPTION
    }
}

export const newFeedEntry = (entry, contest) => {
    return {
        type : NEW_FEED_ENTRY,
        entry,
        contest
    }
};

export const receivedFeed = (feed, contestId) => {
    return {
        type : RECEIVED_FEED,
        feed,
        contestId
    }
};