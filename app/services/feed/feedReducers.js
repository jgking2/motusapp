import { 
    FEED_ENTRY_RECEIVED,
    NEW_FEED_ENTRY,
    RECEIVED_FEED
} from './feedActions';

//TODO verify the feed item exists.
const initialState = {
    loading : true,
    feeds : { }
};

export const feedState = (state = initialState, { type, ...action }) => {
    switch(type) {
        case RECEIVED_FEED:
            const update = {
                feeds : {
                    ...state.feeds,
                    [action.contestId] : action.feed
                }
            };
            return Object.assign({}, state, update); 
        default:
            return state;
    }
}