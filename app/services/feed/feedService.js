import firebase from 'react-native-firebase';
import { eventChannel } from 'redux-saga';

import { dataStructure } from '../config';

//Extracts name from structure.
const {
    contests: {
        name : contestCollection,
        feed: {
            name : feedCollection,
            comments : {
                name : commentsCollection
            }
        }
    }
} = dataStructure;

const feedLoadLimit = 30;

/**
 * Running, weightlifting etc. would fall into this category.
 */
const ACTIVITY = 1;
/**
 * Lost weight, bodyfat etc.
 */
const PROGRESS = 2;
/**
 * Tip, inspiring message etc.
 */
const CONTENT = 3;
/**
 * Pretty self explanitory.
 */
const IMAGE = 4;

export const feedTypes = {
    ACTIVITY,
    PROGRESS,
    CONTENT,
    IMAGE
};

/**
 * Posts the feed to all contestants.
 * @param {{ Type, Data }} data 
 * @param {string} contestId
 */
export const postToContestFeed = async (data, contestId) => {
    const { uid } = firebase.app().auth().currentUser;
    const { serverTimestamp } = firebase.firestore.FieldValue;
    const feedData = {
        ...data,
        uid,
        timestamp : serverTimestamp()
    };
    const feed = _getContestFeed(contestId);
    const { id, ...other } = await feed.add(feedData);
    return {
        key : id,
        ...feedData
    }
};

export const loadRecentContestFeed = async (contestId, skip = 0) => {
    const feed = _getContestFeed(contestId);
    const results = await feed
            .limit(feedLoadLimit)
            .get()
            .then(snapshot => {
                const feed = [];
                snapshot.forEach(child => {
                    const { data, id } = child;
                    feed.push({
                        key : id,
                        ...child.data()
                    });
                });
                return feed;
            });
    return results;
};

export const getContestFeedChannel = (contestId) => {    
    return eventChannel(dispatch => {
        const contestFeedRef = _getContestFeed(contestId);

        const unsubscribe = contestFeedRef
            .limit(feedLoadLimit)
            .orderBy("timestamp", "desc")
            .onSnapshot(snapshot => {
                const feed = [];
                snapshot.forEach(child => {
                    const { data, id } = child;
                    feed.push({
                        key : id,
                        ...child.data()
                    });
                });
                dispatch(feed)
            });
        return () => unsubscribe()
    });
};

//Social section - split this if it grows too large.
/**
 * 
 * @param {{ likes:Object }} feedItem 
 * @param {string} contestId
 * @returns {Promise}
 */
export const toggleFeedLike = async (feedItem, contestId) => {
    const { uid } = firebase.app().auth().currentUser;
    const { 
        FieldValue
    } = firebase.firestore;

    const method = (feedItem.likes && feedItem.likes[uid] ? 'delete' : 'serverTimestamp');
    const update = {
        [`likes.${uid}`] : FieldValue[method]()
    };
    const feed = _getContestFeed(contestId);
    await feed.doc(feedItem.key)
        .update(update);
};

/**
 * 
 * @param {string} contestId 
 * @returns {{ add:function, doc:function, get:function }}
 */
const _getContestFeed = (contestId) => {
    const feed = firebase.firestore()
        .collection(contestCollection)
        .doc(contestId)
        .collection(feedCollection);
    return feed;
};