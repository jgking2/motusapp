import { 
    take, 
    takeEvery, 
    put, 
    fork,
    call,
    all,
    cancel
} from 'redux-saga/effects';
import {  } from 'redux-saga';

import { 
    NEW_FEED_ENTRY, 
    RECEIVED_FEED, 
    REQUEST_FEED,
    SUBMIT_PICTURE,
    SUBMIT_MESSAGE,
    MESSAGE_SUBMIT_SUCCESS,
    REMOVE_FEED_SUBSCRIPTION,
    TOGGLE_FEED_LIKE,
    SUBMIT_FEED_COMMENT,
    receivedFeed
} from './feedActions';


import {
    METRIC_SAVE_SUCCESS
} from '../metrics';

import {
    uploadFile, 
    FILE_UPLOAD_COMPLETE
} from '../file-upload';

import { USER_AUTHENTICATED, USER_LOGGED_OUT } from '../authentication';
import {
    toggleFeedLike,
    // submitComment,
    loadRecentContestFeed, 
    postToContestFeed,
    getContestFeedChannel,
    feedTypes
} from './feedService';

function* onContestFeedUpdate(contestId, feed) {
    yield put(receivedFeed(feed, contestId));
}

function* loadContestFeed({ contestId }) {
    const contestFeedChannel = getContestFeedChannel(contestId);
    const task = yield takeEvery(contestFeedChannel, onContestFeedUpdate, contestId);
    yield take(REMOVE_FEED_SUBSCRIPTION);
    yield cancel(task);

    // try {
    //     const feed = yield call(loadRecentContestFeed, contestId);
    //     yield put(receivedFeed(feed, contestId));
    // }
    // catch(err) {
    //     //TODO - notify of error loading messages.
    //     console.warn(err);
    // }
}

function* uploadPicture(message) {
    const { data, uri, type } = message;

    const { tempId } = yield put(uploadFile(uri, 'contest'));
    const { url } = yield take((action) => action.tempId === tempId);
    return { url };
    const pictureMessage = {
        pictureUrl : url,
        type
    };
    yield put
    return { url };
}

function* onPictureSubmission({ contest, feedItem }) {
    const { type } = feedItem;
    const { url : pictureUrl } = yield call(uploadPicture, feedItem);

    const pictureMessage = {
        pictureUrl,
        type
    };

    yield call( uploadMessage, pictureMessage, contest );
}

//TODO handle cache of messages, and allow edits etc.
function* onMessageSubmission({ message, contest }) {
    const type = feedTypes.CONTENT;
    if(typeof message === 'string') {
        message = {
            content : message,
            type
        };
    };
    try {
        yield call(uploadMessage, message, contest);
    }
    catch(err) {
        //TODO - handle message error, idea use tempId to reference back initial caller.
        console.warn(err);
    }
}

//TODO - Assume happy path, then confirm success
function *toggleLike({ feedItem, contest }) {
    try {
        yield call(toggleFeedLike, feedItem, contest.key);
    }
    catch(err) {
        console.warn(err);
    }
}

//TODO likely merge this with submitmessage, a bit confusing.
function *uploadMessage(message, contest) {
    const result = yield call(postToContestFeed, message, contest.key);
    yield put({
        type : MESSAGE_SUBMIT_SUCCESS, 
        message : result,
        contest
    });
};

function* onMetricSave({ contests, metric }) {
    try {    
        yield all([
            contests
                .map(contest => {
                    const message = {
                        type : feedTypes.ACTIVITY,
                        metric                    
                    };
                    return call(uploadMessage, message, contest);
                })
        ])
    }   
    catch(err) {
        console.log(err);
    }
}

function* contestFeedInit() {
    yield takeEvery(SUBMIT_MESSAGE, onMessageSubmission);
    yield takeEvery(SUBMIT_PICTURE, onPictureSubmission);
    yield takeEvery(REQUEST_FEED, loadContestFeed);
    yield takeEvery(TOGGLE_FEED_LIKE, toggleLike);
    yield takeEvery(METRIC_SAVE_SUCCESS, onMetricSave);
}

//TODO reduce code, and make the process a bit more clear.
export function* contestFeedSaga() {
    while(true) {
        yield take(USER_AUTHENTICATED);

        const task = yield fork(contestFeedInit);

        yield take(USER_LOGGED_OUT);
        yield cancel(task);
    }
}

