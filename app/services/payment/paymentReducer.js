import { 
    PAYMENT_ERROR, 
    PAYMENT_SUCCESSFUL, 
    SUBMIT_PAYMENT,
    LOAD_TOKEN,
    TOKEN_LOADED,
    TOKEN_LOAD_ERROR
} from './paymentActions';

//TODO maybe add loading states?
const initialState = {
    token : {
        value: '',
        loading: true,
        error: ''
    },
    payment: {
        loading: false,
        error: ''
    }
};

export const paymentState = (state = initialState, { type, token, error }) => {
    let update = {};
    switch(type) {
        case LOAD_TOKEN:
            update = {
                token : {
                    loading: true
                }
            };
            return Object.assign({}, state, update);
        case TOKEN_LOADED:
            update = {
                token : {
                    loading: false,
                    token
                }
            }
            return Object.assign({}, state, update);
        case TOKEN_LOAD_ERROR:
            update = {
                token : {
                    loading: false,
                    error
                }
            };
            return Object.assign({}, state, update);
        case SUBMIT_PAYMENT:
            update = {
                payment : {
                    loading: true
                }
            }
            return Object.assign({}, state, update);
        case PAYMENT_SUCCESSFUL:
            update = {
                payment : {
                    loading: false
                }
            };
            return Object.assign({}, state, update);
        case PAYMENT_ERROR:
            update = {
                payment : {
                    loading: false,
                    error
                }
            };
            return Object.assign({}, state, update);
        default:
            return state;
    }
};