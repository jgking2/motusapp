import firebase from 'react-native-firebase';
import BTClient from 'react-native-braintree-xplat';
import { AsyncStorage } from 'react-native';
import Config from 'react-native-config';

//TODO set this to change with environment.
const url = Config.API_URL;
const tokenKey = 'bt_token_key';

let setup = false;
/**
 * @returns {string} The braintree token.
 */
export const loadBrainTreeToken = async () => {
    let token = await getToken();
    await BTClient.setup(token);
    setup = true;
};

/**
 * 
 * @param {string|Number} amount 
 * @param {string} contestId 
 */
export const submitBraintreePayment = async (amount, contestId) => {    
    if(typeof amount != 'string') {
        amount = amount.toString()
    }
    const result = await BTClient
        .showPaymentViewController({
            amount
        })
        .then(async payment_method_nonce => {
            const headers = await getAuthHeader();
            fetch(url + 'btPayment', {
                method: "POST",
                headers,
                body: JSON.stringify({
                    payment_method_nonce,
                    contestId
                })
            }).then(res => console.info(res));
        });
    return result;
};

/**
 * @returns {Promise<string>}
 */
const initializeBraintree = async (token) => {
    BTClient.setup(token);
};

/**
 * @returns {Promise<string>} a promise that resolves the token.
 */
const setNewToken = async () => {
    const headers = await getAuthHeader();
    const token = await fetch({
        url: url + 'btGenerateToken',
        headers
    }).then(res => res.text());
    await AsyncStorage.setItem(tokenKey, token);
    return token;
};

/**
 * @returns {Promise<{ Authorization:string }>} 
 */
const getAuthHeader = async () => {
    const { currentUser } = firebase.app().auth();
    const token = await currentUser.getIdToken();
    return {
        Authorization : `Bearer ${token}`
    }
};

/**
 * Returns the cached token, or fetches a new one provided it doesn't exist.
 * @returns { Promise<string> }
 */
const getToken = async () => {
    let token = await AsyncStorage.getItem(tokenKey);
    if(!token) {
        token = await setNewToken();
    }
    return token;
};