export const SUBMIT_PAYMENT = 'SUBMIT_PAYMENT';
export const PAYMENT_SUCCESSFUL = 'PAYMENT_SUCCESSFUL';
export const PAYMENT_ERROR = 'PAYMENT_ERROR';

export const LOAD_TOKEN = 'LOADING_TOKEN';
export const TOKEN_LOADED = 'TOKEN_LOADED';
export const TOKEN_LOAD_ERROR = 'TOKEN_LOAD_ERROR';

export const loadToken = () => ({
    type : LOAD_TOKEN
});

export const tokenLoaded = (token) => ({
    type : TOKEN_LOADED,
    token
});

export const submitPayment = (amount, contestId) => ({
    type : SUBMIT_PAYMENT,
    contestId,
    amount
});

export const paymentSuccessful = () => ({
    type : PAYMENT_SUCCESSFUL
});