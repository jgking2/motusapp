import { takeEvery, put, call } from 'redux-saga/effects';
import { 
    SUBMIT_PAYMENT, 
    LOAD_TOKEN, 
    tokenLoaded,
    paymentSuccessful
} from './paymentActions';
import { loadBrainTreeToken, submitBraintreePayment } from './paymentService';

function* loadToken() {
    try {
        const token = yield call(loadBrainTreeToken);
        console.log(token);
        yield put(tokenLoaded(token));
    }
    catch(err) {
        console.warn('yeah fix this, add a put', err);
    }
}

function* submitPayment({ amount, contestId }) {
    try {
        const result = yield call(submitBraintreePayment, amount, contestId);
        yield put(paymentSuccessful());
    }
    catch(err) {
        console.warn(err);
    }
}

export function* paymentSaga() {
    yield call(loadToken);
    yield takeEvery(SUBMIT_PAYMENT, submitPayment);
    yield takeEvery(LOAD_TOKEN, loadToken);
}