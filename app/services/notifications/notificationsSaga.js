import { 
    put, 
    take, 
    takeEvery, 
    fork, 
    cancel, 
    call,
    all
} from 'redux-saga/effects';
import { eventChannel, delay } from 'redux-saga';
import { AsyncStorage } from 'react-native';


import { firebase } from '../firebaseService';
import { MESSAGE_SUBMIT_SUCCESS, feedTypes } from '../feed';
import {
    checkLocalData,
    storeLocalData,
    extractDeviceIds,
    submitNotification
} from './notifications';

import { registerForNotifications } from './notificationsService';
import {
    NOTIFICATIONS_INITIALIZED
} from './notificationsActions';
import { CONTEST_UPDATE } from '../contests';
// import {  } from '../feed';
import { SUBMIT_FEED_COMMENT_SUCCESS } from '../feed-comments';
import { USER_LOGGED_OUT, USER_AUTHENTICATED } from '../authentication';
import { 
    USER_PROFILE_LOADED, 
    USER_JOINED_CONTEST 
} from '../user-profile';

const messageTemplates = {
    [feedTypes.ACTIVITY] : 'Recorded a new activity!',
    [feedTypes.IMAGE] : 'Sent an photo!'
};

function *verifyAllContests(deviceId, { contests }) {
    const tasks = Object.keys(contests).map(key => {
        return verifyContestNotificationRegistration(deviceId, contests[key]);
    });
    yield all(tasks);
}

function *verifyContestNotificationRegistration(
    deviceId, contest
) {
    const { key } = contest;
    const localDataCheck = checkLocalData(deviceId).then(localData => {
        const exists = !!(localData && localData.indexOf(key) > -1);
        return {
            exists,
            localData
        };
    });
    const { exists, localData } = yield call(() => localDataCheck);
    if(!exists) {
        const toSave = [...(localData || []), key];
        yield call(registerForNotifications, deviceId, contest, key);
        yield call(storeLocalData, deviceId, toSave);
    }
};

function* onMessageSubmission({ message, contest }) {
    const {  uid, displayName } = firebase.auth().currentUser;
    console.info(message, contest);
    const recipients = extractDeviceIds(contest, uid);
    const prefix = `${displayName} @ ${contest.name} : `;
    const body = messageTemplates[message.type] || message.content;
    const content = `${prefix}${body}`;
    const url = `motus://contests/${contest.key}`;
    yield call(submitNotification, content, recipients, url);
}

function* onFeedComment({ feedItem, contest, comment }) {
    const {  uid, displayName } = firebase.auth().currentUser;
    console.log(feedItem, contest, comment);
    const recipients = extractDeviceIds(contest, uid, feedItem.uid);
    const content = `${displayName} commented: ${comment}`;
    console.log(content);
    yield call(submitNotification, content, recipients);
};

function* onFeedCheer({ feedItem, contest }) {
    console.log(feedItem, contest);
}

function* notificationWatchers(playerId, contests) {    
    //On every contest update, let's ensure we are setup to receive notifications.
    yield takeEvery(
        CONTEST_UPDATE, 
        verifyAllContests, 
        playerId
    );
    //We can send notifications to users.
    yield takeEvery(
        MESSAGE_SUBMIT_SUCCESS, 
        onMessageSubmission
    );

    yield takeEvery(
        SUBMIT_FEED_COMMENT_SUCCESS,
        onFeedComment
    );
}

export function* notificationsSaga() {
    console.log('hit');
    while (true) {
        // const { playerId } = yield take(NOTIFICATIONS_INITIALIZED);
        //Take user logged in, and notification id received.
        const {
            notification : { 
                playerId 
            },
            contest : {
                contests
            }
        } = yield all({
            notification : take(NOTIFICATIONS_INITIALIZED),
            contest : take(CONTEST_UPDATE)
        });

        yield call(verifyAllContests, playerId, { contests });
        const task = yield fork(notificationWatchers, playerId, contests);

        yield take(USER_LOGGED_OUT);
        //TODO - clean up remove notifications for this user.
        yield cancel(task);
    }
}