import { AsyncStorage } from 'react-native';
import OneSignal from 'react-native-onesignal';

// OneSignal.
/**
 * 
 * @param {string} playerId 
 * @returns {Promise<[]>}
 */
export const checkLocalData = (playerId) => {
    return AsyncStorage.getItem(playerId)
        .then(res => {
            const cache = JSON.parse(res);
            return cache;
        });
};

export const storeLocalData = (playerId, contestList) => {
    const dataToCache = JSON.stringify(contestList);
    return AsyncStorage.setItem(playerId, dataToCache);
};


//TODO Add url to open message appropriately.
/**
 * 
 * @param {{ content : string }} message 
 * @param {[string]} playerIds 
 */
export function submitNotification(
    content, 
    playerIds, 
    url = 'motus://', 
    data = {}) {
    const contents = {
        en : content
    };
    const otherParameters = {
        url,
        'include_player_ids' : playerIds,
        'ios_badgeType' : 'Increase', //Potentially pass as an argument
        'ios_badgeCount' : 1
    };

    OneSignal.postNotification(contents, data, '', otherParameters);
};

//Grabs the notification Ids (player Ids) from the members within the contest.
export const extractDeviceIds = ({ members }, uid, selectedMember = '') => {
    let memberPool = !!selectedMember?
        Object.assign({ }, {[selectedMember] : members[selectedMember] }) : //If member is selected
        members;
         
    const devices = Object.keys(memberPool)
        .filter(userId => userId != uid)
        .reduce((prev, curr) => {
            const { notificationId } = members[curr];
            return [
                ...prev, 
                ...( notificationId && [notificationId] || [])];
        }, []);
    return devices;
};