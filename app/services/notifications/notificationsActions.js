export const INITIALIZE_NOTIFICATIONS = 'INITIALIZE_NOTIFICATIONS';
export const NOTIFICATIONS_INITIALIZED = 'NOTIFICATIONS_INITIALIZED';

export const notificationsInitialized = (playerId) => {
    return {
        type : NOTIFICATIONS_INITIALIZED,
        playerId
    }
};