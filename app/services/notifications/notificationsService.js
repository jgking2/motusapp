import firebase from 'react-native-firebase';

import { dataStructure } from '../config';

const {
    contests : {
        name : contestCollection,
        notifications : {
            name : notificationsCollection
        }
    }
} = dataStructure;

export const registerForNotifications = async (
    deviceId, contest, key) => {
    const { uid } = firebase.app().auth().currentUser;
    const db = _getNotificationsCollection(key);

    await db.update({
        [`members.${uid}.notificationId`] : deviceId 
    });
};

const _getNotificationsCollection = (contestId) => {
    

    const notifications = firebase.firestore()
        .collection(contestCollection)
        .doc(`${contestId}`);

    return notifications;
};
