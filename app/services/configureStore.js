import { createStore, applyMiddleware, compose } from 'redux';
import createSagaMiddleWare from 'redux-saga';
import { rootSaga } from './sagas';
import { appReducers } from './reducers';
import { firebase } from './firebaseService';

const motusSagas = createSagaMiddleWare();

export let configureStore = () => {
    let store = createStore(
        appReducers,
        applyMiddleware(motusSagas)
    );
    motusSagas.run(rootSaga);
    return store;
}