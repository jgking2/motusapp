/**
 * Generates a temporary Id, to help track track newly generated objects.
 */
export const generateId = () => +new Date();