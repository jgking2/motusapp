
//Contest List Screen
export const titleContestList = {
    en : 'Contests',
    es : 'Concurso'
};

export const missingContests = {
    en : 'No contests',
    es : 'Sin concursos'
};

export const actionInviteCode = {
    en : 'Insert invite code',
    es : 'Insertar código de invitación'
};

//Join Contest Screen
export const titleJoinContest = {
    en: 'Join Contest',
    es : ''
};

export const placeholderInviteCode = {
    en : 'Invitation Code',
    es : 'Código de invitación'
};

export const actionSubmitInviteCode = {
    en : 'Submit Code',
    es : 'Ingrese el código'
};

//Various
export const actionLogout = {
    en : 'Logout',
    es : 'Cerrar sesión'
};