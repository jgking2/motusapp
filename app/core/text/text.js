import { retrieveDeviceLocal } from '../retrieveDeviceLocale';
import { 
    titleContestList, 
    missingContests, 
    actionInviteCode,
    actionLogout,
    titleJoinContest,
    placeholderInviteCode,
    actionSubmitInviteCode
} from './localizedText';
import { locale } from 'moment';

const language = retrieveDeviceLocal();

const [ languageRoot, ] = language.split('_');
const localize = (map) => {
    return map[languageRoot] || map.en;
};


export const text = {
    titleContest : localize(titleContestList),
    missingContests : localize(missingContests),
    actionLogout : localize(actionLogout),
    actionInviteCode :localize(actionInviteCode),
    actionSubmitInviteCode : localize(actionSubmitInviteCode),
    placeholderInviteCode : localize(placeholderInviteCode),
    titleJoinContest : localize(titleJoinContest)
};