//iPhone X tools
import { Dimensions } from 'react-native';

const { height, width } = Dimensions.get("window");


const X_WIDTH = 375;
const X_HEIGHT = 812;

const isPhoneX = height === X_HEIGHT && width === X_WIDTH;

export const paddingBottom = isPhoneX? 34 : 0;

export const randomInt = (maxValue = 1) => {
    return Math.floor(
        Math.random() * Math.floor(maxValue)
    );
};