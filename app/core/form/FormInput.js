import { 
    TextInput,
    View
} from 'react-native';
import React, { Component } from 'react';
import PropTypes from 'prop-types';
//Needs - if invalid, 

export class FormInput extends Component  {
    state = {
        hasErrors : false,
        isDirty : false
    };
    render() {
        const {
            validation, 
            errorText = '',
            errorStyle = {},
            style,
            ...addtl 
        } = this.props;
        const { hasErrors } = this.state;
        const styles = [style, hasErrors && errorStyle || {}];

        return (
            <TextInput
                ref="formInput"
                style={styles}
                onChangeText={this.onChangeText.bind(this)}
                {...addtl} />
        )
    }

    reset() {
        this.refs['formInput'].clear();
    }

    async onChangeText(text) {
        const {
            validation,
            onChangeText
        } = this.props;

        const hasErrors = validation && !(await validation(text));
        console.log(hasErrors);
        this.setState({
            isDirty : true,
            hasErrors
        });

        onChangeText && onChangeText(text);
    }
}

FormInput.propTypes = TextInput.propTypes;