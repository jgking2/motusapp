import { 
    TextInput, 
    View, 
    Text 
} from 'react-native';
import React, { Component } from 'react';
import PropTypes from 'prop-types';
//Haserror or not
//Hassuccess or not
//Validation
//Component to render
//Is dirty
//form - to report to
//Success style
//Error style
//Error text style
export class InputContainer extends Component {
    state = {
        isValid : false,
        hasError : false,
        isDirty : true
    };

    componentWillMount() {
        const { form, name } = this.props;
        console.log(form);
        form.register(name);
    }

    render() {
        const { 
            isValid, 
            hasError, 
            isDirty 
        } = this.state;

        const {
            changeMethod,
            containerErrorStyle, 
            containerSuccessStyle,
            validation,
            Component,
            ...addtl
        } = this.props;

        const componentProps = Object
            .assign({}, 
                addtl,
                { 
                    [changeMethod] : this.onChange.bind(this) 
                }
        );

        return (
            <View>
                <Component 
                    {...componentProps} />
            </View>
        )
    }

    async onChange(data) {
        const { validation } = this.props;
        const hasErrors = validation && !(await validation(data));
        this.setState({
            hasErrors,
            isDirty : true
        });
    }
}
