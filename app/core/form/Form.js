import { 
    TextInput, 
    View, 
    Text 
} from 'react-native';
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { updateContest } from '../../services/contests/contestService';

export class Form extends PureComponent {
    static defaultProps = {
        onValidityChange : () => ({})
    };

    state = {
        fields: {

        },
        errors : []
    };

    get valid() {
        const { errors } = this.state;
        return Object.keys(errors)
            .reduce((prev, curr) => {
                return errors[curr] && prev;
            }, true);
    }

    render() {
        const { children } = this.props;
        return (
            <View>
                { children }
            </View>
        )
    }

    register(
        field, 
        valid, message) {
        const { items } = this.state;
        this.setValidity(
            field,
            valid
        );
    }

    setValidity(field, valid, error) {
        const { onValidityChange } = this.props;
        const { errors } = this.state;

        const currentValue = errors[field];
        if(errors.indexOf(field) > -1)
        if(currentValue.valid !== valid) {
            let update = Object.assign({},
                errors, 
                { [field] : valid }
            );
            this.setState({
               errors : update
            });
        }
    }
}