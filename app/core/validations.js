/**
 * In ms per km
 */
const fastestMileRunning = 138647;
//Kilometers.
const maxDistanceRunning = 200;
//Duration
const maxDuration = 86400000;

export const required = (value) => !!value;

/**
 * 
 * @param {string} value 
 * @param {number} min 
 * @param {number} max 
 */
export const validTextLength = (value, min = 0, max) => {
    let validMax = true;
    const validMin = value.length > min;
    if(!!max) {
        validMax = value.length < max;
    }
    return validMax && validMin;
};

//TODO - validate based on locale.
export const phoneNumber = (value, localization = '') => {
    
};

/**
 * Determines if the pace of the run is a bit too unrealistic, validated again on the server.
 * @param {number} distance Distance in km
 * @param {number} duration Duration in ms
 */
export const validRun = (distance = 0, duration = Infinity) => {
    const velocity = distance / duration;
    const validVelocity = velocity < fastestMileRunning;
    const validDistance = distance < maxDistanceRunning;
    const validDuration = duration < maxDuration;
    const isValid = validVelocity && validDistance && validDuration;
    return isValid;
};

export const validCycle = (distance = 0, duration = Infinity) => {
    
};