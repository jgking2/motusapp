import { 
    NativeModules,
    Platform
} from 'react-native';

export const retrieveDeviceLocal = () => {
    const language = Platform
        .select({
            'ios' : languageIos(),
            'android' : languageAndroid()
        });
    return language;
};

const languageIos = () => {
    const language = NativeModules.SettingsManager.settings.AppleLocale;
    return language;
};

const languageAndroid = () => {
    const language = NativeModules.I18nManager.localIdentifier;
    return language;
};