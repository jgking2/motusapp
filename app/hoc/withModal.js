import React, {Component} from 'react';
import Icon from 'react-native-vector-icons/Ionicons';
import {Modal, View, Text, TouchableWithoutFeedback} from 'react-native';

const DefaultButton = (props) => {
    return (
        <Icon name="ios-add" size={20}></Icon>
    );
};
/**
 * TODO - pass data back on close, and discern between a close and dismiss.
 */
/**
 * Wraps Component with Modal functionality. 
 * Also passes in the close function to hide the modal.
 */
// {/* {this.props.children || } */}
export const withModal = () => (ModalComponent) => {
    return class WithModalHoc extends Component {
        state = {
            modalVisible : false
        };

        render() {
            const {modalVisible} = this.state;
            let {...addtlProps} = this.props;
            addtlProps['close'] = (data) => {
                this.hideModal();
            };

            const childElement = this.props.children && React.cloneElement(this.props.children, {
                onPress : () => this.showModal()
            });

            return (
                <View>
                    {
                        childElement || <TouchableWithoutFeedback onPress={this.showModal.bind(this)}>
                            <View>
                                <DefaultButton/>
                            </View>
                        </TouchableWithoutFeedback>
                    }
                    
                    <Modal 
                        visible={modalVisible}
                        animationType={"slide"}>
                        <ModalComponent {...addtlProps} />
                    </Modal>
                </View>
            )
        }

        showModal() {
            this.setModalVisibility(true);
        }

        hideModal() {
            this.setModalVisibility(false);
        }

        setModalVisibility(visibility) {
            this.setState({
                modalVisible: visibility
            });
        }
    }
}

        /*<ModalComponent {...addtlProps}></ModalComponent>*/