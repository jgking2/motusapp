//Ensures user is authenticated.
import React, {Component} from 'react';
import {
    TouchableWithoutFeedback,
    Keyboard
} from 'react-native';

const dismissKeyboard = () => Keyboard.dismiss

export const avoidKeyboard = () => (DismissableKeyboardComponent) => {
    return ( ) => {
        return (
            <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
                <DismissableKeyboardComponent />
            </TouchableWithoutFeedback>
        )
    }
};