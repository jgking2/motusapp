//Ensures user is authenticated.
import React, {Component} from 'react';
import {Modal, View, Text, TouchableWithoutFeedback} from 'react-native';
import {connect} from 'react-redux';

const mapStoreToState = ({ 
    authState : { 
        authenticated, 
        initializing 
    } }) => ({ authenticated, initializing });

//TODO - update loading screen.
const LoadingScreen = (props) => {
    return (
        <View style={{
            flex: 1,
            flexDirection:"column",
            justifyContent:"center",
            alignContent:"center"
            }}>
            <Text style={{textAlign:"center"}}>
                Motus
            </Text>
        </View>
    )
};

export const withUserReady = (config) => (ProtectedComponent) => {    
    return connect(mapStoreToState) (
        class WithAuthenticationHOC extends Component {

            render() {
                
                const UnauthorizedScreen = config.unauthorizedScreen;
                const { initializing, authenticated } = this.props;

                if(initializing) {
                    return <LoadingScreen />
                }
                return authenticated? <ProtectedComponent /> : <UnauthorizedScreen />
            }
        }
    )
}