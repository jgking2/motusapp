import React, {Component} from 'react';
import {
  AppRegistry,
  View,
  Text,
  Button
} from 'react-native';
import { StackNavigator } from 'react-navigation';
import {App} from './app/app';

// import data from './testNative';

AppRegistry.registerComponent('MotusApp', () => App);
// /**
//  * Sample React Native App
//  * https://github.com/facebook/react-native
//  * @flow
//  */

// import React, { Component } from 'react';
// import {
//   AppRegistry,
//   StyleSheet,
//   Text,
//   View
// } from 'react-native';

// // import { App } from './app/App';

// export default class MotusApp extends Component {
//   render() {
//     return (
//       <Text>Yo</Text>
//     );
//   }
// }

// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//     justifyContent: 'center',
//     alignItems: 'center',
//     backgroundColor: '#F5FCFF',
//   },
//   welcome: {
//     fontSize: 20,
//     textAlign: 'center',
//     margin: 10,
//   },
//   instructions: {
//     textAlign: 'center',
//     color: '#333333',
//     marginBottom: 5,
//   },
// });

// AppRegistry.registerComponent('MotusApp', () => MotusApp);
