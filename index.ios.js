import React, {Component} from 'react';
import {
  AppRegistry,
  View,
  Text,
  Button
} from 'react-native';
import { StackNavigator } from 'react-navigation';


import { App } from './app/app';

AppRegistry.registerComponent(
  'MotusApp', () => App
);

// import { TestComponent } from './TestingComponent';
// AppRegistry.registerComponent(
//   'MotusApp', () => TestComponent
// );