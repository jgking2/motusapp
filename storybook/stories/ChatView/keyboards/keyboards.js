import {KeyboardRegistry} from 'react-native-keyboard-input';

import React, {Component} from 'react';
import {View, Text, TextInput} from 'react-native';

class KeyboardTestView extends Component {
    render() {
        return (
            <View style={{flex:1 , backgroundColor: 'green', height: 700}}>
                <Text>Keyboard baby</Text>

                <TextInput style={{height: 200, flex: 1}} placeholder="whatyadoin"></TextInput>
            </View>
        )
    }
}

KeyboardRegistry.registerKeyboard('testKb', () => KeyboardTestView);
