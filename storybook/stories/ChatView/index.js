import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  TextInput,
  View,
  ScrollView,
  TouchableOpacity,
  PixelRatio,
  Platform,
  Keyboard,
  FlatList,
  LayoutAnimation
} from 'react-native';
import {AutoGrowingTextInput} from 'react-native-autogrow-textinput';
// import {BlurView} from 'react-native-blur';
import {KeyboardAccessoryView, KeyboardUtils} from 'react-native-keyboard-input';
import {styles} from './style';
import {Messages} from './component/messages';
import {LaunchCamera} from './component/camera';
import {LaunchCameraRoll} from './component/cameraRoll';
import Icon from 'react-native-vector-icons/Ionicons';
import * as kb from './keyboards/keyboards';
import {Header,Title} from 'native-base';
import {ChatToolbar} from './component/chatToolbar';

// import './demoKeyboards';

const IsIOS = Platform.OS === 'ios';
const TrackInteractive = true;

export class ChatView extends Component {

  constructor(props) {
    super(props);
    this.keyboardAccessoryViewContent = this.keyboardAccessoryViewContent.bind(this);
    this.onKeyboardItemSelected = this.onKeyboardItemSelected.bind(this);
    this.resetKeyboardView = this.resetKeyboardView.bind(this);
    this.onKeyboardResigned = this.onKeyboardResigned.bind(this);

    this.state = {
      customKeyboard: {
        component: undefined,
        initialProps: undefined,
      },
      receivedKeyboardData: undefined,
    };
  }

  onKeyboardItemSelected(keyboardId, params) {
    const receivedKeyboardData = `onItemSelected from "${keyboardId}"\nreceived params: ${JSON.stringify(params)}`;
    this.setState({receivedKeyboardData});
  }

  getToolbarButtons() {
    return [
      {
        text: 'show1',
        testID: 'show1',
        onPress: () => {
          console.info('pressed kb1');
           this.showKeyboardView('testKb', 'FIRST - 1 (passed prop)')
        },
      },
    //   {
    //     text: 'show2',
    //     testID: 'show2',
    //     onPress: () => this.showKeyboardView('AnotherKeyboardView', 'SECOND - 2 (passed prop)'),
    //   },
      {
        text: 'reset',
        testID: 'reset',
        onPress: () => this.resetKeyboardView(),
      },
    ];
  }

  resetKeyboardView() {
    this.setState({
      customKeyboard: {}
    });
  }

  onKeyboardResigned() {
    this.resetKeyboardView();
  }

  showKeyboardView(component, title) {
    this.setState({
      customKeyboard: {
        component,
        initialProps: {title},
      },
    });
  }

  setContent(text) {
    // LayoutAnimation.spring(() => console.info('rock and roll!'));
    LayoutAnimation.configureNext(
        LayoutAnimation.create(
          200, 
          LayoutAnimation.Types.easeInEaseOut, 
          LayoutAnimation.Properties.scaleXY
        ));
    this.setState({ 
      content: text
    });
  }

  keyboardAccessoryViewContent() {
    const InnerContainerComponent = View;
    let {
      content, 
      showCustomKb
    } = this.state;

    return (
      <InnerContainerComponent blurType="xlight" style={styles.blurContainer}>
        <View style={
          {
            borderTopWidth: StyleSheet.hairlineWidth, borderColor: '#bbb', 
            backgroundColor: 'rgba(256,256,256,.8)'
          }}/>
        
        <View style={styles.inputContainer}>
           { !content && (
            <View style={{flexDirection: 'row', justifyContent: 'space-around', flex: .5}}>
                <LaunchCameraRoll></LaunchCameraRoll>
                <LaunchCamera></LaunchCamera>
              </View>
            )
          }
          <View style={styles.inputWrapper}>
            <AutoGrowingTextInput
              maxHeight={200}
              minHeight={30}
              style={styles.textInput}
              ref={(r) => {
                this.textInputRef = r;
              }}
              onChangedText={(text) => console.info(text)}
              onChange={(e)=> this.setContent(e.nativeEvent.text)}
              onPress={() => console.info('press')}
              onFocus={() => console.info('entering')}
              onBlur={() => console.info('leaving')}
              placeholder={'Message'}
              underlineColorAndroid="transparent"
              onFocus={() => this.resetKeyboardView()}
              testID={'input'}
            />
            {/*{!!content &&*/}
              <TouchableOpacity style={styles.sendButton}>
                <Icon name="ios-send" color={'#fff'} size={15}></Icon>
              </TouchableOpacity>
            {/*}*/}
          </View>
        </View>
        <View style={{flexDirection: 'row', flex: 1}}>
          {
            this.getToolbarButtons().map((button, index) =>
              <TouchableOpacity 
                
                onPress={button.onPress}
                style={{paddingLeft: 15, paddingBottom: 10}} key={index} testID={button.testID}>
                <Text>{button.text}</Text>
              </TouchableOpacity>)
          }
        </View>
        {
          showCustomKb &&
          <View style={{flexDirection:'column', flex: 10, borderWidth: 1, borderColor: 'black'}}>
            <Text>Box</Text>
          </View>
        }
      </InnerContainerComponent>
    );
  }

  render() {
    console.info(this.state.customKeyboard.component);
    return (
      <View style={styles.container}>
        <Header>
          <Title>
            Yo
          </Title>
        </Header>
        <View style={{flex:1,flexDirection:'column'}}>
          <Messages style={{flex:1}} messages={new Array(20).fill('Etiam felis sem, gravida sit amet lacinia a, ornare et odio. Donec ac nisl ipsum. Vivamus et consequat massa. Nulla vel rutrum urna. Nulla vel enim et neque volutpat bibendum vitae a tellus. Maecenas id purus lectus. Aliquam dolor sapien, egestas sit amet sem ut, fermentum malesuada orci. In pulvinar congue ex cursus porta. In vulputate vulputate metus, in congue arcu consequat consectetur.')}>
          </Messages>
          <ChatToolbar />
        </View>
      </View>
    );
  }
}
{/*revealKeyboardInteractive*/}