import { StyleSheet, Platform, PixelRatio, Dimensions } from 'react-native';

const screenSize = Dimensions.get('window');

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F5FCFF',
  },
  scrollContainer: {
    justifyContent: 'center',
    padding: 15,
    // paddingBottom: 95,
    marginBottom: 70,
    flex: 1,
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
    paddingTop: 50,
    paddingBottom: 50,
  },
  inputContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  blurContainer: {
    ...Platform.select({
      ios: {
        flex: 1,
      },
    }),
  },
  textInput: {
    flex: 1,
    paddingLeft: 5,
    height:20
    // alignSelf:'center',
    // textAlignVertical: 'center'
    // lineHeight: 30
    // lineHeight: 30
    // marginLeft: 10,
    // marginTop: 10,
    // marginBottom: 10,
    // paddingLeft: 10,
    // paddingTop: 2,
    // paddingBottom: 5,
    // fontSize: 16,
    // backgroundColor: 'white',
    // borderWidth: 0.5 / PixelRatio.get(),
    // borderRadius: 18,
  },
  inputWrapper: {
    // padding: 5,
    flex: 1, 
    flexDirection: "row", 
    alignItems: "center",
    borderColor: '#ddd', 
    borderWidth: 0.5,
    margin: 5,
    borderRadius: 18
  },
  sendButton: {
    flex: 0,
    backgroundColor: 'blue',
    padding: 5,
    borderRadius: 15,
    paddingLeft: 8,
    paddingRight: 8
    // paddingRight: 15,
    // paddingLeft: 15,
  },
  container: {
		flex: 1,
		backgroundColor: '#F5FCFF'
	},
	scrollContainer: {
		justifyContent: 'center',
		paddingBottom: 25
	},
	welcome: {
		fontSize: 20,
		textAlign: 'center',
		margin: 10,
		paddingTop: 50,
		paddingBottom: 50
	},
	image: {
		height: 250,
		width: undefined,
		marginBottom: 10
	},
	trackingToolbarContainer: {
		// position: 'absolute',
		// bottom: 0,
		// left: 0,
		width: screenSize.width,
		borderWidth: 0.5 / PixelRatio.get()
	},
	blurContainer: {
		flex: 1,
		flexDirection: 'row',
		alignItems: 'center',
		justifyContent: 'space-between'
	},
	textInput: {
		flex: 1,
		height: 36,
		marginLeft: 10,
		marginTop: 10,
		marginBottom: 10,
		paddingLeft: 10,
		fontSize: 17,
		backgroundColor: 'white',
		borderWidth: 0.5 / PixelRatio.get(),
		borderRadius: 18
	},
	sendButton: {
		paddingRight: 15,
		paddingLeft: 15
	}
});
