import {
    CameraRoll,
    TouchableHighlight,
    Text
} from 'react-native';
import ImagePicker from 'react-native-image-picker';
import React from 'react';
import Icon from 'react-native-vector-icons/Ionicons';

export const LaunchCameraRoll = (props) => {
    console.info(props);
    return (
        <TouchableHighlight onPress={() => {
            ImagePicker.launchImageLibrary({}, (res) => {
                console.info(res);
            })
            }}>
            <Icon name="ios-image" size={30} color={'rgba(0,128,128,.7)'} />
        </TouchableHighlight>
    );
};