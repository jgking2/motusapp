import {AutoGrowingTextInput} from 'react-native-autogrow-textinput';
import React, {Component} from 'react';
import {View, TouchableOpacity} from 'react-native';
import {styles} from '../style';
import Icon from 'react-native-vector-icons/Ionicons';

export class ChatInput extends Component {
    render() {
        return (
            <View style={styles.inputWrapper}>
                <AutoGrowingTextInput
                maxHeight={200}
                minHeight={30}
                style={styles.textInput}
                ref={(r) => {
                    this.textInputRef = r;
                }}
                onChangedText={(text) => console.info(text)}
                onChange={(e)=> this.setContent(e.nativeEvent.text)}
                onPress={() => console.info('press')}
                onFocus={() => console.info('entering')}
                onBlur={() => console.info('leaving')}
                placeholder={'Message'}
                underlineColorAndroid="transparent"
                testID={'input'}
                />
                {/*{!!content &&*/}
                <TouchableOpacity style={styles.sendButton}>
                    <Icon name="ios-send" color={'#fff'} size={15}></Icon>
                </TouchableOpacity>
                {/*}*/}
          </View>
        )
    }
}