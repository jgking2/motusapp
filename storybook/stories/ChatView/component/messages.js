import React, {Component} from 'react';
import {
    FlatList,
    Text,
    Alert,
    KeyboardAvoidingView,
    View,
    Button,
    ScrollView,
    Keyboard
} from 'react-native';
import {styles} from '../style';
/**
 * Component to test messages
 */
export class Messages extends Component {
    componentWillMount() {
        // Keyboard.addListener()
        Keyboard.addListener('keyboardWillShow', () => this.scrollMofo());
        Keyboard.addListener('keyboardWillHide', () => setTimeout(() => this.scrollMofo()));
    }

    componentDidMount() {
        // this.refs.scrollView.scrollToBottom();    
    }

    scrollMofo() {
        const { messages } = this.props;
        this.scrollView.scrollToEnd();
    }

    // {/*keyboardShouldPersistTaps={"never"}*/}

    render() {
        const { messages, style }  = this.props;
        return (
            <KeyboardAvoidingView behavior="padding" style={[styles.scrollContainer, style]} >
                <ScrollView 
                    onContentSizeChange={() => this.scrollMofo()}
                    ref={ref => this.scrollView = ref}
                >
                    {messages.map((msg, index) => <Text key={index}>{msg}</Text>)}
                </ScrollView>
            </KeyboardAvoidingView>
        )
    }
}

                /*<FlatList  
                    onTouchStart={()=> this.scrollMofo()}
                    keyExtractor={(item, index) => index}
                    ref={ref => this.scrollView = ref}
                    contentContainerStyle={styles.scrollContainer}
                    
                    data={messages}
                    renderItem={({item}) => <Text>{item}</Text>}/> */