/* eslint-disable import/no-extraneous-dependencies, import/no-unresolved, import/extensions */

import React from 'react';
import { Text } from 'react-native';

import { storiesOf } from '@storybook/react-native';
import { action } from '@storybook/addon-actions';
import { linkTo } from '@storybook/addon-links';

import Button from './Button';
import CenterView from './CenterView';
import Welcome from './Welcome';
import { MotusInput } from './Input';
// import {ChatView} from './ChatView';
// import {ContestItem} from './Contests';

storiesOf('Welcome', module).add('to Storybook', () => <Welcome showApp={linkTo('Button')} />);

storiesOf('Motus Input', module)
  .add('Input Field', () => <MotusInput onChangeText={(text) => console.log(text)} label={"Username"} />);

// storiesOf('ChatView', module)
//   .add('container', () => <ChatView></ChatView>);

// storiesOf('Contests', module)
//   .add('contestItem', () => <ContestItem></ContestItem>);

storiesOf('Button', module)
  .addDecorator(getStory => <CenterView>{getStory()}</CenterView>)
  .add('with text', () =>
    <Button onPress={action('clicked-text')}>
      <Text>Hello Button</Text>
    </Button>
  )
  .add('with some emoji', () =>
    <Button onPress={action('clicked-emoji')}>
      <Text>😀 😎 👍 💯</Text>
    </Button>
  );
