import React, { Component } from 'react';
import { TextInput, Text, View, Animated, StyleSheet, ScrollView } from 'react-native';

const noop = () => ({});

export const MotusInput = ({ 
    label = '',
    labelProps = {},
    onChangeText = noop,
    onBlur = noop,
    onFocus = noop,
    ...inputProps }) => {
    let text = '';


    const animation = new Animated.Value(0);

    const scale = animation.interpolate({
        inputRange: [0, 1],
        outputRange: [1, .8]
    });

    const opacity = animation.interpolate({
        inputRange: [0, 1],
        outputRange : [1, .8]
    });

    const translateY = animation.interpolate({
        inputRange: [0, 1],
        outputRange: [20, 0]
    });

    const translateX = animation.interpolate({
        inputRange: [0, 1],
        outputRange: [0, -6]
    });

    const labelStyle = {
        alignSelf: 'flex-start',
        transform: [{ translateY }, { translateX }, { scale }],
        opacity
    };

    return (
        <ScrollView style={{flex:1, paddingTop:40}}>
            <View style={styles.container}>
                {  
                    label && 
                    <FloatingLabel
                        {...labelProps}
                        style={labelStyle}
                        text={label} />
                }
                <TextInput 
                    style={styles.text}
                    onChangeText={(value) => { 
                        text = value;
                        onChangeText(value);
                    }}
                    onBlur={(e) => {
                        !text && triggerAnimations(animation, 0);
                        onBlur(e);
                    }}
                    onFocus={(e) => {
                        triggerAnimations(animation, 1);
                        onFocus(e);
                    }} 
                    {...inputProps} />
            </View>
        </ScrollView>
    )
};

/**
 * 
 * @param {Animated.Value} animation 
 * @param {number} toValue
 */
const triggerAnimations = (animation, toValue) => Animated.timing(animation, {
    toValue,
    duration: 100
}).start();

export const FloatingLabel = ({ text, style }) => {
    return (
        <Animated.Text 
            pointerEvents={"none"}
            style={style}>
            { text }
        </Animated.Text>
    )
};

const styles = StyleSheet.create({
    container : {
        // backgroundColor: 'blue'
    },
    label : {

    },
    text: {

    }
});