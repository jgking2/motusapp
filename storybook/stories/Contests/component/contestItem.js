import React, {Component} from 'react';
import {
    TouchableOpacity,
    Text,
    View,
    Image,
    StyleSheet,
    Animated
} from 'react-native';

//In MS.
const ANIMATION_DURATION = 200;

export class Contest extends Component {
    animation = {
        fadeAnim: new Animated.Value(0)
    }

    componentDidMount() {
        let {contestIndex} = this.props;
        const delay = (ANIMATION_DURATION / 3) * contestIndex;
        Animated.sequence([
            Animated.delay(delay),
            Animated.timing(
                this.animation.fadeAnim, {
                    toValue: 1,
                    duration: ANIMATION_DURATION
                }
            )
        ])
        .start();
    }

    render() {
        let {contest} = this.props;
        let temp = contest;
        let { fadeAnim } = this.animation;

        return (
            // <Animated.View>
            
                <Animated.View style={[{
                        opacity: fadeAnim,
                        transform: [{
                            translateY: fadeAnim.interpolate({
                                inputRange: [0, 1],
                                outputRange: [20, 0]
                            })
                        }]
                    }, styles.contest ]}>
                    <TouchableOpacity style={styles.contestInner} activeOpacity={.8}>
                        <Image style={styles.image} source={{uri:temp.photoUrl}}/>
                    <View style={styles.contestDetails}>
                        <Text style={styles.smallLabel}>
                            {temp.message.user}
                        </Text>
                        <Text>
                            {temp.message.content}
                        </Text>
                        <Text style={styles.date}>
                            {temp.message.when}
                        </Text>
                    </View>
                    </TouchableOpacity>
                </Animated.View>            
        )
    }
}

let styles = StyleSheet.create({
    smallLabel: {
        fontSize: 10,
        color: 'rgba(0,0,0,.8)'
    },
    contestDetails: {
        flex: 1,
        marginLeft: 50,
        padding: 5,
        justifyContent: 'center'
    },
    contest: {
        flex: 1,
        margin: 5,
    },
    contestInner: {
        flexDirection: 'row',        
    },
    date: {
        textAlign: 'right',
        fontSize: 8
    },
    image: {
        position: 'absolute',
        left: 0,
        height: 40,
        width: 40
    }
})