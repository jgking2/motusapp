import React, {Component} from 'react';

import {
    StyleSheet,
    View,
    Text,
    ScrollView,
    TouchableOpacity,
    ListView,
    Image
} from 'react-native';
import {Contest} from './component/contestItem';
import {Header} from 'react-navigation';

export class ContestItem extends Component {

    render() {
        let temp = [
            { 
                name : 'Testing', 
                photoUrl: 'https://firebasestorage.googleapis.com/v0/b/hellmonth-b0318.appspot.com/o/contest%2F1496370584836?alt=media&token=d209b751-e227-4fd0-bfaf-60d60e2cf8d1',
                message: {
                    user: 'Joseph King',
                    when: '07/22/2017 09:22:17',
                    content: 'Yeah here we are'
                },
                contestants: [
                    0, 2, 4
                ]
            }
        ]
        
        let data = new ListView.DataSource({
            rowHasChanged : (r1, r2) => r1 !== r2
        });
        const contests = data.cloneWithRows(new Array(15).fill(temp[0]));
        
        return (
            <ListView contentContainerStyle={styles.container} 
                onScroll={(e) => console.info('scrolling', e.nativeEvent)}
                dataSource={contests}
                renderRow={(item, sectionId, rowId)  => {
                        return (
                            <Contest contest={item} contestIndex={+rowId} />
                        )
                    }} />
                
            // </ScrollView>
        )
    }
}

let styles = StyleSheet.create({
    container: {
        marginTop: 22,
        padding: 5,
        flexDirection: 'column',
        // flex: 1,
        // alignItems: 'flex-start',
        // flexWrap: 'wrap'
    }
})