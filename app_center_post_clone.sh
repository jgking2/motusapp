#!/usr/bin/env bash
if [ “$APPCENTER_BRANCH” == master ] || [ “$1” == “production” ];
then
  echo “Switching to Firebase $1 environment”
  yes | cp -rf firebase_environments/$1/google-services.json android/app
  yes | cp -rf firebase_environments/$1/GoogleService-Info.plist ios/MotusApp
elif [ “$APPCENTER_BRANCH” == “development” ] || [ “$1” == “development” ];
then
  echo “Switching to Firebase $1 environment”
  yes | cp -rf firebase_environments/$1/google-services.json android/app
  yes | cp -rf firebase_environments/$1/GoogleService-Info.plist ios/MotusApp
elif [ “$1” == “envs” ]
then
  echo production
  echo development
else
  echo “Run ‘appcenter-post-clone.sh envs’ to list available environments.”
fi